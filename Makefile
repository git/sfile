#
# sfile makefile
#
# Copyright © 1998-2002, 2004-2005, 2008  Guillem Jover <guillem@hadrons.org>
#

export

MAKEFLAGS = --no-print-directory

#
# Config Section
#

PACKAGE   = sfile
VERSION   = 0.2

DIST_NAME = $(PACKAGE)-$(VERSION)
DIST_FILE = $(DIST_NAME).tar.xz
DIST_SIGN = gpg2 -a -b

prefix    = $(DESTDIR)/usr
bindir    = $(prefix)/bin
datadir   = $(prefix)/share
localedir = $(datadir)/locale

#
# Start of non User area
#

top_srcdir = $(CURDIR)
auxdir    = $(top_srcdir)/build-aux
SUBDIRS   = src

ALL_CPPFLAGS = -DLOCALEDIR=\""$(localedir)"\" -D_GNU_SOURCE=1
ALL_LDFLAGS = -L. -g

CFLAGS ?= -g -Wall

ifdef DEBUG
CFLAGS += -O0
else
CFLAGS += -O2
endif

INSTALL   = install

include $(auxdir)/dir.mk

all:: $(SUBDIRS)

dist:
	@if [ -d .git ]; then \
	  echo "Generating dist tarball $(DIST_FILE)"; \
	  git archive --prefix=$(DIST_NAME)/ HEAD | xz >$(DIST_FILE); \
	  $(DIST_SIGN) $(DIST_FILE); \
	else \
	  echo "error: cannot distribute outside the git tree" 2>&1; \
	  exit 1; \
	fi

.PHONY: all $(SUBDIRS) $(clean_targets) clean $(install_targets) install dist
