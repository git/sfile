#
# sfile included base makefile
#
# Copyright © 2004, 2005  Guillem Jover <guillem@hadrons.org>
#

srcdir = $(subst $(top_srcdir)/,,$(CURDIR))

%.o: %.c
	@echo " [CC]    $(srcdir)/$@"
	@$(CC) $(ALL_CPPFLAGS) $(CPPFLAGS) $(ALL_CFLAGS) $(CFLAGS) -c $< -o $@

DEPSDIR = .deps

$(DEPSDIR):
	@echo " [MKDIR] $(srcdir)/$@"
	@mkdir -p $@

$(DEPSDIR)/%.d: %.c
	@echo " [DEP]   $(srcdir)/$@"
	@$(CC) $(ALL_CPPFLAGS) $(CPPFLAGS) $< -MM > $@

all::

DEPS = $(addprefix $(DEPSDIR)/,$(SRC:.c=.d))

ifeq ($(sort $(DEPS)),$(sort $(wildcard $(DEPSDIR)/*.d)))
include $(DEPS)
endif

cmd_rm = @echo " [RM]    $(1)" && $(RM) $(1)

cmd_rmdir = @echo " [RMDIR] $(1)" && $(RM) -r $(1)

depends:: $(DEPSDIR) $(DEPS)

clean::
	$(call cmd_rmdir,$(DEPSDIR))

.PHONY: all depends clean
