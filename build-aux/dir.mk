#
# sfile makefile
#
# Copyright © 1998-2002, 2004, 2005  Guillem Jover <guillem@hadrons.org>
#

include $(auxdir)/base.mk

clean_targets = $(addsuffix -clean,$(SUBDIRS))
install_targets = $(addsuffix -install,$(SUBDIRS))

$(SUBDIRS):
	@$(MAKE) -C $@

clean:: $(clean_targets)
	$(call cmd_rm,*.o)

$(clean_targets):
	@$(MAKE) -C $(subst -clean,,$@) clean

install:: $(install_targets)

$(install_targets):
	@$(MAKE) -C $(subst -install,,$@) install

.PHONY:  $(SUBDIRS) $(clean_targets) clean $(install_targets) install
