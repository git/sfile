#
# sfile included makefile to generate libraries
#
# Copyright © 1998-2001, 2004-2005, 2008  Guillem Jover <guillem@hadrons.org>
#

include $(auxdir)/base.mk

#
# Needs to have defined $(SRC)
#

FILES   = $(basename $(SRC))
OBJS    = $(addsuffix .o,$(FILES))

ALL_CPPFLAGS += -iquote $(top_srcdir)/src -iquote .

all:: depends library

ifndef $(LIBRARY)
LIBRARY = lib$(notdir $(CURDIR)).a
endif

library: $(LIBRARY)

$(LIBRARY): $(OBJS)
	@echo " [AR]    $(srcdir)/$@"
	@$(AR) -rcs $(LIBRARY) $(OBJS)

install::

clean::
	$(call cmd_rm,*.o)
	$(call cmd_rm,*.a)

.PHONY: all library clean
