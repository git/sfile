/*
 * GNU Gettext MO file (multilingual translations)
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2000, 2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef MO_H
#define MO_H

#include "types.h"

/*
 * MO (PO binary file)
 */

#define MO_HDRSZ	sizeof(mo_header_t)

#define MO_SIGN1	"\x95\x04\x12\xde"
#define MO_SIGN2	"\xde\x12\x04\x95"

typedef struct {
	uint32_t	sign;
	uint32_t	ver;
	uint32_t	string_num;
	uint32_t	untranslated_table_offs;
	uint32_t	translated_table_offs;
	uint32_t	hash_table_size;
	uint32_t	hash_table_offs;
} PACKED mo_header_t;

typedef struct {
	uint32_t	string_size;
	uint32_t	string_offs;
} PACKED mo_table_entry_t;

#endif
