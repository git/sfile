/*
 * Interchange Format File (IFF 85)
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2000, 2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef IFF_H
#define IFF_H

#include "types.h"

#define IFF_HDRSZ	sizeof(iff_header_t)

/* Main file IDs */
#define IFF_ID_FORM	"FORM"
#define IFF_ID_CAT	"CAT "
#	define IFF_DATA_ID_JJJJ	"JJJJ"
#define IFF_ID_LIST	"LIST"

/* Found in CAT or LIST chunks */
#define IFF_ID_PROP	"PROP"
#	define	IFF_DATA_ID_OPGM	"OPGM"
#	define	IFF_DATA_ID_OCPU	"OCPU"
#	define	IFF_DATA_ID_OCMP	"OCMP"
#	define	IFF_DATA_ID_OSN		"OSN "
#	define	IFF_DATA_ID_ONAM	"UNAM"
#define IFF_ID_FILLER	"    "

#define IFF_TYPE_IFF	"IFF "
#define IFF_TYPE_8SVX	"8SVX"	/* 8-bit sampled sound voice */
#define IFF_TYPE_ANBM	"ANBM"	/* animated bitmap */
#define IFF_TYPE_FNTR	"FNTR"	/* raster font */
#define IFF_TYPE_FNTV	"FNTV"	/* vector font */
#define IFF_TYPE_FTXT	"FTXT"	/* formatted text */
#	define IFF_DATA_ID_TEXT	"TEXT"
#define IFF_TYPE_GSCR	"GSCR"	/* general-use musical score */
#define IFF_TYPE_ILBM	"ILBM"	/* interleaved raster bitmap image */
#define IFF_TYPE_PDEF	"PDEF"	/* Deluxe Print page definition */
#define IFF_TYPE_PICS	"PICS"	/* Macintosh picture */
#	define IFF_DATA_ID_PICT	"PICT"
#define IFF_TYPE_PLBM	"PLBM"	/* (obsolete) */
#define IFF_TYPE_USCR	"USCR"	/* Uhuru Sound Software musical score */
#define IFF_TYPE_UVOX	"UVOX"	/* Uhuru Sound Software Macintosh voice */
#define IFF_TYPE_SMUS	"SMUS"	/* simple musical score */
#define IFF_TYPE_VDEO	"VDEO"	/* Deluxe Video Construction Set video */

typedef struct {
	int8_t		sign[4];
	uint32_t	size;
	int8_t		type[4];
} PACKED iff_header_t;

typedef struct {
	int8_t		id[4];
	uint32_t	size;
	uint8_t		data[0];
} PACKED iff_chunk_t;

#endif
