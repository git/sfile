/*
 * Interchange Format File (IFF 85)
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2000, 2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include "subfile.h"
#include "file.h"
#include "iff.h"

static uint32_t
iff_dumper(file_t *file)
{
	HEADER(iff, file, hdr);

	file_add_entry(file, "iff header signature", "%.4s", hdr->sign);
	file_add_entry(file, "iff header type", "%.4s", hdr->type);
/*
	switch (hdr->type) {
	case IFF_TYPE_ : file_add_entry(file, ""); break;
	case IFF_TYPE_ : file_add_entry(file, ""); break;
	}
*/

	return 0;
}

static struct subfile_sign iff_sign[] = {
	{.si_id = IFF_ID_FORM, .si_size = 4,},
	{.si_id = IFF_ID_LIST, .si_size = 4,},
	{.si_id = IFF_ID_CAT, .si_size = 4,},
	{NULL}
};

struct subfile iff_subfile = {
	.sf_type = sf_type_unclassified,
	.sf_desc = "Interchange Format File (IFF 85)",
	.sf_sign = iff_sign,
	.sf_size = IFF_HDRSZ,
	.sf_dumper = iff_dumper,
};
