/*
 * String functions
 *
 * Part of sfile
 *
 * Copyright © 2012 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>

#include "string.h"

char *
str_bin2hex(char *dst, const char *src, size_t size)
{
	const char hex[] = "0123456789abcdef";
	size_t i;

	for (i = 0; i < size; i++) {
		int c = src[i];

		*dst++ = hex[(c >> 4) & 0xf];
		*dst++ = hex[c & 0xf];
	}
	*dst = '\0';

	return dst;
}

char *
str_msdosdate(char *dst, uint16_t date_val)
{
	unsigned int year, month, day;

	year = ((date_val >> 9) & 0x7f) + 1980;
	month = (date_val >> 5) & 0xf;
	day = date_val & 0x1f;

	sprintf(dst, "%04u-%02u-%02u", year, month, day);

	return dst;
}

char *
str_msdostime(char *dst, uint16_t time_val)
{
	unsigned int hour, minute, second;

	hour = (time_val >> 11) & 0x1f;
	minute = (time_val >> 5) & 0x3f;
	second = (time_val & 0x1f) * 2;

	sprintf(dst, "%02u:%02u:%02u", hour, minute, second);

	return dst;
}

char *
str_msdos_tdstamp(char *dst, uint32_t tdstamp)
{
	str_msdosdate(dst, tdstamp & 0xffff);
	dst[10] = ' ';
	str_msdostime(dst + 11, tdstamp >> 16);

	return dst;
}
