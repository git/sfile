/*
 * String functions
 *
 * Part of sfile
 *
 * Copyright © 2012 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef SFILE_STRING_H
#define SFILE_STRING_H

#include <stddef.h>
#include <stdint.h>

char *str_bin2hex(char *dst, const char *src, size_t size);
char *str_msdosdate(char *dst, uint16_t date_val);
char *str_msdostime(char *dst, uint16_t time_val);
char *str_msdos_tdstamp(char *dst, uint32_t tdstamp);

#endif
