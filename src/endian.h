/*
 * Endianness conversion functions
 *
 * Lib Version 1.0.3
 *
 * Copyright © 2000, 2001, 2002 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef ENDIAN_H
#define ENDIAN_H

#include <stdint.h>
#include <endian.h>

uint16_t swapb16(uint16_t v);
uint32_t swapb32(uint32_t v);
uint64_t swapb64(uint64_t v);

uint32_t swaps32(uint32_t v);
uint32_t swapbs32(uint32_t v);
uint64_t swapl64(uint64_t v);

/* In some cases we need actual functions, but due to the endian converters
 * possibly being implemented as macros, we use these simple wrappers. */
static inline uint16_t
betoh16(uint16_t value)
{
	return be16toh(value);
}
static inline uint32_t
betoh32(uint32_t value)
{
	return be32toh(value);
}
static inline uint64_t
betoh64(uint64_t value)
{
	return be64toh(value);
}

static inline uint16_t
letoh16(uint16_t value)
{
	return le16toh(value);
}
static inline uint32_t
letoh32(uint32_t value)
{
	return le16toh(value);
}
static inline uint64_t
letoh64(uint64_t value)
{
	return le64toh(value);
}

#endif
