/*
 * Subfile management
 *
 * Part of sfile
 *
 * Copyright © 2002, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include "subfile.h"

string_t subfile_type_desc[] = {
	"unknown",
	"unclassified",
	"archiver",
	"compressed",
	"executable",
	"executable library",
	"3D model",
	"graphics",
	"video",
	"sound",
	"formatted text",
	"data base",
	"spreadsheet",
};

int
print_field(subfile_t *subfile, string_t desc, string_t content, ...)
{
	return 0;
}
