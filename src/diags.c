/*
 * Diagnosis & debug functions
 *
 * Lib Version 2.1.0
 *
 * Copyright © 2000, 2001, 2002 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 */

#include <stdio.h>	/* fprintf(), vsnprintf() */
#include <stdlib.h>	/* exit() */
#include <stdarg.h>	/* va_*() */

#include "diags.h"

int verbose_level = 0;

void
verbose(int level, char *msg, ...)
{
	char str[400];
	va_list arg;

	va_start(arg, msg);
	vsnprintf(str, sizeof(str), msg, arg);
	va_end(arg);

	if (level <= verbose_level)
	        fprintf(stdout, "%s", str);
}

void
warning(char *msg, ...)
{
	char str[400];
	va_list arg;

	va_start(arg, msg);
	vsnprintf(str, sizeof(str), msg, arg);
	va_end(arg);

	fprintf(stderr, "warning: %s\n", str);
}

void
die(int die_stat, char *msg, ...)
{
	char str[400];
	va_list arg;

	va_start(arg, msg);
	vsnprintf(str, sizeof(str), msg, arg);
	va_end(arg);

	fprintf(stderr, "fatal: %s\n", str);

	exit(die_stat);
}
