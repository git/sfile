/*
 * sfile (SubFile structure dumper)
 *
 * - Searches and dumps info on internal file structure.
 * - Possibly ripping capabilities will be added and
 *   unstructured search.
 *
 * Copyright © 1998-2002, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <sys/types.h>
#include <sys/stat.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <errno.h>

#include "diags.h"
#include "subfile.h"
#include "file.h"
#include "i18n.h"

#include "subfile-list.h"
#include "config.h"

static int
default_dumper(file_t *file)
{
	int base, i, c;

	printf(" generic file dumper	:\n");
	for (base = 0; base < file->sf_size; base += 16) {
		printf(" %08x:", base);

		for (i = 0; i < 16; i++)
			printf(" %02x", file->sf_buf[base + i]);

		printf("  |");

		for (i = 0; i < 16; i++) {
			c = file->sf_buf[base + i];
			if (!isprint(c))
				c = '.';
			printf("%c", c);
		}

		printf("|\n");
	}

	return 0;
}

static void
print_file_info(file_t *file, enum subfile_type sf_type)
{
	if (file->sf_depth == 0) {
		file_add_head(file, "file name", "%s", file->f_name);
		file_add_head(file, "file type", "%s", subfile_type_desc[sf_type]);
		file_add_head(file, "file size", "%u", file->f_size);
	}
	file_add_head(file, "file offset", "%#0x (%u)",
	              file->f_offs, file->f_offs);
}

static void
print_sign_info(file_t *file, subfile_t *subfile, struct subfile_sign *sign)
{
	char *name;

	if (file->sf_depth == 0)
		name = "main header type";
	else
		name = "sub header type";

	if (sign && sign->si_desc)
		file_add_head(file, name, "%s (%s)", subfile->sf_desc,
		              sign->si_desc);
	else
		file_add_head(file, name, "%s", subfile->sf_desc);
}

/*
 * Detects if the header signature or
 * if the file extension is the equivalent
 */
static struct subfile_sign *
detect_signature(file_t *file, subfile_t *subfile)
{
	if (subfile->sf_sign) {
		struct subfile_sign *sign = subfile->sf_sign;

		while (sign->si_size) {
			if (!memcmp(file->sf_buf + sign->si_offs,
				    sign->si_id, sign->si_size))
				return sign;
			sign++;
		}
	}

	return NULL;
}

static int
detect_extension(file_t *file, subfile_t *subfile)
{
	if (subfile->sf_ext && file->f_ext) {
		char tmp[strlen(subfile->sf_ext) + 1];
		string_t ext;

		strcpy(tmp, subfile->sf_ext);
		for (ext = strtok(tmp, " ,;\\/"); ext; ext = strtok(NULL, " ,;\\/"))
			if (strcmp(file->f_ext, ext) == 0)
				return 1;
	}

	return 0;
}

static void
dump_subfile(file_t *file, subfile_t *subfile, struct subfile_sign *sign)
{
	print_file_info(file, subfile->sf_type);
	print_sign_info(file, subfile, sign);

	file->sf_depth++;

	if (file->sf_size < subfile->sf_size) {
		file->sf_buf = realloc(file->sf_buf, subfile->sf_size);
		if (file->sf_buf == NULL)
			die(ENOMEM, _("not enough memory"));

		fseek(file->f_fd, file->f_offs + file->sf_size, SEEK_SET);
		fread(file->sf_buf + file->sf_size,
		      subfile->sf_size - file->sf_size, 1, file->f_fd);
		file->sf_size = subfile->sf_size;
	}

	if (subfile->sf_dumper == NULL)
		default_dumper(file);
	else
		subfile->sf_dumper(file);
}

static int32_t
search_subfiles(file_t *file)
{
	size_t i;

	fseek(file->f_fd, file->f_offs, SEEK_SET);
	fread(file->sf_buf, file->sf_size, 1, file->f_fd);

	for (i = 0; subfile_list[i]; i++) {
		struct subfile_sign *sign;

		sign = detect_signature(file, subfile_list[i]);
		if (!sign &&
		    !(file->sf_depth == 0 &&
		      detect_extension(file, subfile_list[i])))
			continue;

		dump_subfile(file, subfile_list[i], sign);

		while (file->sf_offs.st_count) {
			file_pop_offs(file);
			search_subfiles(file);
		}
		return i;
	}

	print_file_info(file, sf_type_unknown);
	default_dumper(file);

	return -1;
}

static void
do_search_subfiles(string_t fname)
{
	struct file file;
	struct stat st;

	file.f_name = fname;
	file.f_ext = strrchr(file.f_name, '.');
	if (file.f_ext != NULL)
		file.f_ext++;

	file.f_fd = fopen(file.f_name, "rb");
	if (file.f_fd == NULL)
		die(ENOENT, _("opening file"));

	if (fstat(fileno(file.f_fd), &st) < 0)
		die(errno, _("stat'ing file"));
	file.f_size = st.st_size;
	file.f_offs = 0;

	stack_alloc(&file.sf_offs, SF_OFFS);

	file.sf_depth = 0;
	file.sf_size = SF_BUFSZ;
	file.sf_buf = malloc(file.sf_size);
	if (file.sf_buf == NULL)
		die(ENOMEM, _("not enough memory"));

	search_subfiles(&file);

	free(file.sf_buf);
	stack_free(&file.sf_offs);
	fclose(file.f_fd);
}

static size_t
list_subfiles(void)
{
	size_t i;

	for (i = 0; subfile_list[i]; i++) {
		printf("Description: \"%s\"\n", subfile_list[i]->sf_desc);
		printf("Type: %s\n", subfile_type_desc[subfile_list[i]->sf_type]);
		printf("Size: %zu\n", subfile_list[i]->sf_size);
		if (subfile_list[i]->sf_ext)
			printf("Extensions: %s\n", subfile_list[i]->sf_ext);
		printf("\n");
	}

	return 0;
}

static void
usage(string_t prog_name)
{
	printf(_("Usage: %s [options] file\n\n"), prog_name);
	puts(_("Options:\n"
	       "  -h, --help	prints this information\n"
	       "  -v, --version	prints version information\n"
	       "  -l, --list	dumps a list of all subfiles known"));

	exit(1);
}

int
main(int argc, char **argv)
{
	/* Initialize the i18n */
	setlocale(LC_ALL, "");
	setlocale(LC_MESSAGES, "");
	bindtextdomain(PACKAGE, LOCALEDIR);
	textdomain(PACKAGE);

	if (argc < 2)
		usage(PACKAGE);

	argv++;

	if (strcmp(*argv, "-h") == 0 || strcmp(*argv, "--help") == 0)
		usage(PACKAGE);

	if (strcmp(*argv, "-v") == 0 || strcmp(*argv, "--version") == 0) {
		printf("%s %s\n", PACKAGE, VERSION);
		return 0;
	}
	if (strcmp(*argv, "-l") == 0 || strcmp(*argv, "--list") == 0) {
		list_subfiles();
		return 0;
	}

	do_search_subfiles(*argv);

	return 0;
}
