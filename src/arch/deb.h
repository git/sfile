/*
 * Debian package
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2000, 2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef DEB_H
#define DEB_H

#include "types.h"

#define DEB_HDRSZ	0

#define DEB_SIGN1	"debian-binary   "
#define DEB_SIGN2	"debian-split    "

#define DEB_CONTROL	"control.tar.gz  "
#define DEB_DATA	"data.tar.gz     "

/*
"package"
"version"
"architecture"
"maintainer"
"source"
"depends"
"pre-depends"
"recommends"
"suggests"
"conflicts"
"provides"
"replaces"
"description"
"essential"
"section"
"priority"
"binary"
"installed-size"
"files"
"standards-version"
"distribution"
	"_stable_" "_unstable_" "_contrib_" "_non-free_" "_experimental_"
	"_frozen_"
"urgency"
"date"
"format"
"changes"
"filename" "msdos-filename"
"size" "md5sum"
"status"
"config-version"
"conffiles"
*/

typedef struct {
	uint32_t	dummy[500];
} PACKED deb_header_t;

#endif
