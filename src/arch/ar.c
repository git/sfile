/*
 * Unix / BSD archiver
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include <string.h>
#include <time.h>

#include "subfile.h"
#include "ar.h"

static uint32_t
ar_dumper(file_t *file)
{
	string_t sign = (string_t)file->sf_buf;

	file_add_entry(file, "ar header signature", "%.7s", sign);

	file_push_offs(file, file->f_offs + 8);

	return 0;
}

static struct subfile_sign ar_sign[] = {
	{.si_id = AR_SIGN, .si_size = 8,},
	{.si_id = AR_SVR1a_SIGN, .si_size = 5,
	 .si_desc = "System V Release 1 archive"},
	{.si_id = AR_SVR1b_SIGN, .si_size = 5,
	 .si_desc = "System V Release 1 archive"},
	{.si_id = AR_BOUT_SIGN, .si_size = 8,
	 .si_desc = "b.out archive"},
	{NULL}
};

struct subfile ar_subfile = {
	.sf_type = sf_type_arch,
	.sf_desc = "Unix/BSD archiver",
	.sf_sign = ar_sign,
	.sf_size = AR_HDRSZ,
	.sf_dumper = ar_dumper,
};

static uint32_t
ar_entry_dumper(file_t *file)
{
	ar_file_header_t *hdr = (ar_file_header_t *)file->sf_buf;
	off_t size;
	char *str_date;
	time_t t;

	sscanf(hdr->date, "%12jd", &t);
	str_date = ctime(&t);
	str_date[strlen(str_date) - 1] = '\0';

	file_add_entry(file, "file name", "%.16s", hdr->name);
	file_add_entry(file, "file date", "%.12s (%s)", hdr->date, str_date);
	file_add_entry(file, "file uid", "%.6s", hdr->uid);
	file_add_entry(file, "file gid", "%.6s", hdr->gid);
	file_add_entry(file, "file mode", "%.8s", hdr->mode);
	file_add_entry(file, "file size", "%.10s", hdr->size);
	file_add_entry(file, "file signature", "%#0.2hhx%0.2hhx", hdr->sign[0], hdr->sign[1]);

	sscanf(hdr->size, "%10jd", (intmax_t *)&size);
	size += size & 1;
	file_push_offs(file, file->f_offs + sizeof(ar_file_header_t) + size);

	return 0;
}

static struct subfile_sign ar_entry_sign[] = {
	{.si_id = AR_FILE_SIGN, .si_size = 2,
	 .si_offs = sizeof(ar_file_header_t) - 2},
	{NULL}
};

struct subfile ar_entry_subfile = {
	.sf_type = sf_type_arch,
	.sf_desc = "Unix/BSD archive file entry",
	.sf_sign = ar_entry_sign,
	.sf_size = sizeof(ar_file_header_t),
	.sf_dumper = ar_entry_dumper,
};
