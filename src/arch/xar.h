/*
 * eXtensible ARchiver
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2014 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef SFILE_XAR_H
#define SFILE_XAR_H

#include "types.h"

#define XAR_HDRSZ		sizeof(xar_header_t)

#define XAR_SIGN	"xar!"

#define XAR_HEADER_VERSION 0

#define XAR_CHKSUM_ALGO_NONE	0
#define XAR_CHKSUM_ALGO_SHA1	1
#define XAR_CHKSUM_ALGO_MD5	2

typedef struct {
	uint8_t		sign[4];
	uint16_t	size;
	uint16_t	version;
	uint64_t	toc_compressed_size;
	uint64_t	toc_uncompressed_size;
	uint32_t	chksum_algo;
} PACKED xar_header_t;

#endif
