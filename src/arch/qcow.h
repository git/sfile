/*
 * QCOW disk image
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2014 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef QCOW_H
#define QCOW_H

#include "types.h"

#define QCOW_HDRSZ		sizeof(qcow2_header_t)

#define QCOW_SIGN		"QFI\xfb"

#define QCOW_CRYPT_NONE		0
#define QCOW_CRYPT_AES		1

typedef struct {
	uint8_t sign[4];
	uint32_t version;

	uint64_t backing_file_offs;
	uint32_t backing_file_size;
	uint32_t mtime;

	uint64_t size;

	uint8_t cluster_bits;
	uint8_t l2_bits;
	uint32_t crypt_method;

	uint64_t l1_table_offs;
} qcow1_header_t;

typedef struct {
	uint8_t sign[4];
	uint32_t version;

	uint64_t backing_file_offs;
	uint32_t backing_file_size;

	uint32_t cluster_bits;
	uint64_t size;
	uint32_t crypt_method;

	uint32_t l1_table_size;
	uint64_t l1_table_offs;

	uint64_t refcount_table_offs;
	uint32_t refcount_table_clusters;

	uint32_t snapshots_count;
	uint64_t snapshots_offs;
} qcow2_header_t;

#endif
