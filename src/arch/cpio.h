/*
 * Unix CPIO archiver
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2000, 2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef CPIO_H
#define CPIO_H

#include "types.h"

#define CPIO_NEW_HDRSZ		sizeof(cpio_new_header_t)
#define CPIO_OLD_HDRSZ		sizeof(cpio_old_header_t)

#define CPIO_NEW_SIGN		"070701"
#define CPIO_CRC_SIGN		"070702"
#define CPIO_OLD_BSIGN		070707
#define CPIO_OLD_SIGN		"070707"

typedef struct {
	uint8_t	sign[6];
	uint8_t	dev[6];
	uint8_t	ino[6];
	uint8_t	mode[6];
	uint8_t	uid[6];
	uint8_t	gid[6];
	uint8_t	nlink[6];
	uint8_t	rdev[6];
	uint8_t	mtimes[11];
	uint8_t	name_size[6];
	uint8_t	file_sizes[11];
	uint8_t	mtime;		/* Long-aligned copy of `c_mtimes'. */
	uint8_t	filesize;	/* Long-aligned copy of `c_filesizes'. */
	uint8_t	*name;
} cpio_old_header_t;

typedef struct {
	uint8_t	sign[6];
	uint8_t	ino[8];
	uint8_t	mode[8];
	uint8_t	uid[8];
	uint8_t	gid[8];
	uint8_t	nlink[8];
	uint8_t	mtime[8];
	uint8_t	file_size[8];
	uint8_t	hdev[8];
	uint8_t	ldev[8];
	uint8_t	hrdev[8];
	uint8_t	lrdev[8];
	uint8_t	name_size[8];
	uint8_t	chksum[8];
	uint8_t	*name;
	uint8_t	*tar_linkname;
} cpio_new_header_t;

#endif
