/*
 * Redhat Package Manager
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2000, 2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef RPM_H
#define RPM_H

#include "types.h"

#define RPM_HDRSZ		sizeof(rpm_header_t)

#define RPM_SIGN		"\xed\xab\xee\xdb"

#define RPM_TYPE_BINARY		0
#define RPM_TYPE_SOURCE		1

#define RPM_MACH_i386		1

#define RPM_OS_LINUX		1

#define RPM_AUTH_NEWHEADER	5

typedef struct {
	uint8_t		sign[4];
	uint8_t		ver_hi;
	uint8_t		ver_lo;
	uint16_t	type;
	uint16_t	machine;
	uint8_t		pkg_name[66];
	uint16_t	os_type;
	uint16_t	auth_type;
	uint8_t		reserved[16];
} PACKED rpm_header_t;

typedef struct {
	uint8_t		sign[3];
	uint8_t		ver;
	uint8_t		reserved[4];
	uint32_t	count;
	uint32_t	size;
} PACKED rpm_entry_header_t;

#define RPM_TYPE_NULL		0
#define RPM_TYPE_CHAR		1
#define RPM_TYPE_INT8		2
#define RPM_TYPE_INT16		3
#define RPM_TYPE_INT32		4
#define RPM_TYPE_INT64		5
#define RPM_TYPE_STRING		6
#define RPM_TYPE_BIN		7
#define RPM_TYPE_STRING_ARRAY	8

#define RPM_AUTH_SIZE		1000
#define RPM_AUTH_MD5		1001
#define RPM_AUTH_PGP		1002

#define RPM_TAG_NAME		1000
#define RPM_TAG_VERSION		1001
#define RPM_TAG_RELEASE		1002
#define RPM_TAG_SERIAL		1003
#define RPM_TAG_SUMMARY		1004
#define RPM_TAG_DESCRIPTION	1005
#define RPM_TAG_BUILDTIME	1006
#define RPM_TAG_BUILDHOST	1007
#define RPM_TAG_INSTALLTIME	1008
#define RPM_TAG_SIZE		1009
#define RPM_TAG_DISTRIBUTION	1010
#define RPM_TAG_VENDOR		1011
#define RPM_TAG_GIF		1012
#define RPM_TAG_XPM		1013
#define RPM_TAG_COPYRIGHT	1014
#define RPM_TAG_PACKAGER	1015
#define RPM_TAG_GROUP		1016
#define RPM_TAG_CHANGELOG	1017 /* internal */
#define RPM_TAG_SOURCE		1018
#define RPM_TAG_PATCH		1019
#define RPM_TAG_URL		1020
#define RPM_TAG_OS		1021
#define RPM_TAG_ARCH		1022
#define RPM_TAG_PREIN		1023
#define RPM_TAG_POSTIN		1024
#define RPM_TAG_PREUN		1025
#define RPM_TAG_POSTUN		1026
#define RPM_TAG_FILENAMES	1027
#define RPM_TAG_FILESIZES	1028
#define RPM_TAG_FILESTATES	1029
#define RPM_TAG_FILEMODES	1030
#define RPM_TAG_FILEUIDS	1031 /* internal */
#define RPM_TAG_FILEGIDS	1032 /* internal */
#define RPM_TAG_FILERDEVS	1033
#define RPM_TAG_FILEMTIMES	1034
#define RPM_TAG_FILEMD5S	1035
#define RPM_TAG_FILELINKTOS	1036
#define RPM_TAG_FILEFLAGS	1037
#define RPM_TAG_ROOT		1038
#define RPM_TAG_FILEUSERNAME	1039
#define RPM_TAG_FILEGROUPNAME	1040
#define RPM_TAG_EXCLUDE		1041 /* not used - deprecated */
#define RPM_TAG_EXCLUSIVE	1042 /* not used - deprecated */
#define RPM_TAG_ICON		1043
#define RPM_TAG_SOURCERPM	1044
#define RPM_TAG_FILEVERIFYFLAGS	1045
#define RPM_TAG_ARCHIVESIZE	1046
#define RPM_TAG_PROVIDES	1047
#define RPM_TAG_REQUIREFLAGS	1048
#define RPM_TAG_REQUIRENAME	1049
#define RPM_TAG_REQUIREVERSION	1050
#define RPM_TAG_NOSOURCE	1051 /* internal */
#define RPM_TAG_NOPATCH		1052 /* internal */
#define RPM_TAG_CONFLICTFLAGS	1053
#define RPM_TAG_CONFLICTNAME	1054
#define RPM_TAG_CONFLICTVERSION	1055
#define RPM_TAG_DEFAULTPREFIX	1056
#define RPM_TAG_BUILDROOT	1057
#define RPM_TAG_INSTALLPREFIX	1058
#define RPM_TAG_EXCLUDEARCH	1059
#define RPM_TAG_EXCLUDEOS	1060
#define RPM_TAG_EXCLUSIVEARCH	1061
#define RPM_TAG_EXCLUSIVEOS	1062
#define RPM_TAG_AUTOREQPROV	1063 /* internal */
#define RPM_TAG_RPMVERSION	1064
#define RPM_TAG_TRIGGERSCRIPTS	1065 /* internal */
#define RPM_TAG_TRIGGERNAME	1066 /* internal */
#define RPM_TAG_TRIGGERVERSION	1067 /* internal */
#define RPM_TAG_TRIGGERFLAGS	1068 /* internal */
#define RPM_TAG_TRIGGERINDEX	1069 /* internal */
#define RPM_TAG_VERIFYSCRIPT	1079
#define RPM_TAG_CHANGELOGTIME	1080
#define RPM_TAG_CHANGELOGNAME	1081
#define RPM_TAG_CHANGELOGTEXT	1082
#define RPM_TAG_BROKENMD5	1083 /* internal */
#define RPM_TAG_PREREQ		1084 /* internal */
#define RPM_TAG_PREINPROG	1085
#define RPM_TAG_POSTINPROG	1086
#define RPM_TAG_PREUNPROG	1087
#define RPM_TAG_POSTUNPROG	1088
#define RPM_TAG_BUILDARCHS	1089
#define RPM_TAG_OBSOLETES	1090
#define RPM_TAG_VERIFYSCRIPTPROG	1091
#define RPM_TAG_TRIGGERSCRIPTPROGS	1092
#define RPM_TAG_DOCDIR		1093 /* internal */
#define RPM_TAG_COOKIE		1094
#define RPM_TAG_FILEDEVICES	1095
#define RPM_TAG_FILEINODES	1096
#define RPM_TAG_FILELANGS	1097

#define RPM_TAG_EXTERNAL_TAG	1000000

typedef struct {
	uint32_t	tag;
	uint32_t	type;
	uint32_t	offs;
	uint32_t	count;
} PACKED rpm_index_header_t;

#endif
