/*
 * Tape ARchive
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2000, 2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include <inttypes.h>
#include "subfile.h"
#include "tar.h"

/**
 * Convert an ASCII octal string to an intmax_t.
 */
static intmax_t
tar_atol8(const char *s, size_t size)
{
	intmax_t n = 0;

	while (*s == ' ') {
		s++;
		size--;
	}

	while (--size >= 0 && *s >= '0' && *s <= '7')
		n = (n * 010) + (*s++ - '0');

	return n;
}

static intmax_t
tar_atol256(const char *s, size_t size)
{
	intmax_t n = 0;
	unsigned char c;
	int sign;

	/* The encoding always sets the first bit to one, so that it can be
	 * distinguished from the ASCII encoding. */
	c = *s;
	sign = c & 0x40;
	if (!sign)
		c &= 0x7f;

	while (size-- > sizeof(intmax_t)) {
		if (*s++ != c)
			return sign ? INTMAX_MIN : INTMAX_MAX;
	}

	while (size-- > 0) {
		n = (n << 8) | c;
		c = *++s;
	}

	return n;
}

static intmax_t
tar_atol(const char *s, size_t size)
{
	/* Check if it is a long two-complement base-256 number, positive or
	 * negative. */
	if (*s == 0xff || *s == 0x80)
		return tar_atol256(s, size);

	return tar_atol8(s, size);
}

static uint32_t
tar_dumper(file_t *file)
{
	tar_header_t *hdr = (tar_header_t *)file->sf_buf;
	intmax_t file_size;
	intmax_t next_offs;
	intmax_t pad_size;

	file_size = tar_atol(hdr->size, sizeof(hdr->size));

	file_add_entry(file, "tar header signature", "%.7s", hdr->sign);
	file_add_entry(file, "file size", "%jd", file_size);
	file_add_entry(file, "file name", "%.100s", hdr->name);
	file_add_entry(file, "file link", "%.100s", hdr->link_name);
	file_add_entry(file, "file prefix", "%.155s", hdr->prefix);
	file_add_entry(file, "file mode", "%0o", tar_atol8(hdr->mode, sizeof(hdr->mode)));
	file_add_entry(file, "file uid", "%jd", tar_atol(hdr->uid, sizeof(hdr->uid)));
	file_add_entry(file, "file uname", "%.32s", hdr->uname);
	file_add_entry(file, "file gid", "%jd", tar_atol(hdr->gid, sizeof(hdr->gid)));
	file_add_entry(file, "file gname", "%.32s", hdr->gname);
	file_add_entry(file, "file time", "%jd", tar_atol(hdr->time, sizeof(hdr->time)));

	next_offs = file->f_offs + TAR_BLKSZ + file_size;
	pad_size = file_size % TAR_BLKSZ;
	if (pad_size > 0)
		next_offs += TAR_BLKSZ - pad_size;

	file_push_offs(file, next_offs);

	return 0;
}

static struct subfile_sign tar_sign[] = {
	{.si_id = TAR_USTAR_SIGN, .si_size = 5, .si_offs = 257, },
	{.si_id = TAR_GNU_SIGN, .si_size = 5, .si_offs = 257, },
	{NULL}
};

struct subfile tar_subfile = {
	.sf_type = sf_type_arch,
	.sf_desc = "Tape archiver",
	.sf_sign = tar_sign,
	.sf_size = TAR_HDRSZ,
	.sf_dumper = tar_dumper,
};
