/*
 * Debian package
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2000, 2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include "subfile.h"
#include "deb.h"

static struct subfile_sign deb_sign[] = {
	{.si_id = DEB_SIGN1, .si_size = 5,},
	{.si_id = DEB_SIGN2, .si_size = 5,},
	{NULL}
};

struct subfile deb_subfile = {
	.sf_type = sf_type_arch,
	.sf_desc = "Debian package",
	.sf_sign = deb_sign,
	.sf_size = DEB_HDRSZ,
};
