/*
 * StuffIt archive
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2014 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include "subfile.h"
#include "endian.h"
#include "stuffit.h"

static uint32_t
stuffit_dumper(file_t *file)
{
	stuffit_header_t *hdr = (stuffit_header_t *)file->sf_buf;

	file_add_entry(file, "stuffit header signature", "%.4s", hdr->sign);
	file_add_entry(file, "stuffit header id", "%.4s", hdr->id);
	file_add_entry(file, "stuffit header version", "%u", hdr->version);
	file_add_entry(file, "size", "%u", be32toh(hdr->size));
	file_add_entry(file, "files count", "%hu", be16toh(hdr->files_count));

	return 0;
}

static struct subfile_sign stuffit_sign[] = {
	{ .si_id = STUFFIT_SIGN, .si_size = 4 },
	{ NULL }
};

struct subfile stuffit_subfile = {
	.sf_type = sf_type_arch,
	.sf_desc = "StuffIt archive",
	.sf_sign = stuffit_sign,
	.sf_size = STUFFIT_HDRSZ,
	.sf_dumper = stuffit_dumper,
};
