/*
 * Tape ARchive
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2000, 2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef TAR_H
#define TAR_H

#include "types.h"

#define TAR_HDRSZ		sizeof(tar_header_t)
#define TAR_BLKSZ		512

#define TAR_USTAR_SIGN		"ustar\0" "00"
#define TAR_GNU_SIGN		"ustar " " \0"

#define TAR_MODE_SUID	04000
#define TAR_MODE_SGID	02000
#define TAR_MODE_SVTX	01000
#define TAR_MODE_UREAD	00400
#define TAR_MODE_UWRITE	00200
#define TAR_MODE_UEXEC	00100
#define TAR_MODE_GREAD	00040
#define TAR_MODE_GWRITE	00020
#define TAR_MODE_GEXEC	00010
#define TAR_MODE_OREAD	00004
#define TAR_MODE_OWRITE	00002
#define TAR_MODE_OEXEC	00001

#define TAR_TYPE_REG	'0'	/* Regular file (preferred code)	*/
#define TAR_TYPE_AREG	'\0'	/* Regular file (alternate code)	*/
#define TAR_TYPE_LNK	'1'	/* Hard link				*/
#define TAR_TYPE_SYM	'2'	/* Symbolic link (hard if not supported)*/
#define TAR_TYPE_CHR	'3'	/* Character special			*/
#define TAR_TYPE_BLK	'4'	/* Block special			*/
#define TAR_TYPE_DIR	'5'	/* Directory				*/
#define TAR_TYPE_FIFO	'6'	/* Named pipe				*/
#define TAR_TYPE_CONT	'7'	/* Contiguous file			*/
				/* (regular file if not supported)	*/

/* byte order: ASCII header */

typedef struct {
	char	name[100];
	char	mode[8];
	char	uid[8];
	char	gid[8];
	char	size[12];
	char	time[12];
	char	chksum[8];
	char	type;
	char	link_name[100];
	char	sign[8];
	char	uname[32];
	char	gname[32];
	char	dev_major[8];
	char	dev_minor[8];
	char	prefix[155];
} tar_header_t;

#endif
