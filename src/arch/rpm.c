/*
 * Redhat Package Manager
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2000, 2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include "endian.h"
#include "string.h"
#include "subfile.h"
#include "rpm.h"

static uint32_t
rpm_dumper(file_t *file)
{
	rpm_header_t *hdr = (rpm_header_t *)file->sf_buf;
	char hex_sign[sizeof(hdr->sign) + 1];
	uint16_t type, machine, os_type, auth_type;

	str_bin2hex(hex_sign, (const char *)hdr->sign, sizeof(hdr->sign));
	type = be16toh(hdr->type);
	machine = be16toh(hdr->machine);
	os_type = be16toh(hdr->os_type);
	auth_type = be16toh(hdr->auth_type);

	file_add_entry(file, "rpm header signature", "0x%s", hex_sign);
	file_add_entry(file, "rpm version", "%d.%d", hdr->ver_hi, hdr->ver_lo);
	file_add_entry(file, "rpm type", "%s (%d)",
	               type ? "binary" : "source", type);
	file_add_entry(file, "rpm machine", "%s (%d)",
	               machine == 1 ? "i386" : "unknown", machine);
	file_add_entry(file, "rpm package", "%.66s", hdr->pkg_name);
	file_add_entry(file, "rpm os type", "%s (%d)",
	               os_type == 1 ? "Linux" : "unknown", os_type );
	file_add_entry(file, "rpm auth type", "%s (%d)",
	               auth_type == 5 ? "new header" : "unknown", auth_type);

	return 0;
}

static struct subfile_sign rpm_sign[] = {
	{.si_id = RPM_SIGN, .si_size = 4,},
	{NULL}
};

struct subfile rpm_subfile = {
	.sf_type = sf_type_arch,
	.sf_desc = "Red Hat Package Manager",
	.sf_sign = rpm_sign,
	.sf_size = RPM_HDRSZ,
	.sf_dumper = rpm_dumper,
};
