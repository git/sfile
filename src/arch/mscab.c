/*
 * Microsoft Cabinet archiver
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2000-2001, 2004, 2014 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include "subfile.h"
#include "mscab.h"

static uint32_t
mscab_dumper(file_t *file)
{
	mscab_header_t *hdr = (mscab_header_t *)file->sf_buf;

	file_add_entry(file, "mscab header signature", "%.4s", hdr->sign);
	file_add_entry(file, "mscab header version", "%u.%u",
	               hdr->version_major, hdr->version_minor);
	file_add_entry(file, "size", "%u", hdr->size);
	file_add_entry(file, "flags", "%hx", hdr->flags);
	file_add_entry(file, "set ID", "%hu", hdr->set_id);
	file_add_entry(file, "cab ID", "%hu", hdr->cab_id);
	file_add_entry(file, "files offset", "%hu", hdr->files_offs);
	file_add_entry(file, "files count", "%hu", hdr->files_count);
	file_add_entry(file, "folders count", "%hu", hdr->folders_count);

	return 0;
}

static struct subfile_sign mscab_sign[] = {
	{ .si_id = MSCAB_SIGN, .si_size = 4 },
	{ NULL }
};

struct subfile mscab_subfile = {
	.sf_type = sf_type_arch,
	.sf_desc = "Microsoft Cabinet file",
	.sf_sign = mscab_sign,
	.sf_size = MSCAB_HDRSZ,
	.sf_dumper = mscab_dumper,
};
