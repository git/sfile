/*
 * QCOW disk image
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2014 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>

#include "subfile.h"
#include "endian.h"
#include "qcow.h"

static uint32_t
qcow_dumper(file_t *file)
{
	qcow2_header_t *hdr = (qcow2_header_t *)file->sf_buf;
	string_t desc;

	file_add_entry(file, "qcow header signature", "%.3s", hdr->sign);
	file_add_entry(file, "qcow header version", "%u", be32toh(hdr->version));

	file_add_entry(file, "backing file offs", "%ju", be64toh(hdr->backing_file_offs));
	file_add_entry(file, "backing file size", "%zu", be32toh(hdr->backing_file_size));

	file_add_entry(file, "cluster bits", "%hu", be32toh(hdr->cluster_bits));

	switch (hdr->crypt_method) {
	case QCOW_CRYPT_NONE	: desc = "none"; break;
	case QCOW_CRYPT_AES	: desc = "AES"; break;
	default			: desc = "unknown"; break;
	};
	file_add_entry(file, "crypt method", "%s", desc);

	file_add_entry(file, "size", "%ju", be64toh(hdr->size));

	file_add_entry(file, "L1 table size", "%u", be32toh(hdr->l1_table_size));
	file_add_entry(file, "L1 table offs", "%ju", be64toh(hdr->l1_table_offs));

	file_add_entry(file, "refcount table clusters", "%u", be32toh(hdr->refcount_table_clusters));
	file_add_entry(file, "refcount table offs", "%ju", be64toh(hdr->refcount_table_offs));

	file_add_entry(file, "snapshots count", "%u", be32toh(hdr->snapshots_count));
	file_add_entry(file, "snapshots offs", "%ju", be64toh(hdr->snapshots_offs));

	return 0;
}

static struct subfile_sign qcow_sign[] = {
	{ .si_id = QCOW_SIGN, .si_size = 4 },
	{ NULL }
};

struct subfile qcow_subfile = {
	.sf_type = sf_type_arch,
	.sf_desc = "QCOW disk image file",
	.sf_sign = qcow_sign,
	.sf_size = QCOW_HDRSZ,
	.sf_dumper = qcow_dumper,
};
