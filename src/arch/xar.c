/*
 * eXtensible ARchiver
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2014 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include <string.h>

#include "subfile.h"
#include "endian.h"
#include "xar.h"

static uint32_t
xar_dumper(file_t *file)
{
	xar_header_t *hdr = (xar_header_t *)file->sf_buf;

	file_add_entry(file, "xar header signature", "%.4s", hdr->sign);
	file_add_entry(file, "xar header version", "%d", be16toh(hdr->version));
	file_add_entry(file, "xar header size", "%d", be16toh(hdr->size));
	file_add_entry(file, "toc compressed size", "%jd",
	               be64toh(hdr->toc_compressed_size));
	file_add_entry(file, "toc uncompressed size", "%jd",
	               be64toh(hdr->toc_uncompressed_size));
	file_add_entry(file, "checksum algorithm", "%d", be32toh(hdr->chksum_algo));

	return 0;
}

static struct subfile_sign xar_sign[] = {
	{ .si_id = XAR_SIGN, .si_size = 4 },
	{ NULL }
};

struct subfile xar_subfile = {
	.sf_type = sf_type_arch,
	.sf_desc = "Extensible archiver",
	.sf_sign = xar_sign,
	.sf_size = XAR_HDRSZ,
	.sf_dumper = xar_dumper,
};
