/*
 * Unix / BSD archiver
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef AR_H
#define AR_H

#include "types.h"

/*
 * Unix/BSD ARchive header
 */

#define AR_HDRSZ		7

#define AR_SIGN			"!<arch>\012"
#define AR_SVR1a_SIGN		"\\<ar>"
#define AR_SVR1b_SIGN		"=<ar>"
#define AR_BOUT_SIGN		"!<bout>\012"

#define AR_FILE_SIGN		"`\012"

typedef struct {
	char	name[16];
	char	date[12];
	char	uid[6];
	char	gid[6];
	char	mode[8];
	char	size[10];
	char	sign[2];
} PACKED ar_file_header_t;

#endif
