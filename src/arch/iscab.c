/*
 * InstallShield Cabinet archiver
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2014 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include "subfile.h"
#include "iscab.h"

static uint32_t
iscab_dumper(file_t *file)
{
	iscab_header_t *hdr = (iscab_header_t *)file->sf_buf;

	file_add_entry(file, "iscab header signature", "%.4s", hdr->sign);
	file_add_entry(file, "iscab header version", "%#08x", hdr->version);
	file_add_entry(file, "volume_info", "%#08x", hdr->volume_info);
	file_add_entry(file, "cab descriptor offset", "%#08x", hdr->cab_descriptor_offs);
	file_add_entry(file, "cab descriptor size", "%#08x", hdr->cab_descriptor_size);

	return 0;
}

static struct subfile_sign iscab_sign[] = {
	{ .si_id = ISCAB_SIGN, .si_size = 4 },
	{ NULL }
};

struct subfile iscab_subfile = {
	.sf_type = sf_type_arch,
	.sf_desc = "InstallShield Cabinet file",
	.sf_sign = iscab_sign,
	.sf_size = ISCAB_HDRSZ,
	.sf_dumper = iscab_dumper,
};
