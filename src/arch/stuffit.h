/*
 * StuffIt archive
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2014 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef SFILE_STUFFIT_H
#define SFILE_STUFFIT_H

#include "types.h"

#define STUFFIT_HDRSZ		sizeof(stuffit_header_t)

#define STUFFIT_SIGN		"SIT!"
#define STUFFIT_ID		"rLau"

#define STUFFIT_METHOD_NONE		0	/* Uncompressed */
#define STUFFIT_METHOD_RLE		1	/* RLE compression */
#define STUFFIT_METHOD_LZW		2	/* LZW compression, 18k buffer, 14 bit code size */
#define STUFFIT_METHOD_HUFFMAN		3	/* Huffman compression */
#define STUFFIT_METHOD_ENCRYPTED	16	/* bit set if encrypted. ex: ENCRYPTED + LZW */
#define STUFFIT_METHOD_FOLDER_INI	32	/* marks start of a new folder */
#define STUFFIT_METHOD_FOLDER_END	33	/* marks end of the last folder started */

typedef struct {
	uint8_t		sign[4];
	uint16_t	files_count;
	uint32_t	size;
	uint8_t		id[4];
	uint8_t		version;
	uint8_t		reserved1;
} PACKED stuffit_header_t;

typedef struct {
	uint8_t		rsrc_fork_compression_method;
	uint8_t		data_fork_compression_method;
	uint8_t		file_name_len;
	uint8_t		file_name[0x3f];
	uint32_t	file_type;
	uint32_t	file_creator;
	uint16_t	finder_flags;
	uint32_t	ctime;
	uint32_t	mtime;
	uint32_t	rsrc_fork_uncompressed_size;
	uint32_t	data_fork_uncompressed_size;
	uint32_t	rsrc_fork_compressed_size;
	uint32_t	data_fork_compressed_size;
	uint16_t	rsrc_fork_crc;
	uint16_t	data_fork_crc;
	uint8_t		rsrc_fork_padding;
	uint8_t		data_fork_padding;
	uint32_t	reserved1;
	uint16_t	file_header_crc;
} PACKED stuffit_file_header_t;

#endif
