/*
 * InstallShield Cabinet archiver
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2014 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef SFILE_ISCAB_H
#define SFILE_ISCAB_H

#include "types.h"

#define ISCAB_HDRSZ		sizeof(iscab_header_t)

#define ISCAB_SIGN		"ISc("

typedef struct {
	uint8_t		sign[4];
	uint32_t	version;
	uint32_t	volume_info;
	uint32_t	cab_descriptor_offs;
	uint32_t	cab_descriptor_size;
} iscab_header_t;

typedef struct {
	uint64_t	data_offs;
	uint32_t	first_file_index;
	uint32_t	last_file_index;
	uint64_t	first_file_offs;
	uint64_t	first_file_uncompressed_size;
	uint64_t	first_file_compressed_size;
	uint64_t	last_file_offs;
	uint64_t	last_file_uncompressed_size;
	uint64_t	last_file_compressed_size;
} iscab_volume_t;

#define ISCAB_FILE_GROUP_OFFS_MAX	71
#define ISCAB_COMPONENT_OFFS_MAX	71

typedef struct {
	uint32_t file_table_offs_lo;
	uint64_t file_table_size;
	uint32_t directory_count;
	uint32_t file_count;
	uint32_t file_table_offs_hi;

	uint32_t file_group_offs[ISCAB_FILE_GROUP_OFFS_MAX];
	uint32_t component_offs[ISCAB_COMPONENT_OFFS_MAX];
} iscab_descriptor_t;

#define ISCAB_FILE_SPLIT		0x0001
#define ISCAB_FILE_OBFUSCATED		0x0002
#define ISCAB_FILE_COMPRESSED		0x0004
#define ISCAB_FILE_INVALID		0x0008

#define ISCAB_LINK_NONE			0
#define ISCAB_LINK_PREV			1
#define ISCAB_LINK_NEXT			2
#define ISCAB_LINK_BOTH			3

typedef struct {
	uint32_t name_offs;
	uint32_t directory_index;
	uint16_t flags;
	uint32_t uncompressed_size;
	uint32_t compressed_size;
	uint32_t data_offs;
	uint8_t md5[16];
	uint16_t volume;
	uint32_t link_prev;
	uint32_t link_next;
	uint8_t link_flags;
} iscab_file_descriptor_t;

typedef struct {
	uint32_t name_offs;
	uint32_t descriptor_offs;
	uint32_t next_offs;
} iscab_offs_list_t;

#endif
