/*
 * Microsoft Cabinet archiver
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2000-2001, 2004, 2014 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef SFILE_MSCAB_H
#define SFILE_MSCAB_H

#include "types.h"

#define MSCAB_HDRSZ		sizeof(mscab_header_t)

#define MSCAB_SIGN		"MSCF"

#define MSCAB_HEADER_FLAG_PREV_CAB		0x0001
#define MSCAB_HEADER_FLAG_NEXT_CAB		0x0002
#define MSCAB_HEADER_FLAG_RESERVE_PRESENT	0x0004

typedef struct {
	uint8_t		sign[4];
	uint32_t	reserved1;
	uint32_t	size;
	uint32_t	reserved2;
	uint32_t	files_offs;
	uint32_t	reserved3;
	uint8_t		version_minor;
	uint8_t		version_major;
	uint16_t	folders_count;
	uint16_t	files_count;
	uint16_t	flags;
	uint16_t	set_id;
	uint16_t	cab_id;
} mscab_header_t;

#define MSCAB_FOLDER_COMPTYPE_NONE	0x0000
#define MSCAB_FOLDER_COMPTYPE_MSZIP	0x0001
#define MSCAB_FOLDER_COMPTYPE_QUANTUM	0x0002
#define MSCAB_FOLDER_COMPTYPE_LZX	0x0003

typedef struct {
	uint32_t	data_offs;
	uint16_t	blocks_count;
	uint16_t	compression_type;
} mscab_folder_t;

/* Special file::folder_index values. */
#define MSCAB_FILE_CONT_FROM_PREV	0xfffd
#define MSCAB_FILE_CONT_TO_NEXT		0xfffe
#define MSCAB_FILE_CONT_PREV_AND_NEXT	0xffff

#define MSCAB_FILE_NAME_MAX		4096

#define MSCAB_ATTR_RDONLY		0x0001 /* file is read-only */
#define MSCAB_ATTR_HIDDEN		0x0002 /* file is hidden */
#define MSCAB_ATTR_SYSTEM		0x0004 /* file is a system file */
#define MSCAB_ATTR_ARCH			0x0020 /* file modified since last backup */
#define MSCAB_ATTR_EXEC			0x0040 /* run after extraction */
#define MSCAB_ATTR_NAME_IS_UTF8		0x0080 /* name[] contains UTF-8 */

typedef struct {
	uint32_t	uncompressed_size;
	uint32_t	folder_offs;
	uint16_t	folder_index;
	uint32_t	tdstamp;	/* DOS format */
	uint16_t	attrs;
	uint8_t		name[MSCAB_FILE_NAME_MAX];
} mscab_file_t;

typedef struct {
	uint32_t	cksum;
	uint16_t	compressed_size;
	uint16_t	uncompressed_size;
} mscab_data_t;

#endif
