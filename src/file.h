/*
 * File structures
 *
 * Part of sfile
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef FILE_H
#define FILE_H

#include <stdio.h>
#include "stack.h"
#include "types.h"

/*
 * Structure passed through the SubFile routines
 */

#define SF_OFFS		50
#define SF_BUFSZ	512

struct file {
	FILE		*f_fd;		/* File descriptor	*/
	string_t	f_name;		/* File name		*/
	string_t	f_ext;		/* File extension	*/
	uintmax_t	f_size;		/* File size		*/
	uintmax_t	f_offs;		/* File offset		*/

	uint8_t		*sf_buf;	/* SubFile slice	*/
	uintmax_t	sf_size;	/* SubFile size		*/
	size_t		sf_depth;	/* SubFile depth level	*/
	stack_t		sf_offs;	/* SubFile offset stack	*/
};

typedef struct file file_t;

extern void file_add_head(file_t *file, char *desc, char *fmt, ...);
extern void file_add_entry(file_t *file, char *desc, char *fmt, ...);

/*
 * Adds a offs in th offs stack
 */
extern int file_push_offs(file_t *file, uintmax_t offs);

/*
 * Sets file.f_offs (current file offs) to the popped offs
 */
extern int file_pop_offs(file_t *file);

#endif
