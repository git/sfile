/*
 * Diagnosis & debug functions
 *
 * Lib Version 2.0.1
 *
 * Copyright © 2000, 2001, 2002 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef DIAGS_H
#define DIAGS_H

#ifdef DEBUG
#define debug(format, args...)	fprintf(stderr, format, ##args)
#else
#define debug(format, args...)
#endif

extern int verbose_level;

extern void verbose(int level, char *msg, ...);
extern void warning(char *msg, ...);
extern void die(int die_stat, char *msg, ...);

#endif
