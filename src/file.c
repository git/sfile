/*
 * File structures
 *
 * Part of sfile
 *
 * Copyright © 1998-2001 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <errno.h>
#include <stdarg.h>
#include <stdlib.h>

#include "diags.h"
#include "file.h"

void
file_add_head(file_t *file, char *name, char *fmt, ...)
{
	va_list args;
	char *value;

	va_start(args, fmt);
	if (vasprintf(&value, fmt, args) < 0)
		die(ENOMEM, "failed allocating entry value");
	va_end(args);

	printf("%-24.24s: %s\n", name, value);

	free(value);
}

void
file_add_entry(file_t *file, char *name, char *fmt, ...)
{
	va_list args;
	char *value;

	va_start(args, fmt);
	if (vasprintf(&value, fmt, args) < 0)
		die(ENOMEM, "failed allocating entry value");
	va_end(args);

	printf(" %-23.23s: %s\n", name, value);

	free(value);
}

int
file_push_offs(file_t *file, uintmax_t offs)
{
	if (offs >= file->f_size)
		return 0;

	return stack_push(&file->sf_offs, offs);
}

int
file_pop_offs(file_t *file)
{
	return stack_pop(&file->sf_offs, &file->f_offs);
}
