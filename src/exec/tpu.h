/*
 * Turbo Pascal Unit (TPU) library
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef TPU_H
#define TPU_H

#include "types.h"

/*
 * Turbo Pascal Units (aka Object Library)
 */

#define TPU_SIGN	"TPU"

#define TPU_VER_6_0	'9'

/* UNIT 6.0 */
typedef struct {
	uint8_t		sign[3];
	uint8_t		ver;
	uint16_t	res1;
	uint16_t	res2;
	uint16_t	offs_this_unit;
	uint16_t	offs_hashtable;
	uint16_t	offs_entry_ptrs;
	uint16_t	offs_code_blocks;
	uint16_t	offs_const_blocks;
	uint16_t	offs_var_blocks;
	uint16_t	offs_dll_list;
	uint16_t	offs_unit_list;
	uint16_t	offs_src_name;
	uint16_t	offs_line_lengths;
	uint16_t	sym_size;
	uint16_t	code_size;
	uint16_t	const_size;
	uint16_t	reloc_size;
	uint16_t	const_reloc_size;
	uint16_t	var_size;
	uint16_t	offs_full_hash;
	uint16_t	flags;
	uint8_t		res3[21];
} PACKED tpu6_header_t;

#define TPU_VER_7_0	'Q'

/* UNIT 7.0 */
typedef struct {
	uint8_t		sign[3];
	uint8_t		ver;
	uint16_t	res1;
	uint16_t	res2;
	uint16_t	offs_this_unit;
	uint16_t	offs_hashtable;
	uint16_t	offs_entry_ptrs;
	uint16_t	offs_code_blocks;
	uint16_t	offs_const_blocks;
	uint16_t	offs_var_blocks;
	uint16_t	offs_dll_list;
	uint16_t	offs_unit_list;
	uint16_t	offs_src_name;
	uint16_t	offs_line_count;	/* diff from 6.0 */
	uint16_t	offs_line_lengths;
	uint16_t	sym_size;
	uint16_t	browser_size;		/* diff from 6.0 */
	uint16_t	code_size;
	uint16_t	const_size;
	uint16_t	reloc_size;
	uint16_t	const_reloc_size;
	uint16_t	var_size;
	uint16_t	offs_full_hash;
	uint16_t	flags;
	uint16_t	object_type_list;	/* diff from 6.0 from here */
						/* offset of last object type def
						previous object in previous_object_def */
	uint16_t	br_defs_end;
	uint16_t	br_symbol_refxx1;
	uint32_t	next_tpu;		/* next pointers used in TPUMOVER */
	uint32_t	browser_ptr;
	uint32_t	code_ptr;
	uint32_t	const_ptr;
	uint32_t	reloc_ptr;
	uint32_t	const_reloc_ptr;
	uint8_t		res3[17];
} PACKED tpu7_header_t;

#endif
