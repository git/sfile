/*
 * Portable Executable (PE)
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include <time.h>
#include "subfile.h"
#include "pe.h"

static uint32_t
pe_dumper(file_t *file)
{
	pe_header_t *hdr = (pe_header_t *)file->sf_buf;
	pe_opt_header_t *ohdr = (pe_opt_header_t *)(file->sf_buf + PE_HDRSZ);
	const char *desc;
	time_t t = (time_t)hdr->coff.tdstamp;
	char ts[32];

	file_add_entry(file, "pe header signature", "%#x (%.2s)", hdr->sign, (string_t)&(hdr->sign));
	file_add_entry(file, "coff header signature", "%#x", hdr->coff.machine);
	printf(" flags			: <|");
	if (hdr->coff.flags & COFF_FLAG_EXEC_IMAGE)
		printf("exec image|");
	if (hdr->coff.flags & COFF_FLAG_DLL)
		printf("dll|");
	if (hdr->coff.flags & COFF_FLAG_AGGRO_WS_TRIM)
		printf("aggro ws trim|");
	if (hdr->coff.flags & COFF_FLAG_16BIT)
		printf("16-bit|");
	if (hdr->coff.flags & COFF_FLAG_32BIT)
		printf("32-bit|");
	if (hdr->coff.flags & COFF_FLAG_STRIP_RELOC)
		printf("strip reloc|");
	if (hdr->coff.flags & COFF_FLAG_STRIP_LNNO)
		printf("strip lnno|");
	if (hdr->coff.flags & COFF_FLAG_STRIP_LSYM)
		printf("strip lsym|");
	if (hdr->coff.flags & COFF_FLAG_STRIP_DEBUG)
		printf("strip debug|");
	if (hdr->coff.flags & COFF_FLAG_LARGE_ADDR)
		printf("large addr|");
	printf(">\n");
	file_add_entry(file, "number of sections", "%u", hdr->coff.sect_num);
	if (strftime(ts, sizeof(ts), "%c", gmtime(&t)) > 0)
		file_add_entry(file, "time & date stamp", "%s", ts);
	file_add_entry(file, "symbol table offs", "%u", hdr->coff.sym_offs);
	file_add_entry(file, "symbol items", "%u", hdr->coff.sym_num);
	file_add_entry(file, "optional header size", "%u", hdr->coff.opthdr_size);

	if (hdr->coff.opthdr_size > 0) {
		file_add_entry(file, "opt header signature", "%#o", ohdr->aout.sign);
		file_add_entry(file, "opt header size", "%u", ohdr->hdrs_size);
		file_add_entry(file, "version stamp", "%u", ohdr->aout.ver);
		file_add_entry(file, "section (.text) size", "%u", ohdr->aout.text_size);
		file_add_entry(file, "section (.data) size", "%u", ohdr->aout.data_size);
		file_add_entry(file, "section (.bss) size", "%u", ohdr->aout.bss_size);
		file_add_entry(file, "entry point", "%u", ohdr->aout.entry);
		file_add_entry(file, "section (.text) start", "%u", ohdr->aout.text_start);
		file_add_entry(file, "section (.data) start", "%u", ohdr->aout.data_start);
		file_add_entry(file, "os version", "%u.%u",
		               ohdr->os_ver_hi, ohdr->os_ver_lo);
		file_add_entry(file, "subsystem version", "%u.%u",
		               ohdr->subsys_ver_hi, ohdr->subsys_ver_lo);
		file_add_entry(file, "image version", "%u.%u",
		               ohdr->image_ver_hi, ohdr->image_ver_lo);
		file_add_entry(file, "image base", "%u", ohdr->image_base);
		file_add_entry(file, "image size", "%u", ohdr->image_size);
		file_add_entry(file, "section alignment", "%u", ohdr->sect_align);
		file_add_entry(file, "file alignment", "%u", ohdr->file_align);
		file_add_entry(file, "checksum", "%u", ohdr->chksum);

		switch (ohdr->subsystem) {
		case PE_SUBSYS_NATIVE		: desc = "Native"; break;
		case PE_SUBSYS_WINDOWS_GUI	: desc = "Windows GUI"; break;
		case PE_SUBSYS_WINDOWS_CLI	: desc = "Windows CLI"; break;
		case PE_SUBSYS_OS2_CLI		: desc = "OS/2 CLI"; break;
		case PE_SUBSYS_POSIX_CLI	: desc = "POSIX CLI"; break;
		case PE_SUBSYS_WINDOWS_CE_GUI	: desc = "Windows CE GUI"; break;
		case PE_SUBSYS_WINDOWS_BOOT_APP	: desc = "Windows Boot App"; break;
		case PE_SUBSYS_XBOX		: desc = "XBox"; break;
		case PE_SUBSYS_EFI_APP		: desc = "EFI application"; break;
		default				: desc = "unknown"; break;
		}
		file_add_entry(file, "subsystem", "%s (%u)", desc, ohdr->subsystem);

		file_add_entry(file, "dll characteristics", "%u", ohdr->dll_flags);
		file_add_entry(file, "stack reserve size", "%u", ohdr->stack_reserve_size);
		file_add_entry(file, "stack commit size", "%u", ohdr->stack_commit_size);
		file_add_entry(file, "heap reserve size", "%u", ohdr->heap_reserve_size);
		file_add_entry(file, "heap commit size", "%u", ohdr->heap_commit_size);
		file_add_entry(file, "loader flags", "%u", ohdr->loader_flags);
		file_add_entry(file, "number of rva and size ", "%u", ohdr->rva_num_size);
	}

	return 0;
}

static struct subfile_sign pe_sign[] = {
	{.si_id = PE_SIGN, .si_size = 2,},
	{NULL}
};

struct subfile pe_subfile = {
	.sf_type = sf_type_exec,
	.sf_desc = "Portable Executable (based on Unix COFF)",
	.sf_sign = pe_sign,
	.sf_size = PE_HDRSZ + PE_OPTHDRSZ,
	.sf_dumper = pe_dumper,
};
