/*
 * Portable Executable (PE)
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef PE_H
#define PE_H

#include "types.h"
#include "coff.h"

/*
 * Portable Executable header (PE)
 */

#define PE_HDRSZ		sizeof(pe_header_t)
#define PE_OPTHDRSZ		sizeof(pe_opt_header_t)

#define PE_SIGN		"PE"    /* 0x4550 */

typedef struct {
	uint32_t		sign;
	coff_header_t		coff;
} PACKED pe_header_t;

#define PE_SUBSYS_UNKNOWN		0
#define PE_SUBSYS_NATIVE		1
#define PE_SUBSYS_WINDOWS_GUI		2
#define PE_SUBSYS_WINDOWS_CLI		3
#define PE_SUBSYS_OS2_CLI		5
#define PE_SUBSYS_POSIX_CLI		7
#define PE_SUBSYS_NATIVE_WINDOWS9X	8
#define PE_SUBSYS_WINDOWS_CE_GUI	9
#define PE_SUBSYS_EFI_APP		10
#define PE_SUBSYS_EFI_BOOT_DRIVER	11
#define PE_SUBSYS_EFI_RUNTIME_DRIVER	12
#define PE_SUBSYS_EFI_ROM		13
#define PE_SUBSYS_XBOX			14
#define PE_SUBSYS_WINDOWS_BOOT_APP	16

typedef struct {
	uint32_t		vaddr;
	uint32_t		size;
} PACKED pe_data_dir_t;

typedef struct {
	coff_opt_header_t	aout;
	uint32_t		image_base;
	uint32_t		sect_align;
	uint32_t		file_align;
	uint16_t		os_ver_hi;
	uint16_t		os_ver_lo;
	uint16_t		image_ver_hi;
	uint16_t		image_ver_lo;
	uint16_t		subsys_ver_hi;
	uint16_t		subsys_ver_lo;
	uint8_t			reserved[4];
	uint32_t		image_size;
	uint32_t		hdrs_size;
	uint32_t		chksum;
	uint16_t		subsystem;
	uint16_t		dll_flags;
	uint32_t		stack_reserve_size;
	uint32_t		stack_commit_size;
	uint32_t		heap_reserve_size;
	uint32_t		heap_commit_size;
	uint32_t		loader_flags;
	uint32_t		rva_num_size;
	pe_data_dir_t		data_dir[16];
} PACKED pe_opt_header_t;

typedef struct {
	uint16_t	sign;
	uint16_t	ver;
	uint32_t	text_size;
	uint32_t	data_size;
	uint32_t	bss_size;
	uint32_t	entry;
	uint32_t	text_start;
} PACKED pe64_coff_opt_header_t;

typedef struct {
	pe64_coff_opt_header_t	aout;
	uint64_t		image_base;
	uint32_t		sect_align;
	uint32_t		file_align;
	uint16_t		os_ver_hi;
	uint16_t		os_ver_lo;
	uint16_t		image_ver_hi;
	uint16_t		image_ver_lo;
	uint16_t		subsys_ver_hi;
	uint16_t		subsys_ver_lo;
	uint8_t			reserved[4];
	uint32_t		image_size;
	uint32_t		hdrs_size;
	uint32_t		chksum;
	uint16_t		subsystem;
	uint16_t		dll_flags;
	uint64_t		stack_reserve_size;
	uint64_t		stack_commit_size;
	uint64_t		heap_reserve_size;
	uint64_t		heap_commit_size;
	uint32_t		loader_flags;
	uint32_t		rva_num_size;
	pe_data_dir_t		data_dir[16];
} PACKED pe64_opt_header_t;

#endif
