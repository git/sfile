/*
 * Playstation executable
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2002, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include "subfile.h"
#include "psx.h"

static uint32_t
psx_dumper(file_t *file)
{
	psx_header_t *hdr = (psx_header_t *)file->sf_buf;

	file_add_entry(file, "psx header signature", "%.8s",
	               (char *)&(hdr->sign));
	file_add_entry(file, "entry addr", "%#x (%u)", hdr->entry_addr, hdr->entry_addr);
	file_add_entry(file, "load addr", "%#x (%u)", hdr->load_addr, hdr->load_addr);
	file_add_entry(file, "text size", "%#x (%u)", hdr->text_size, hdr->text_size);
	file_add_entry(file, "stack addr", "%#x (%u)", hdr->stack_addr, hdr->stack_addr);
	file_add_entry(file, "territory", "%c", hdr->disclaimer[37]);

	return 0;
}

static struct subfile_sign psx_sign[] = {
	{.si_id = PSX_SIGN, .si_size = 8,},
	{NULL}
};

struct subfile psx_subfile = {
	.sf_type = sf_type_exec,
	.sf_desc = "Playstation executable",
	.sf_sign = psx_sign,
	.sf_size = PSX_HDRSZ,
	.sf_dumper = psx_dumper,
};
