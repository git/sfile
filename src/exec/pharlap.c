/*
 * Pharlap executable
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include "subfile.h"
#include "pharlap.h"

static struct subfile_sign pharlap_new_sign[] = {
	{.si_id = P2_SIGN, .si_size = 2,},
	{.si_id = P3_SIGN, .si_size = 2,},
	{NULL}
};

struct subfile pharlap_new_subfile = {
	.sf_type = sf_type_exec,
	.sf_desc = "PharLap New executable (.EXP)",
	.sf_sign = pharlap_new_sign,
};

static struct subfile_sign pharlap_old_sign[] = {
	{.si_id = MP_SIGN, .si_size = 2, .si_offs = 0,},
	{NULL}
};

struct subfile pharlap_old_subfile = {
	.sf_type = sf_type_exec,
	.sf_desc = "PharLap Old executable (.EXP)",
	.sf_sign = pharlap_old_sign,
};
