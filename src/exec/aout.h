/*
 * Unix a.out executable
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef AOUT_H
#define AOUT_H

#include "types.h"

/*
 * a.out header
 */

#define AOUT_HDRSZ		sizeof(aout_header_t)

#define AOUT_QMAGIC		"\xcc\x00"	/* 0314 Demand Paged exec w/ hdr in text*/
#define AOUT_COFF_STMAGIC	"\x01\x01"	/* 0401 Target shared lib */
#define AOUT_COFF_OMAGIC	"\x04\x01"	/* 0404 Object file */
#define AOUT_HPUX_OMAGIC	"\x06\x01"	/* 0406 Object file*/
#define AOUT_A29K_SHMAGIC	"\x06\x01"	/* 0406 */
#define AOUT_OMAGIC		"\x07\x01"	/* 0407 Object file/Impure exec */
#define AOUT_NMAGIC		"\x08\x01"	/* 0410 Pure exec */
#define AOUT_ZMAGIC		"\x0b\x01"	/* 0413 Demand Paged exec */
#define AOUT_BMAGIC		"\x0d\x01"	/* 0415 b.out file */
#define AOUT_CMAGIC		"\x11\x01"	/* 0421 Core file */
#define AOUT_COFF_SHMAGIC	"\x23\x01"	/* 0443 Host shared lib */
#define AOUT_A29K_OMAGIC	"\x7a\x01"	/* 0572 */
/*#define AOUT_A29K_OMAGIC	"\x7b\x01"*/	/* 0573 */
#define AOUT_DYNIX_OMAGIC	"\xeb\x12"	/* 0x12eb */
#define AOUT_DYNIX_ZMAGIC	"\xeb\x22"	/* 0x22eb */
#define AOUT_DYNIX_XMAGIC	"\xeb\x32"	/* 0x32eb */
#define AOUT_DYNIX_SMAGIC	"\xeb\x42"	/* 0x42eb */
#define AOUT_64_OMAGIC		"\x01\x10"	/* 0x1001 */
#define AOUT_64_ZMAGIC		"\x02\x10"	/* 0x1002 */
#define AOUT_64_NMAGIC		"\x03\x10"	/* 0x1003 */
#define AOUT_ADOBE_ZMAGIC	"\xbe\xd0\x0a"	/* 0xAD0BE */

typedef struct {
	uint16_t	sign;
	uint16_t	ver;
	uint32_t	text_size;
	uint32_t	data_size;
	uint32_t	bss_size;
	uint32_t	syms_size;
	uint32_t	entry;
	uint32_t	text_reloc_size;
	uint32_t	data_reloc_size;
} PACKED aout_header_t;

#endif
