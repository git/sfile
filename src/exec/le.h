/*
 * OS/2 Linear Executable (LE)
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef LE_H
#define LE_H

#include "types.h"

/*
 * Linear executable (LE, LX)
 */

#define LE_HDRSZ		sizeof(le_header_t)

#define LX_SIGN			"LX"	/* 0x584C */
#define LE_SIGN			"LE"	/* 0x454C */
#define W3_SIGN			"W3"	/* 0x3357 */
#define W4_SIGN			"W4"	/* 0x3457 */

#define OS_WINDOWS		0x454C
#define OS_OS2			0x584C

/*
 * Machine Type
 */

#define LE_MACH_286		1
#define LE_MACH_386		2
#define LE_MACH_486		3
#define LE_MACH_PENTIUM		4
#define LE_MACH_860		0x20
#define LE_MACH_N11		0x21
#define LE_MACH_MIPS1		0x40
#define LE_MACH_MIPS2		0x41
#define LE_MACH_MIPS3		0x42

/*
 * OS Type
 */

#define LE_OS_OS2		1
#define LE_OS_WINDOWS		2
#define LE_OS_EURODOS4		3
#define LE_OS_WINDOWS386	4

/*
 * Module Type Bitfield
 */

#define LE_INIT_MASK		1
#define LE_INIT_GLOBAL		0
#define LE_INIT_PRPRSS		1

#define LE_NO_INT_FIXUPS_MASK	0x10
#define LE_NO_EXT_FIXUPS_MASK	0x20

#define LE_API_SHIFT			8
#define LE_API_MASK			0x70
#define LE_API_UNKNOWN			0
#define LE_API_PM_WINDOWS_INCOMPAT	1
#define LE_API_PM_WINDOWS_COMPAT	2
#define LE_API_PM_WINDOWS_USED		3

#define LE_NO_LOAD_MASK		0x2000

#define LE_MT_SHIFT		15
#define LE_MT_MASK		0x700
#define LE_MT_PROG		0
#define LE_MT_DLL		1
#define LE_MT_PDLL		3
#define LE_MT_PHIS		4
#define LE_MT_VIRT		6

#define LE_TERM_MASK		0x40000000

typedef struct {
	uint16_t	sign;
	uint8_t		byte_order;
	uint8_t		word_order;
	uint32_t	exe_fmt_level;
	uint16_t	cpu_type;
	uint16_t	os_type;
	uint32_t	module_ver;
	uint32_t	module_type;
	uint32_t	mem_page_num;
	uint32_t	cs;
	uint32_t	eip;
	uint32_t	ss;
	uint32_t	esp;
	uint32_t	mem_page_size;
	union {
		uint32_t	last_size;
		uint32_t	offs_shift;
	} page;
	uint32_t	fixup_size;
	uint32_t	fixup_chksum;
	uint32_t	loader_size;
	uint32_t	loader_chksum;
	uint32_t	object_table_offs;
	uint32_t	object_table_entries;
	uint32_t	object_page_map_table_offs;
	uint32_t	object_iterate_dmap_offs;
	uint32_t	resource_table_offs;
	uint32_t	resource_table_entries;
	uint32_t	resident_name_table_offs;
	uint32_t	entry_table_offs;
	uint32_t	module_directives_table_offs;
	uint32_t	module_directives_table_entries;
	uint32_t	fixup_page_table_offs;
	uint32_t	fixup_record_table_offs;
	uint32_t	imported_module_name_table_offs;
	uint32_t	imported_module_count;
	uint32_t	imported_procedure_name_table_offs;
	uint32_t	per_page_chksum_table_offs;
	uint32_t	data_page_offs;
	uint32_t	preload_page_count;
	uint32_t	nonresident_name_table_offs;
	uint32_t	nonresident_name_table_size;
	uint32_t	nonresident_name_chksum;
	uint32_t	autodata_object;
	uint32_t	debug_info_offs;
	uint32_t	debug_info_size;
	uint32_t	preload_instance_page_num;
	uint32_t	demand_instance_page_num;
	uint32_t	extra_heap_alloc;
	uint8_t		reserved[12];
	uint32_t	ver_info_resource_offs;
	uint32_t	pointer_to_unknown;
	uint16_t	device_id;
	uint16_t	DDK_ver;
} PACKED le_header_t;

#endif
