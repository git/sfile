/*
 * Segmented New Executable (NE)
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef NE_H
#define NE_H

#include "types.h"

/*
 * New Executable header (NE)
 */

#define NE_HDRSZ		sizeof(ne_header_t)

#define NE_SIGN			"NE"	/* 0x454E */

/*
 * OS Type
 */

#define NE_OS_UNKNOWN		0
#define NE_OS_OS2		1
#define NE_OS_WINDOWS		2
#define NE_OS_EURODOS4		3
#define NE_OS_WINDOWS386	4
#define NE_OS_BOSS		5
#define NE_OS_PL286_OS2		0x81
#define NE_OS_PL286_WINDOWS	0x82

/*
 * NE program flags
 */

#define NE_PROG_DGROUP_MASK	3
#define NE_PROG_DGROUP_NONE	0
#define NE_PROG_DGROUP_SINGLE	1
#define NE_PROG_DGROUP_MULTIPLE	2
#define NE_PROG_DGROUP_NULL	3

#define NE_PROG_GLOBAL_INIT	4
#define NE_PROG_PM		8

#define NE_PROG_MACH_SHIFT	4
#define NE_PROG_MACH_MASK	0xf0
#define NE_PROG_MACH_8086	1
#define NE_PROG_MACH_80286	2
#define NE_PROG_MACH_80386	4
#define NE_PROG_MACH_80x86	8

/*
 * NE application flags
 */

#define NE_APP_TYPE_MASK		7
#define NE_APP_TYPE_FULL_SCREEN		1
#define NE_APP_TYPE_WINDOWS_API_COMPAT	2
#define NE_APP_TYPE_WINDOWS_API_USED	3

#define NE_APP_FAMILY		8
#define NE_APP_EXEC_STATE	0x20
#define NE_APP_NO_EXEC		0x40
#define NE_APP_DLL_DRV		0x80

/*
 * NE other flags
 */

#define NE_EXE_LFN		1
#define NE_EXE_PM_2		2
#define NE_EXE_PF_2		4
#define NE_EXE_GANGLOAD		8

typedef struct {
	uint16_t	sign;
	uint8_t		linker_ver_hi;
	uint8_t		linker_ver_lo;
	uint16_t	entry_table_offs;
	uint16_t	entry_table_size;
	uint32_t	fcrc;
	uint8_t		prog_flags;
	uint8_t		app_flags;
	uint16_t	data_seg_index;
	uint16_t	ini_heap_size;
	uint16_t	ini_stack_size;
	uint16_t	ip;
	uint16_t	cs;
	uint16_t	sp;
	uint16_t	ss;
	uint16_t	seg_count;
	uint16_t	module_ref_count;
	uint16_t	nonresident_name_table_size;
	uint16_t	seg_table_offs;
	uint16_t	resource_table_offs;
	uint16_t	resident_name_table_offs;
	uint16_t	module_ref_table_offs;
	uint16_t	imported_name_table_offs;
	uint32_t	nonresident_name_table_offs;
	uint16_t	entry_table_moveable_count;
	uint16_t	align_shift;
	uint16_t	resource_table_entries;
	uint8_t		os_type;
	uint8_t		exe_flags;
	union {
		struct {
			uint16_t	offs;
			uint16_t	size;
		} gangload;
		struct {
			uint16_t	return_offs;
			uint16_t	seg_ref;
		} thunks;
	} misc;
	uint16_t	min_code_swap_size;
	uint8_t		windows_ver_lo;
	uint8_t		windows_ver_hi;
} PACKED ne_header_t;

#endif
