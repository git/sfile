/*
 * Turbo Pascal Unit (TPU) library
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include "subfile.h"
#include "tpu.h"

static struct subfile_sign tpu_sign[] = {
	{.si_id = TPU_SIGN, .si_size = 3,},
	{NULL}
};

struct subfile tpu_subfile = {
	.sf_type = sf_type_exec_lib,
	.sf_desc = "Turbo Pascal Unit (Library)",
	.sf_sign = tpu_sign,
};
