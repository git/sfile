/*
 * DOS executable
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef DOS_H
#define DOS_H

#include "types.h"

/*
 * DOS header
 */

#define DOS_HDRSZ		sizeof(dos_header_t)

#define DOS_SIGN1		"MZ"	/* 0x5A4D */
#define DOS_SIGN2		"ZM"	/* 0x4D5A */

typedef struct {			/* Offset */
	uint16_t	sign;			/* 0x00 */
	uint16_t	last_page_size;		/* 0x02 */
	uint16_t	total_page_size;	/* 0x04 */
	uint16_t	reloc_num;		/* 0x06 */
	uint16_t	size;			/* 0x08 */
	uint16_t	min_mem;		/* 0x0A */
	uint16_t	max_mem;		/* 0x0C */
	uint16_t	ss;			/* 0x0E */
	uint16_t	sp;			/* 0x10 */
	uint16_t	chksum;			/* 0x12 */
	uint16_t	ip;			/* 0x14 */
	uint16_t	cs;			/* 0x16 */
	uint16_t	reloc_offs;		/* 0x18 */
	uint16_t	overlay;		/* 0x1A */
	uint8_t		opt_info[32];		/* 0x1C */
	uint32_t	ne_hdr_offs;		/* 0x3C */
} PACKED dos_header_t;

#endif
