/*
 * Executable and Linkable Format (ELF)
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include "endian.h"
#include "subfile.h"
#include "elf.h"

typedef uint16_t elftoh16_func(uint16_t value);
typedef uint32_t elftoh32_func(uint32_t value);
typedef uint64_t elftoh64_func(uint64_t value);

static uint16_t notoh16(uint16_t value) { return value; }
static uint32_t notoh32(uint32_t value) { return value; }
static uint64_t notoh64(uint64_t value) { return value; }

struct elf_ops {
	elftoh16_func *elftoh16;
	elftoh32_func *elftoh32;
	elftoh64_func *elftoh64;
} elf_no_ops = {
	.elftoh16 = notoh16,
	.elftoh32 = notoh32,
	.elftoh64 = notoh64,
}, elf_be_ops = {
	.elftoh16 = betoh16,
	.elftoh32 = betoh32,
	.elftoh64 = betoh64,
}, elf_le_ops = {
	.elftoh16 = letoh16,
	.elftoh32 = letoh32,
	.elftoh64 = letoh64,
};

static void
elf32_dumper(file_t *file, struct elf_ops *ops, elf32_header_t *hdr)
{
	file_add_entry(file, "processor flags", "%#x", ops->elftoh32(hdr->flags));
	file_add_entry(file, "entry point", "%ju", (uintmax_t)ops->elftoh32(hdr->entry));
	file_add_entry(file, "prog hdr table offs", "%ju", (uintmax_t)ops->elftoh32(hdr->prog_hdr_offs));
	file_add_entry(file, "prog hdr table ent size", "%u", ops->elftoh16(hdr->prog_hdr_entry_size));
	file_add_entry(file, "prog hdr table ent num", "%u", ops->elftoh16(hdr->prog_hdr_entry_count));
	file_add_entry(file, "sect hdr table offs", "%ju", (uintmax_t)ops->elftoh32(hdr->sect_hdr_offs));
	file_add_entry(file, "sect hdr table ent size", "%u", ops->elftoh16(hdr->sect_hdr_entry_size));
	file_add_entry(file, "sect hdr table ent num", "%u", ops->elftoh16(hdr->sect_hdr_entry_count));
	file_add_entry(file, "sect hdr str table index", "%u", ops->elftoh16(hdr->sect_hdr_string_index));
}

static void
elf64_dumper(file_t *file, struct elf_ops *ops, elf64_header_t *hdr)
{
	file_add_entry(file, "processor flags", "%#x", ops->elftoh32(hdr->flags));
	file_add_entry(file, "entry point", "%ju", (uintmax_t)ops->elftoh64(hdr->entry));
	file_add_entry(file, "prog hdr table offs", "%ju", (uintmax_t)ops->elftoh64(hdr->prog_hdr_offs));
	file_add_entry(file, "prog hdr table ent size", "%u", ops->elftoh16(hdr->prog_hdr_entry_size));
	file_add_entry(file, "prog hdr table ent num", "%u", ops->elftoh16(hdr->prog_hdr_entry_count));
	file_add_entry(file, "sect hdr table offs", "%ju", (uintmax_t)ops->elftoh64(hdr->sect_hdr_offs));
	file_add_entry(file, "sect hdr table ent size", "%u", ops->elftoh16(hdr->sect_hdr_entry_size));
	file_add_entry(file, "sect hdr table ent num", "%u", ops->elftoh16(hdr->sect_hdr_entry_count));
	file_add_entry(file, "sect hdr str table index", "%u", ops->elftoh16(hdr->sect_hdr_string_index));
}

static uint32_t
elf_dumper(file_t *file)
{
	struct elf_ops *elf_ops;
	elf32_header_t *hdr = (elf32_header_t *)file->sf_buf;
	elf64_header_t *hdr64 = (elf64_header_t *)file->sf_buf;
	uint16_t hdr_size;
	string_t desc_endian, desc_bits;
	string_t desc;

	file_add_entry(file, "elf header signature", "%#x (%.4s)", hdr->sign,
	       (string_t)&(hdr->sign));

	switch (hdr->byte_order) {
	case ELF_ORDER_NONE:
		elf_ops = &elf_no_ops;
		desc_endian = "invalid";
		break;
	case ELF_ORDER_2LSB:
		elf_ops = &elf_le_ops;
		desc_endian = "little-endian (LSB first)";
		break;
	case ELF_ORDER_2MSB:
		elf_ops = &elf_be_ops;
		desc_endian = "big-endian (MSB first)";
		break;
	default:
		elf_ops = &elf_no_ops;
		desc_endian = "unknown";
		break;
	}

	switch (hdr->bits) {
	case ELF_BITS_NONE:
		hdr_size = 0;
		desc_bits = "invalid";
		break;
	case ELF_BITS_32:
		hdr_size = elf_ops->elftoh16(hdr->size);
		desc_bits = "32 bits";
		break;
	case ELF_BITS_64:
		hdr_size = elf_ops->elftoh16(hdr64->size);
		desc_bits = "64 bits";
		break;
	default:
		hdr_size = 0;
		desc_bits = "unknown";
		break;
	}

	file_add_entry(file, "elf header size", "%u", hdr_size);

	file_add_entry(file, "byte order", "%s", desc_endian);
	file_add_entry(file, "machine bits", "%s", desc_bits);

	switch (elf_ops->elftoh16(hdr->type)) {
	case ELF_TYPE_NONE	: desc = "no type"; break;
	case ELF_TYPE_RELOC	: desc = "relocatable"; break;
	case ELF_TYPE_EXEC	: desc = "executable"; break;
	case ELF_TYPE_DYN	: desc = "shared"; break;
	case ELF_TYPE_CORE	: desc = "core"; break;
	case ELF_TYPE_LOPROC	:
	case ELF_TYPE_HIPROC	: desc = "processor specific"; break;
	default			: desc = "unknown"; break;
	}
	file_add_entry(file, "object type", "%s (%#x)", desc, elf_ops->elftoh16(hdr->type));

	switch (elf_ops->elftoh16(hdr->machine)) {
	case ELF_MACH_NONE	: desc = "no machine"; break;
	case ELF_MACH_M32	: desc = "AT&T WE 32100"; break;
	case ELF_MACH_ALPHA	: desc = "Alpha"; break;
	case ELF_MACH_SH	: desc = "SuperH"; break;
	case ELF_MACH_SPARC	: desc = "Sun Sparc"; break;
	case ELF_MACH_SPARC32	: desc = "Sun Sparc v8 plus"; break;
	case ELF_MACH_SPARC64	: desc = "Sun Sparc v9 64-bits"; break;
	case ELF_MACH_68K	: desc = "Motorola m68k family"; break;
	case ELF_MACH_88K	: desc = "Motorola m88k family"; break;
	case ELF_MACH_IA_386	: desc = "Intel x86 32-bits family"; break;
	case ELF_MACH_AMD64	: desc = "AMD64 x86 64-bits family"; break;
	case ELF_MACH_IA_MCU	: desc = "Intel MCU"; break;
	case ELF_MACH_IA_860	: desc = "Intel 80860"; break;
	case ELF_MACH_IA_960	: desc = "Intel 80960"; break;
	case ELF_MACH_IA64	: desc = "Intel Itanium"; break;
	case ELF_MACH_MIPS	: desc = "Mips R3000"; break;
	case ELF_MACH_MIPS_R4	: desc = "Mips R4000"; break;
	case ELF_MACH_PARISC	: desc = "HPPA"; break;
	case ELF_MACH_PPC_OLD	: desc = "PowerPC old"; break;
	case ELF_MACH_PPC	: desc = "PowerPC"; break;
	case ELF_MACH_PPC64	: desc = "PowerPC 64-bits"; break;
	case ELF_MACH_PPC_CYGNUS: desc = "PowerPC Cygnus back-end"; break;
	case ELF_MACH_S390	: desc = "IBM S390"; break;
	case ELF_MACH_SPU	: desc = "IBM SPU/SPC"; break;
	case ELF_MACH_ARM	: desc = "ARM"; break;
	case ELF_MACH_ARM64	: desc = "ARM64"; break;
	case ELF_MACH_NIOS2	: desc = "Altera NIOS II"; break;
	case ELF_MACH_RISCV	: desc = "RISC-V"; break;
	case ELF_MACH_TILEGX	: desc = "Tilera TILE-Gx"; break;
	case ELF_MACH_TILE64	: desc = "Tilera TILE64"; break;
	case ELF_MACH_TILEPRO	: desc = "Tilera TILEPro"; break;
	default			: desc = "unknown"; break;
	}
	file_add_entry(file, "machine type", "%s (%#x)", desc, elf_ops->elftoh16(hdr->machine));

	switch (hdr->os_abi) {
	case ELF_OSABI_SYSV	: desc = "Unix System V"; break;
	case ELF_OSABI_HPUX	: desc = "HPUX"; break;
	case ELF_OSABI_NETBSD	: desc = "NetBSD"; break;
	case ELF_OSABI_GNU	: desc = "GNU"; break;
	case ELF_OSABI_SOLARIS	: desc = "Solaris"; break;
	case ELF_OSABI_AIX	: desc = "Aix"; break;
	case ELF_OSABI_IRIX	: desc = "Irix"; break;
	case ELF_OSABI_FREEBSD	: desc = "FreeBSD"; break;
	case ELF_OSABI_TRU64	: desc = "Tru64"; break;
	case ELF_OSABI_MODESTO	: desc = "Modesto"; break;
	case ELF_OSABI_OPENBSD	: desc = "OpenBSD"; break;
	case ELF_OSABI_ARM	: desc = "ARM"; break;
	case ELF_OSABI_ARM_EABI	: desc = "ARM EABI"; break;
	case ELF_OSABI_STANDALONE	: desc = "Standalone"; break;
	default			: desc = "unknown"; break;
	}
	file_add_entry(file, "os abi", "%s", desc);

	if (hdr->ver_type == ELF_VER_CURRENT)
		file_add_entry(file, "version", "%u", elf_ops->elftoh32(hdr->ver));

	switch (hdr->bits) {
	case ELF_BITS_32:
		elf32_dumper(file, elf_ops, hdr);
		break;
	case ELF_BITS_64:
		elf64_dumper(file, elf_ops, hdr64);
		break;
	default:
		break;
	}

	return 0;
}

static struct subfile_sign elf_sign[] = {
	{.si_id = ELF_SIGN, .si_size = 4,},
	{NULL}
};

struct subfile elf_subfile = {
	.sf_type = sf_type_exec,
	.sf_desc = "ELF (Executable and Linkable Format)",
	.sf_sign = elf_sign,
	.sf_size = ELF_HDRSZ,
	.sf_dumper = elf_dumper,
};
