/*
 * DJGPP dynamic executable
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef	DXE_H
#define	DXE_H

#include "types.h"

/*
 * DXE header
 */

#define	DXE_HDRSZ		sizeof(dxe_header_t)

#define	DXE_SIGN		"DXE1"  /* 0x31455844L */

typedef struct {
	uint32_t	sign;
	uint32_t	sym_offs;
	uint32_t	item_size;
	uint32_t	reloc_num;
} PACKED dxe_header_t;

#endif
