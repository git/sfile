/*
 * Unix COFF executable
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef COFF_H
#define COFF_H

#include "types.h"

/*
 * COFF header
 */

#define COFF_HDRSZ		sizeof(coff_header_t)
#define COFF_OPT_HDRSZ		sizeof(coff_opt_header_t)

#define COFF_MACH_UNKNOWN	"\x00\x00"	/* 0x0000 */
#define COFF_MACH_LYNX		"\x0d\x01"	/* 0x010d | 0415 */
#define COFF_MACH_I386		"\x4C\x01"	/* 0x014C */
#define COFF_MACH_I860		"\x4D\x01"	/* 0x014D */
#define COFF_MACH_I386PTX	"\x54\x01"	/* 0x0154 */
#define COFF_MACH_R3000		"\x62\x01"	/* 0x0162 */
#define COFF_MACH_R4000		"\x66\x01"	/* 0x0166 */
#define COFF_MACH_R10000	"\x68\x01"	/* 0x0168 */
#define COFF_MACH_ALPHA		"\x84\x01"	/* 0x0184 */
#define COFF_MACH_I386AIX	"\x75\x01"	/* 0x0175 */
#define COFF_MACH_ARM		"\xC0\x01"	/* 0x01C0 */
#define COFF_MACH_POWERPC	"\xF0\x01"	/* 0x01F0 */
#define COFF_MACH_IA64		"\x00\x02"	/* 0x0200 */
#define COFF_MACH_AMD64		"\x64\x86"	/* 0x8664 */

#define COFF_FLAG_STRIP_RELOC	0x0001
#define COFF_FLAG_EXEC_IMAGE	0x0002
#define COFF_FLAG_STRIP_LNNO	0x0004
#define COFF_FLAG_STRIP_LSYM	0x0008
#define COFF_FLAG_AGGRO_WS_TRIM	0x0010
#define COFF_FLAG_LARGE_ADDR	0x0020
#define COFF_FLAG_16BIT		0x0040
#define COFF_FLAG_BYTES_RES_LO	0x0040
#define COFF_FLAG_32BIT		0x0100
#define COFF_FLAG_STRIP_DEBUG	0x0200
#define COFF_FLAG_SYSTEM	0x1000
#define COFF_FLAG_DLL		0x2000
#define COFF_FLAG_UP_SYS_ONLY	0x4000
#define COFF_FLAG_BYTES_RES_HI	0x8000

typedef struct {
	uint16_t	machine;
	uint16_t	sect_num;
	uint32_t	tdstamp;
	uint32_t	sym_offs;
	uint32_t	sym_num;
	uint16_t	opthdr_size;
	uint16_t	flags;
} PACKED coff_header_t;

#define COFF_OPT_SIGN_PE32	0x010b
#define COFF_OPT_SIGN_PE64	0x020b
#define COFF_OPT_SIGN_ROM	0x0107

/*
 * coff optional header (a.out header)
 */
typedef struct {
	uint16_t	sign;
	uint16_t	ver;
	uint32_t	text_size;
	uint32_t	data_size;
	uint32_t	bss_size;
	uint32_t	entry;
	uint32_t	text_start;
	uint32_t	data_start;
} PACKED coff_opt_header_t;

#define COFF_SECT_TEXT		".text"
#define COFF_SECT_DATA		".data"
#define COFF_SECT_BSS		".bss"
#define COFF_SECT_COMMENT	".comment"
#define COFF_SECT_LIB		".lib"

typedef struct {
	uint8_t		name[8];
	uint32_t	paddr;
	uint32_t	vaddr;
	uint32_t	size;
	uint32_t	sect_offs;
	uint32_t	reloc_offs;
	uint32_t	lnno_offs;
	uint16_t	reloc_entries;
	uint16_t	lnno_entries;
	uint32_t	flags;
} PACKED coff_sect_header_t;

typedef struct {
	union {
		uint32_t	sym_index;
		uint32_t	paddr;
	} addr;
	uint16_t	lnno;
} PACKED coff_lnno_entry_t;

typedef struct {
	union {
		char		name[8];
		struct {
			uint32_t	zeroes;
			uint32_t	offs;
		} e;
	} e;
	uint32_t	value;
	uint16_t	sect_num;
	uint16_t	type;
	uint8_t		sclass;
	uint8_t		aux_num;
} PACKED coff_sym_entry_t;

typedef struct {
	uint32_t	vaddr;
	uint32_t	sym_index;
	uint16_t	type;
} PACKED coff_reloc_entry_t;

#endif
