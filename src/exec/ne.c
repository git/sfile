/*
 * Segmented New Executable (NE)
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include "subfile.h"
#include "ne.h"

static uint32_t
ne_dumper(file_t *file)
{
	ne_header_t *hdr = (ne_header_t *)file->sf_buf;
	uint16_t os2 = 0, windows = 0, gangload = 0;
	string_t desc;

	if (hdr->align_shift == 0)
		hdr->align_shift = 9;
	if (hdr->exe_flags & NE_EXE_GANGLOAD)
		gangload = 1;

	file_add_entry(file, "ne header signature", "%#x (%.2s)", hdr->sign,
	               (char *)&(hdr->sign));
	switch (hdr->os_type) {
	case NE_OS_UNKNOWN	: desc = "unknown"; break;
	case NE_OS_OS2		: desc = "OS2"; os2 = 1; break;
	case NE_OS_WINDOWS	: desc = "Windows"; windows = 1; break;
	case NE_OS_EURODOS4	: desc = "European Dos 4.0"; break;
	case NE_OS_WINDOWS386	: desc = "Windows 386"; break;
	case NE_OS_BOSS		: desc = "Borland Operating System Services"; break;
	case NE_OS_PL286_OS2	: desc = "Pharlap 286 Dos-Extender / OS2 "; os2 = 1; break;
	case NE_OS_PL286_WINDOWS: desc = "Pharlap 286 Dos-Extender / Windows"; windows = 1; break;
	default			: desc = "unknown"; break;
	}
	file_add_entry(file, "operating system", "%s", desc);
	file_add_entry(file, "linker version ", "%u.%u",
	               hdr->linker_ver_hi, hdr->linker_ver_lo);
	file_add_entry(file, "entry table offset", "%u", hdr->entry_table_offs);
	file_add_entry(file, "entry table size", "%u", hdr->entry_table_size);
	file_add_entry(file, "file load crc", "%u", hdr->fcrc);
	switch (hdr->prog_flags & NE_PROG_DGROUP_MASK) {
	case NE_PROG_DGROUP_NONE	: desc = "none"; break;
	case NE_PROG_DGROUP_SINGLE	: desc = "single shared"; break;
	case NE_PROG_DGROUP_MULTIPLE	: desc = "multiple unshared"; break;
	case NE_PROG_DGROUP_NULL	: desc = "null"; break;
	}
	file_add_entry(file, "dgroup type", "%s", desc);
	file_add_entry(file, "global initialization", "%s",
	       (hdr->prog_flags & NE_PROG_GLOBAL_INIT) ? "yes": "no");
	file_add_entry(file, "protected mode only", "%s",
	       (hdr->prog_flags & NE_PROG_PM) ? "yes": "no");
	switch ((hdr->prog_flags & NE_PROG_MACH_MASK) >> NE_PROG_MACH_SHIFT) {
	case NE_PROG_MACH_8086		: desc = "8086"; break;
	case NE_PROG_MACH_80286		: desc = "80286"; break;
	case NE_PROG_MACH_80386		: desc = "80386"; break;
	case NE_PROG_MACH_80x86		: desc = "80x86"; break;
	default				: desc = "invalid"; break;
	}
	file_add_entry(file, "processor instructions", "%s", desc);
	switch (hdr->app_flags & NE_APP_TYPE_MASK) {
	case NE_APP_TYPE_FULL_SCREEN		: desc = "full screen"; break;
	case NE_APP_TYPE_WINDOWS_API_COMPAT	: desc = "compatible with Windows/PM API"; break;
	case NE_APP_TYPE_WINDOWS_API_USED	: desc = "uses Windows/PM API"; break;
	default					: desc = "unknown"; break;
	}
	file_add_entry(file, "application type", "%s", desc);
	if (os2)
		file_add_entry(file, "family application", "%s", (hdr->app_flags & NE_APP_FAMILY) ? "yes":"no");
	file_add_entry(file, "executable state", "%s", (hdr->app_flags & NE_APP_EXEC_STATE) ? "errors in image":"executable");
	file_add_entry(file, "executable is conforming", "%s", (hdr->app_flags & NE_APP_NO_EXEC) ? "no":"yes");
	file_add_entry(file, "executable type", "%s", (hdr->app_flags & NE_APP_DLL_DRV) ? "dll/driver":"executable");

	file_add_entry(file, "automatic data segment index", "%u", hdr->data_seg_index);
	file_add_entry(file, "initial local heap size", "%u", hdr->ini_heap_size);
	file_add_entry(file, "initial stack size", "%u", hdr->ini_stack_size);
	file_add_entry(file, "entry point", "%04x:%04x", hdr->cs, hdr->ip);
	file_add_entry(file, "stack pointer", "%04x:%04x", hdr->ss, hdr->sp);
	file_add_entry(file, "segment count", "%u", hdr->seg_count);
	file_add_entry(file, "segment table offset", "%u", hdr->seg_table_offs + file->f_offs);
	file_add_entry(file, "resource table entries", "%u", hdr->resource_table_entries);
	file_add_entry(file, "resource table offset", "%u", hdr->resource_table_offs + file->f_offs);
	file_add_entry(file, "resident names table offset", "%u", hdr->resident_name_table_offs + file->f_offs);
	file_add_entry(file, "module reference count", "%u", hdr->module_ref_count);
	file_add_entry(file, "module reference table offset", "%u", hdr->module_ref_table_offs + file->f_offs);
	file_add_entry(file, "imported names table offset", "%u", hdr->imported_name_table_offs + file->f_offs);
	file_add_entry(file, "nonresident names table size", "%u", hdr->nonresident_name_table_size);
	file_add_entry(file, "nonresident names table offset", "%u", hdr->nonresident_name_table_offs);
	file_add_entry(file, "moveable entry point in entry table", "%u", hdr->entry_table_moveable_count);
	file_add_entry(file, "file alignment size shift count", "%u", hdr->align_shift);
	file_add_entry(file, "support long file names", "%s", (hdr->exe_flags & NE_EXE_LFN) ? "yes":"no");
	file_add_entry(file, "2.X protected mode", "%s", (hdr->exe_flags & NE_EXE_PM_2) ? "yes":"no");
	file_add_entry(file, "2.X proportional font", "%s", (hdr->exe_flags & NE_EXE_PF_2) ? "yes":"no");
	if (gangload) {
		file_add_entry(file, "gangload area offset", "%u", hdr->misc.gangload.offs);
		file_add_entry(file, "gangload area size", "%u", hdr->misc.gangload.size);
	} else {
		file_add_entry(file, "thunks return offset", "%u", hdr->misc.thunks.return_offs);
		file_add_entry(file, "thunks segment reference offset", "%u", hdr->misc.thunks.seg_ref);
	}
	file_add_entry(file, "minimum code swap area size", "%u", hdr->min_code_swap_size);
	if (windows)
		file_add_entry(file, "windows version", "%u.%u",
		               hdr->windows_ver_hi, hdr->windows_ver_lo);

	return 0;
}

static struct subfile_sign ne_sign[] = {
	{.si_id = NE_SIGN, .si_size = 2,},
	{NULL}
};

struct subfile ne_subfile = {
	.sf_type = sf_type_exec,
	.sf_desc = "Segmented New Executable",
	.sf_sign = ne_sign,
	.sf_size = NE_HDRSZ,
	.sf_dumper = ne_dumper,
};
