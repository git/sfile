/*
 * Unix COFF executable
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include <time.h>
#include "subfile.h"
#include "coff.h"

static uint32_t
coff_dumper(file_t *file)
{
	coff_header_t *hdr = (coff_header_t *)file->sf_buf;
	coff_opt_header_t *ohdr = (coff_opt_header_t *)(file->sf_buf + COFF_HDRSZ);
	time_t t = (time_t)hdr->tdstamp;
	char ts[32];

	file_add_entry(file, "coff header signature", "%#x", hdr->machine);
	printf(" flags			: <|");
	if (hdr->flags & COFF_FLAG_EXEC_IMAGE)
		printf("exec image|");
	if (hdr->flags & COFF_FLAG_STRIP_RELOC)
		printf("strip reloc|");
	if (hdr->flags & COFF_FLAG_STRIP_LNNO)
		printf("strip lnno|");
	if (hdr->flags & COFF_FLAG_STRIP_LSYM)
		printf("strip lsym|");
	printf(">\n");
	file_add_entry(file, "number of sections", "%u", hdr->sect_num);
	if (strftime(ts, sizeof(ts), "%c", gmtime(&t)))
		file_add_entry(file, "time & date stamp", "%s", ts);
	file_add_entry(file, "symbol table offs", "%u", hdr->sym_offs);
	file_add_entry(file, "symbol items", "%u", hdr->sym_num);
	file_add_entry(file, "optional header size", "%u", hdr->opthdr_size);

	if (hdr->opthdr_size > 0) {
		file_add_entry(file, "aout header signature", "%#x", ohdr->sign);
		file_add_entry(file, "version stamp", "%u", ohdr->ver);
		file_add_entry(file, "section (.text) size", "%u", ohdr->text_size);
		file_add_entry(file, "section (.data) size", "%u", ohdr->data_size);
		file_add_entry(file, "section (.bss) size", "%u", ohdr->bss_size);
		file_add_entry(file, "entry point", "%u", ohdr->entry);
		file_add_entry(file, "section (.text) start", "%u", ohdr->text_start);
		file_add_entry(file, "section (.data) start", "%u", ohdr->data_start);
	}

	return 0;
}

static struct subfile_sign coff_sign[] = {
	{.si_id = COFF_MACH_I386, .si_size = 2,},
	{.si_id = COFF_MACH_I386PTX, .si_size = 2,},
	{.si_id = COFF_MACH_I386AIX, .si_size = 2,},
	{.si_id = COFF_MACH_LYNX, .si_size = 2,},
	{NULL}
};

struct subfile coff_subfile = {
	.sf_type = sf_type_exec,
	.sf_desc = "Unix COFF executable",
	.sf_sign = coff_sign,
	.sf_size = COFF_HDRSZ + COFF_OPT_HDRSZ,
	.sf_dumper = coff_dumper,
};
