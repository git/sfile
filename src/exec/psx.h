/*
 * Playstation executable
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2002, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef	PSX_H
#define	PSX_H

#include "types.h"

/*
 * PSX header
 */

#define	PSX_HDRSZ		sizeof(psx_header_t)

#define	PSX_SIGN		"PS-X EXE"

typedef struct {
	uint8_t		sign[8];	// 0
	uint8_t		res1[8];	// 8 -> 0
	uint32_t	entry_addr;	// 10
	uint32_t	res2;		// 14 -> 0
	uint32_t	load_addr;	// 18
	uint32_t	text_size;	// 1c
	uint32_t	res3[4];	// 20 -> 0
	uint32_t	stack_addr;	// 30
	uint8_t		res4[0x18];	// 34 -> 0
	uint8_t		disclaimer[48];	// 4c
//	uint8_t		territory;	// 71
	uint8_t		res5[1924];	// 7c -> 0
} PACKED psx_header_t;

#endif
