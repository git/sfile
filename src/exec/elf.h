/*
 * Executable and Linkable Format (ELF)
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2003, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef ELF_H
#define ELF_H

#include "types.h"

/*
 * ELF header
 */

#define ELF_HDRSZ		sizeof(elf64_header_t)

#define ELF_SIGN		"\177ELF"
#define ELF_BSIGN		0x464c457f

#define ELF_BITS_NONE		0
#define ELF_BITS_32		1
#define ELF_BITS_64		2

#define ELF_ORDER_NONE		0
#define ELF_ORDER_2LSB		1
#define ELF_ORDER_2MSB		2

#define ELF_TYPE_NONE		0
#define ELF_TYPE_RELOC		1
#define ELF_TYPE_EXEC		2
#define ELF_TYPE_DYN		3
#define ELF_TYPE_CORE		4
#define ELF_TYPE_LOPROC		0xFF00
#define ELF_TYPE_HIPROC		0xFFFF

#define ELF_MACH_NONE		0
#define ELF_MACH_M32		1
#define ELF_MACH_SPARC		2
#define ELF_MACH_IA_386		3		/* Intel 80386 */
#define ELF_MACH_68K		4
#define ELF_MACH_88K		5
#define ELF_MACH_IA_MCU		6		/* Intel MCU */
#define ELF_MACH_IA_860		7		/* Intel 80860 */
#define ELF_MACH_MIPS		8
#define ELF_MACH_S370		9		/* IBM System/370 */
#define ELF_MACH_MIPS_R4	10
#define ELF_MACH_SPARC64_OLD	11
#define ELF_MACH_PARISC		15
#define ELF_MACH_PPC_OLD	17
#define ELF_MACH_SPARC32	18
#define ELF_MACH_IA_960		19		/* Intel 80960 */
#define ELF_MACH_PPC		20		/* PowerPC */
#define ELF_MACH_PPC64		21		/* PowerPC 64-bit */
#define ELF_MACH_S390		22		/* IBM S390 */
#define ELF_MACH_SPU		23		/* IBM SPU/SPC */
#define ELF_MACH_ARM		40		/* ARM */
#define ELF_MACH_ALPHA_OLD	41		/* Digital Alpha */
#define ELF_MACH_SH		42		/* Renesas (Hitachi) SuperH */
#define ELF_MACH_SPARC64	43		/* Sparc v9 64bit */
#define ELF_MACH_IA64		50
#define ELF_MACH_AMD64		62
#define ELF_MACH_NIOS2		113		/* Altera NIOS II */
#define ELF_MACH_ARM64		183		/* ARM 64 (AARCH64) */
#define ELF_MACH_AVR32		185		/* Amtel 32bit µ-processor */
#define ELF_MACH_TILE64		187		/* Tilera TILE64 */
#define ELF_MACH_TILEPRO	188		/* Tilera TILEPro */
#define ELF_MACH_MICROBLAZE	189		/* Xilinx MicroBlaze */
#define ELF_MACH_CUDA		190		/* NVIDIA CUDA */
#define ELF_MACH_TILEGX		191		/* Tilera TILE-Gx */
#define ELF_MACH_RISCV		243		/* RISC-V */
#define ELF_MACH_PPC_CYGNUS	0x9025
#define ELF_MACH_ALPHA		0x9026

#define ELF_VER_NONE		0
#define ELF_VER_CURRENT		1
#define ELF_VER_NUM		2

#define ELF_OSABI_NONE		0		/* UNIX System V ABI */
#define ELF_OSABI_SYSV		ELF_OSABI_NONE	/* Alias */
#define ELF_OSABI_HPUX		1		/* HP-UX */
#define ELF_OSABI_NETBSD	2		/* NetBSD */
#define ELF_OSABI_GNU		3		/* GNU */
#define ELF_OSABI_LINUX		ELF_OSABI_GNU	/* Alias */
#define ELF_OSABI_SOLARIS	6		/* Sun Solaris */
#define ELF_OSABI_AIX		7		/* IBM AIX */
#define ELF_OSABI_IRIX		8		/* SGI Irix */
#define ELF_OSABI_FREEBSD	9		/* FreeBSD */
#define ELF_OSABI_TRU64		10		/* Compaq TRU64 UNIX */
#define ELF_OSABI_MODESTO	11		/* Novell Modesto */
#define ELF_OSABI_OPENBSD	12		/* OpenBSD */
#define ELF_OSABI_ARM_EABI	64		/* ARM */
#define ELF_OSABI_ARM		97		/* ARM */
#define ELF_OSABI_STANDALONE	255		/* Standalone (embedded) application */

typedef struct {
	uint32_t	sign;
	uint8_t		bits;
	uint8_t		byte_order;
	uint8_t		ver_type;
	uint8_t		os_abi;
	uint8_t		ver_abi;
	uint8_t		pad[7];
	uint16_t	type;
	uint16_t	machine;
	uint32_t	ver;
	uint32_t	entry;
	uint32_t	prog_hdr_offs;
	uint32_t	sect_hdr_offs;
	uint32_t	flags;
	uint16_t	size;
	uint16_t	prog_hdr_entry_size;
	uint16_t	prog_hdr_entry_count;
	uint16_t	sect_hdr_entry_size;
	uint16_t	sect_hdr_entry_count;
	uint16_t	sect_hdr_string_index;
} PACKED elf32_header_t;

typedef struct {
	uint32_t	sign;
	uint8_t		bits;
	uint8_t		byte_order;
	uint8_t		ver_type;
	uint8_t		os_abi;
	uint8_t		ver_abi;
	uint8_t		pad[7];
	uint16_t	type;
	uint16_t	machine;
	uint32_t	ver;
	uint64_t	entry;
	uint64_t	prog_hdr_offs;
	uint64_t	sect_hdr_offs;
	uint32_t	flags;
	uint16_t	size;
	uint16_t	prog_hdr_entry_size;
	uint16_t	prog_hdr_entry_count;
	uint16_t	sect_hdr_entry_size;
	uint16_t	sect_hdr_entry_count;
	uint16_t	sect_hdr_string_index;
} PACKED elf64_header_t;

#define	ELF_PROG_TYPE_NULL	0
#define	ELF_PROG_TYPE_LOAD	1
#define	ELF_PROG_TYPE_DYN	2
#define	ELF_PROG_TYPE_INTERP	3
#define	ELF_PROG_TYPE_NOTE	4
#define	ELF_PROG_TYPE_SHLIB	5
#define	ELF_PROG_TYPE_PHDR	6
#define	ELF_PROG_TYPE_LOPROC	0x70000000L
#define	ELF_PROG_TYPE_HIPROC	0x7fffffffL

#define	ELF_PROG_FLAG_EXEC	1
#define	ELF_PROG_FLAG_WRIT	2
#define	ELF_PROG_FLAG_READ	4
#define	ELF_PROG_FLAG_MASKPROC	0xf0000000L

typedef struct {
	uint32_t     type;
	uint32_t     offs;
	uint32_t     vaddr;
	uint32_t     padrr;
	uint32_t     fsize;
	uint32_t     msize;
	uint32_t     flags;
	uint32_t     align;
} PACKED elf32_prog_header_t;

typedef struct {
	uint32_t     type;
	uint32_t     flags;
	uint64_t     offs;
	uint64_t     vaddr;
	uint64_t     padrr;
	uint64_t     fsize;
	uint64_t     msize;
	uint64_t     align;
} PACKED elf64_prog_header_t;

typedef struct {
	uint32_t     name;
	uint32_t     type;
	uint32_t     flags;
	uint32_t     vaddr;
	uint32_t     offs;
	uint32_t     size;
	uint32_t     index_next;
	uint32_t     info;
	uint32_t     align;
	uint32_t     entry_size;
} PACKED elf32_sect_header_t;

typedef struct {
	uint32_t     name;
	uint32_t     type;
	uint64_t     flags;
	uint64_t     vaddr;
	uint64_t     offs;
	uint64_t     size;
	uint32_t     index_next;
	uint32_t     info;
	uint64_t     align;
	uint64_t     entry_size;
} PACKED elf64_sect_header_t;

typedef struct {
	uint32_t     name;
	uint32_t     value;
	uint32_t     size;
	uint8_t      info;
	uint8_t      other;
	uint16_t     sect_index;
} PACKED elf32_sym_entry_t;

typedef struct {
	uint32_t     name;
	uint8_t      info;
	uint8_t      other;
	uint16_t     sect_index;
	uint64_t     value;
	uint64_t     size;
} PACKED elf64_sym_entry_t;

typedef struct {
	uint32_t	name_size;
	uint32_t	descriptor_size;
	uint32_t	type;
	uint8_t		name[1];
} PACKED elf_note_seg_t;

typedef struct {
	uint32_t	offs;
	uint32_t	info;
} PACKED elf32_reloc_entry_t;

typedef struct {
	uint64_t	offs;
	uint64_t	info;
} PACKED elf64_reloc_entry_t;

typedef struct {
	uint32_t	offs;
	uint32_t	info;
	int32_t		addend;
} PACKED elf32_reloca_entry_t;

typedef struct {
	uint64_t	offs;
	uint64_t	info;
	int64_t		addend;
} PACKED elf64_reloca_entry_t;

typedef struct {
	int32_t		tag;
	union {
		uint32_t	val;
		uint32_t	ptr;
	} dyn;
} PACKED elf32_dyn_sect_t;

typedef struct {
	int64_t		tag;
	union {
		uint64_t	val;
		uint64_t	ptr;
	} dyn;
} PACKED elf64_dyn_sect_t;

#endif
