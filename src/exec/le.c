/*
 * OS/2 Linear Executable (LE)
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include "subfile.h"
#include "le.h"

static uint32_t
le_dumper(struct file *file)
{
	le_header_t *hdr = (le_header_t *)file->sf_buf;
	uint16_t dll = 0, prog = 0, windows = 0, os2 = 0;
	string_t desc;

	if (hdr->sign == OS_WINDOWS)
		windows = 1;
	else if (hdr->sign == OS_OS2)
		os2 = 1;

	file_add_entry(file, "le header signature", "%#x (%.2s)", hdr->sign, (string_t)&(hdr->sign));
	file_add_entry(file, "byte order", "%s endian", hdr->byte_order ? "big":"little");
	file_add_entry(file, "word order", "%s endian", hdr->word_order ? "big":"little");
	file_add_entry(file, "executable format level", "%u", hdr->exe_fmt_level);
	switch (hdr->cpu_type) {
	case LE_MACH_286	: desc = "Intel 80286"; break;
	case LE_MACH_386	: desc = "Intel 80386"; break;
	case LE_MACH_486	: desc = "Intel 80486"; break;
	case LE_MACH_PENTIUM	: desc = "Intel Pentium"; break;
	case LE_MACH_860	: desc = "Intel i860 (N10)"; break;
	case LE_MACH_N11	: desc = "Intel N11"; break;
	case LE_MACH_MIPS1	: desc = "MIPS Mark I (R2000, R3000)"; break;
	case LE_MACH_MIPS2	: desc = "MIPS Mark II (R6000)"; break;
	case LE_MACH_MIPS3	: desc = "MIPS Mark III (R4000)"; break;
	default			: desc = "unknown";
	}
	file_add_entry(file, "cpu type", "%s", desc);
	switch (hdr->os_type) {
	case LE_OS_OS2		: desc = "OS/2"; break;
	case LE_OS_WINDOWS	: desc = "Windows"; break;
	case LE_OS_EURODOS4	: desc = "European DOS 4.0"; break;
	case LE_OS_WINDOWS386	: desc = "Windows 386"; break;
	default			: desc = "unknown";
	}
	file_add_entry(file, "target os ", "%s", desc);
	file_add_entry(file, "module version", "%u", hdr->module_ver);
	switch ((hdr->module_type & LE_MT_MASK) >> LE_MT_SHIFT) {
	case LE_MT_PROG		: desc = "program"; prog = 1; break;
	case LE_MT_DLL		: desc = "dynamic link library"; dll = 1; break;
	case LE_MT_PDLL		: desc = "protected memory library"; dll = 1; break;
	case LE_MT_PHIS		: desc = "phisycal device driver"; break;
	case LE_MT_VIRT		: desc = "virtual device driver"; break;
	default			: desc = "unknown";
	}
	file_add_entry(file, "module type", "%s", desc);
	if (prog) {
		file_add_entry(file, "module loadable", "%s", (hdr->module_type & LE_NO_LOAD_MASK) ? "no":"yes");
		switch ((hdr->module_type & LE_API_MASK) >> LE_API_SHIFT) {
		case LE_API_UNKNOWN		: desc = "unknown"; break;
		case LE_API_PM_WINDOWS_INCOMPAT	: desc = "incompatible with PM windowing"; break;
		case LE_API_PM_WINDOWS_COMPAT	: desc = "compatible with PM windowing"; break;
		case LE_API_PM_WINDOWS_USED	: desc = "uses PM windowing API"; break;
		default				: desc = "unknown";
		}
		file_add_entry(file, "api compatibility", "%s", desc);
	} else if (dll) {
		file_add_entry(file, "initialization", "%s", (hdr->module_type & LE_INIT_MASK) ? "per process":"global");
		file_add_entry(file, "termination", "%s", (hdr->module_type & LE_TERM_MASK) ? "per process":"none");
	}
	file_add_entry(file, "internal exec fixups", "%s", (hdr->module_type & LE_NO_INT_FIXUPS_MASK) ? "no":"yes");
	file_add_entry(file, "external exec fixups", "%s", (hdr->module_type & LE_NO_EXT_FIXUPS_MASK) ? "no":"yes");
	file_add_entry(file, "memory pages", "%u", hdr->mem_page_num);
	file_add_entry(file, "memory page size", "%u", hdr->mem_page_size);
	file_add_entry(file, "initial cs:eip", "%04x:%08x", hdr->cs, hdr->eip);
	file_add_entry(file, "initial ss:esp", "%04x:%08x", hdr->ss, hdr->esp);
	if (windows)
		file_add_entry(file, "bytes on last page", "%u", hdr->page.last_size);
	else if (os2)
		file_add_entry(file, "page offset shift count", "%u", hdr->page.offs_shift);
	file_add_entry(file, "fixup section size ", "%u", hdr->fixup_size);
	file_add_entry(file, "fixup section checksum", "%u", hdr->fixup_chksum);
	file_add_entry(file, "loader section size ", "%u", hdr->loader_size);
	file_add_entry(file, "loader section checksum", "%u", hdr->loader_chksum);
	file_add_entry(file, "object table offset", "%u", hdr->object_table_offs);
	file_add_entry(file, "object table entries", "%u", hdr->object_table_entries);
	file_add_entry(file, "object page map table offset", "%u", hdr->object_page_map_table_offs);
	file_add_entry(file, "object iterate data map offset", "%u", hdr->object_iterate_dmap_offs);
	file_add_entry(file, "resource table offset", "%u", hdr->resource_table_offs);
	file_add_entry(file, "resource table entries", "%u", hdr->resource_table_entries);
	file_add_entry(file, "resident names table offset", "%u", hdr->resident_name_table_offs);
	file_add_entry(file, "entry table offset", "%u", hdr->entry_table_offs);
	file_add_entry(file, "module directives table offset", "%u", hdr->module_directives_table_offs);
	file_add_entry(file, "module directives table entries", "%u", hdr->module_directives_table_entries);
	file_add_entry(file, "fixup page table offset", "%u", hdr->fixup_page_table_offs);
	file_add_entry(file, "fixup record table offset", "%u", hdr->fixup_record_table_offs);
	file_add_entry(file, "import module name table offset", "%u", hdr->imported_module_name_table_offs);
	file_add_entry(file, "import module count", "%u", hdr->imported_module_count);
	file_add_entry(file, "import proc name table offset", "%u", hdr->imported_procedure_name_table_offs);
	file_add_entry(file, "per page checksum table offset", "%u", hdr->per_page_chksum_table_offs);
	file_add_entry(file, "data pages offset", "%u", hdr->data_page_offs);
	file_add_entry(file, "preload page count", "%u", hdr->preload_page_count);
	file_add_entry(file, "nonresident name table offset", "%u", hdr->nonresident_name_table_offs);
	file_add_entry(file, "nonresident name table size", "%u", hdr->nonresident_name_table_size);
	file_add_entry(file, "nonresident name checksum", "%u", hdr->nonresident_name_chksum);
	file_add_entry(file, "aoutomatic data object", "%u", hdr->autodata_object);
	file_add_entry(file, "debug information offset", "%u", hdr->debug_info_offs);
	file_add_entry(file, "debug information size", "%u", hdr->debug_info_size);
	file_add_entry(file, "preload instance pages number", "%u", hdr->preload_instance_page_num);
	file_add_entry(file, "demand instance pages number", "%u", hdr->demand_instance_page_num);
	file_add_entry(file, "extra heap allocation", "%u", hdr->extra_heap_alloc);
	if (windows) {
		file_add_entry(file, "VERSIONINFO resource offset", "%u", hdr->ver_info_resource_offs);
		file_add_entry(file, "device id", "%u", hdr->device_id);
		file_add_entry(file, "DDK version", "%u", hdr->DDK_ver);
	}

	return 0;
}

static struct subfile_sign wx_sign[] = {
	{.si_id = W3_SIGN, .si_size = 2,},
	{.si_id = W4_SIGN, .si_size = 2,},
	{NULL}
};


struct subfile wx_subfile = {
	.sf_type = sf_type_exec,
	.sf_desc = "Linear executable Collection",
	.sf_sign = wx_sign,
};

static struct subfile_sign le_sign[] = {
	{.si_id = LE_SIGN, .si_size = 2,},
	{.si_id = LX_SIGN, .si_size = 2,},
	{NULL}
};

struct subfile le_subfile = {
	.sf_type = sf_type_exec,
	.sf_desc = "Linear executable",
	.sf_sign = le_sign,
	.sf_size = LE_HDRSZ,
	.sf_dumper = le_dumper,
};
