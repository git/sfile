/*
 * DJGPP dynamic executable
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include "subfile.h"
#include "dxe.h"

static uint32_t
dxe_dumper(file_t *file)
{
	dxe_header_t *hdr = (dxe_header_t *)file->sf_buf;

	file_add_entry(file, "dxe header signature", "%#x (%.4s)", hdr->sign,
	               (char *)&(hdr->sign));
	file_add_entry(file, "symbol offs", "%u", hdr->sym_offs);
	file_add_entry(file, "item size", "%u", hdr->item_size);
	file_add_entry(file, "num relocations", "%u", hdr->reloc_num);

	return 0;
}

static struct subfile_sign dxe_sign[] = {
	{.si_id = DXE_SIGN, .si_size = 4,},
	{NULL}
};

struct subfile dxe_subfile = {
	.sf_type = sf_type_exec,
	.sf_desc = "DJGPP Dynamic executable",
	.sf_sign = dxe_sign,
	.sf_size = DXE_HDRSZ,
	.sf_dumper = dxe_dumper,
};
