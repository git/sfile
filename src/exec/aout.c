/*
 * Unix a.out executable
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include "subfile.h"
#include "aout.h"

static uint32_t
aout_dumper(file_t *file)
{
	aout_header_t *hdr = (aout_header_t *)file->sf_buf;

	file_add_entry(file, "aout header signature", "%#o", hdr->sign);
	file_add_entry(file, "version stamp", "%u", hdr->ver);
	file_add_entry(file, "section (.text) size", "%u", hdr->text_size);
	file_add_entry(file, "section (.data) size", "%u", hdr->data_size);
	file_add_entry(file, "section (.bss) size", "%u", hdr->bss_size);
	file_add_entry(file, "section (.syms) size", "%u", hdr->syms_size);
	file_add_entry(file, "entry point", "%u", hdr->entry);
	file_add_entry(file, "text relocation size", "%u", hdr->text_reloc_size);
	file_add_entry(file, "data relocation size", "%u", hdr->data_reloc_size);

	return 0;
}

static struct subfile_sign aout_sign[] = {
	{.si_id = AOUT_QMAGIC, .si_size = 2,
	 .si_desc = "demand paged QMAGIC",},
	{.si_id = AOUT_COFF_STMAGIC, .si_size = 2,
	 .si_desc = "COFF target shared lib",},
	{.si_id = AOUT_COFF_OMAGIC, .si_size = 2,
	 .si_desc = "COFF object file",},
	{.si_id = AOUT_HPUX_OMAGIC, .si_size = 2,
	 .si_desc = "HPUX OMAGIC",},
	{.si_id = AOUT_A29K_SHMAGIC, .si_size = 2,
	 .si_desc = "A29K SHMAGIC",},
	{.si_id = AOUT_OMAGIC, .si_size = 2, .si_desc = "impure OMAGIC",},
	{.si_id = AOUT_NMAGIC, .si_size = 2, .si_desc = "pure NMAGIC",},
	{.si_id = AOUT_ZMAGIC, .si_size = 2, .si_desc = "demand paged ZMAGIC",},
	{.si_id = AOUT_BMAGIC, .si_size = 2, .si_desc = "b.out file",},
	{.si_id = AOUT_CMAGIC, .si_size = 2, .si_desc = "core file",},
	{.si_id = AOUT_COFF_SHMAGIC, .si_size = 2,
	 .si_desc = "host shared lib",},
	{.si_id = AOUT_DYNIX_OMAGIC, .si_size = 2,},
	{.si_id = AOUT_DYNIX_ZMAGIC, .si_size = 2,},
	{.si_id = AOUT_DYNIX_XMAGIC, .si_size = 2,},
	{.si_id = AOUT_DYNIX_SMAGIC, .si_size = 2,},
	{.si_id = AOUT_64_OMAGIC, .si_size = 2,},
	{.si_id = AOUT_64_ZMAGIC, .si_size = 2,},
	{.si_id = AOUT_64_NMAGIC, .si_size = 2,},
	{NULL}
};

struct subfile aout_subfile = {
	.sf_type = sf_type_exec,
	.sf_desc = "Unix a.out executable",
	.sf_sign = aout_sign,
	.sf_size = AOUT_HDRSZ,
	.sf_dumper = aout_dumper,
};
