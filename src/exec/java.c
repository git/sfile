/*
 * Java Class executable
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include "endian.h"
#include "subfile.h"
#include "java.h"

static uint32_t
java_dumper(file_t *file)
{
	java_header_t *hdr = (java_header_t *)file->sf_buf;

	file_add_entry(file, "java header signature", "%#x", be32toh(hdr->sign));
	file_add_entry(file, "java compiler version", "%u.%u",
	               be16toh(hdr->ver_hi), be16toh(hdr->ver_lo));
	file_add_entry(file, "java const pool count", "%u", be16toh(hdr->const_pool_count));

	return 0;
}

static struct subfile_sign java_sign[] = {
	{.si_id = JAVA_SIGN, .si_size = 4,},
	{NULL}
};

struct subfile java_subfile = {
	.sf_type = sf_type_exec,
	.sf_desc = "Java Virtual Machine executable",
	.sf_ext = "class",
	.sf_sign = java_sign,
	.sf_size = JAVA_HDRSZ,
	.sf_dumper = java_dumper,
};
