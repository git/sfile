/*
 * DOS executable
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include <string.h>
#include "subfile.h"
#include "diags.h"
#include "dos.h"

static uint32_t pklite_helper(file_t *file, uint8_t *buf);
static uint32_t borland_helper(file_t *file, uint8_t *buf);

static struct subfile_sign dos_extra[] = {
	{.si_id = "LHarc's SFX ", .si_size = 12, .si_offs = 9,
	 .si_desc = "LHarc 1.x self-extracting archive"},
	{.si_id = "SFX by LARC", .si_size = 11, .si_offs = 4,
	 .si_desc = "LARC self-extracting archive"},
	{.si_id = "LHa's SFX ", .si_size = 10, .si_offs = 8,
	 .si_desc = "LHA 2.10 self-extracting archive"},
	{.si_id = "LHA's SFX ", .si_size = 10, .si_offs = 8,
	 .si_desc = "LHA 2.13 self-extracting archive"},
	{.si_id = "LH's SFX", .si_size = 8, .si_offs = 8,
	 .si_desc = "LH self-extracting archive"},
	{.si_id = "PKWARE", .si_size = 6, .si_offs = 32,
	 .si_desc = "PK self-extracting archive"},
	{.si_id = "PKLITE", .si_size = 6, .si_offs = 2,
	 .si_desc = "PKLITE compressed executable",
	 .si_helper = pklite_helper},
	{.si_id = "diet\xf9\x9c", .si_size = 6, .si_offs = 0,
	 .si_desc = "diet compressed executable"},
	{.si_id = "\x01\x00\x8a\x01\x65\x15", .si_size = 6, .si_offs = 0,
	 .si_desc = "TopSpeed C 3.0 CRUNCH compressed file"},
	{.si_id = "\x01\x00\x02\x00\x00\x07", .si_size = 6, .si_offs = 0,
	 .si_desc = "PKARCK 3.5 self-extracting archive"},
	{.si_id = "AIN2", .si_size = 4, .si_offs = 4,
	 .si_desc = "AIN compressed executable"},
	{.si_id = "RSFX", .si_size = 4, .si_offs = 0,
	 .si_desc = "RAR self-extracting archive"},
	{.si_id = "LZ09", .si_size = 4, .si_offs = 0,
	 .si_desc = "LZEXE 0.90 compressed executable"},
	{.si_id = "LZ91", .si_size = 4, .si_offs = 0,
	 .si_desc = "LZEXE 0.91 compressed executable"},
	{.si_id = "RJSX", .si_size = 4, .si_offs = 0,
	 .si_desc = "ARJ self-extracting archive"},
	{.si_id = "\x0f\x00\xa7", .si_size = 3, .si_offs = 0,
	 .si_desc = "BSA (Soviet Archiver)"},
	{.si_id = "\xfb", .si_size = 1, .si_offs = 2,
	 .si_desc = "Borland TLINK",
	 .si_helper = borland_helper},
	{NULL}
};

static uint32_t
pklite_helper(file_t *file, uint8_t *buf)
{
	int flags = *(buf + 1) & 0xF0;
	unsigned int ver_major = *(buf + 1) & 0xF;
	unsigned int ver_minor = *buf;

	file_add_entry(file, "compressor version", "%u.%u", ver_major, ver_minor);
	file_add_entry(file, "extra compression", "%s", flags & 0x10 ? "yes" : "no");
	file_add_entry(file, "huge file ", "%s", flags & 0x20 ? "yes" : "no");

	return 0;
}

static uint32_t
borland_helper(file_t *file, uint8_t *buf)
{
	unsigned int ver_major = (*(buf + 3) >> 4) & 0xF;
	unsigned int ver_minor = *(buf + 3) & 0xF;

	file_add_entry(file, "compiler version", "%u.%u", ver_major, ver_minor);

	return 0;
}

/* FIXME: This should be replaced by the program wide signature processing.
 */
static uint16_t
dos_efind(file_t *file, void *buf)
{
	uint16_t i;

	for (i = 0; dos_extra[i].si_id ; i++) {
		if (!memcmp(buf + dos_extra[i].si_offs, dos_extra[i].si_id,
			    dos_extra[i].si_size)) {
			file_add_entry(file, "extended dos file type", "%s",
			       dos_extra[i].si_desc);

			if (dos_extra[i].si_helper != NULL)
				dos_extra[i].si_helper(file, (uint8_t *)buf);

			return i;
		}
	}

	return 0;
}

static uint32_t
dos_dumper(file_t *file)
{
	dos_header_t *hdr = (dos_header_t *)file->sf_buf;
	uint32_t image_size, header_size;

	image_size = (hdr->total_page_size << 9) - hdr->last_page_size;
	header_size = hdr->size << 4;

	if ((image_size > file->f_size) || (header_size > image_size))
		warning("invalid dos header");

	if (hdr->last_page_size == 4)
		hdr->last_page_size = 0;

	file_add_entry(file, "dos header signature", "%#x (%.2s)", hdr->sign, (string_t)&(hdr->sign));
	file_add_entry(file, "dos header size", "%u", header_size);
	file_add_entry(file, "image size", "%u", image_size);
	file_add_entry(file, "min required memory", "%u", hdr->min_mem << 4);
	file_add_entry(file, "max required memory", "%u", hdr->max_mem << 4);
	file_add_entry(file, "relocatable table offset", "%#x (%u)",
	               hdr->reloc_offs, hdr->reloc_offs);
	file_add_entry(file, "relocatable items", "%u", hdr->reloc_num);
	file_add_entry(file, "initial cs:ip", "%04x:%04x", hdr->cs, hdr->ip);
	file_add_entry(file, "initial ss:sp", "%04x:%04x", hdr->ss, hdr->sp);
	file_add_entry(file, "checksum", "%u", hdr->chksum);
	file_add_entry(file, "overlay number", "%u", hdr->overlay);

	dos_efind(file, &hdr->opt_info);
	if (image_size && image_size < file->f_size)
		file_push_offs(file, file->f_offs + image_size);
	if ((hdr->ne_hdr_offs >= 0x40) && hdr->ne_hdr_offs &&
	    (hdr->ne_hdr_offs < file->f_size)) {
		file_add_entry(file, "new exe header offs", "%#x (%u)", hdr->ne_hdr_offs, hdr->ne_hdr_offs);
		file_push_offs(file, file->f_offs + hdr->ne_hdr_offs);
	}
	return 0;
}

static struct subfile_sign dos_sign[] = {
	{.si_id = DOS_SIGN1, .si_size = 2,},
	{.si_id = DOS_SIGN2, .si_size = 2,},
	{NULL}
};

struct subfile dos_subfile = {
	.sf_type = sf_type_exec,
	.sf_desc = "DOS executable",
	.sf_sign = dos_sign,
	.sf_size = DOS_HDRSZ,
	.sf_dumper = dos_dumper,
};
