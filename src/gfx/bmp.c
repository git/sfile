/*
 * Windows Bitmap (BMP)
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include "subfile.h"
#include "bmp.h"

static uint32_t
bmp_dumper(file_t *file)
{
	bmp_header_t *hdr = (bmp_header_t *)file->sf_buf;
	bmp_info_header_t *info = (bmp_info_header_t *)(file->sf_buf + BMP_HDRSZ);

	file_add_entry(file, "bmp file signature", "%#x", hdr->sign);
	file_add_entry(file, "bmp file size", "%u", hdr->fsize);
	file_add_entry(file, "image offset", "%#x", hdr->data_offs);

	file_add_entry(file, "info hdr size", "%u", info->size);
	file_add_entry(file, "image width", "%u", info->width);
	file_add_entry(file, "image height", "%u", info->height);
	file_add_entry(file, "image planes", "%u", info->planes);
	file_add_entry(file, "image bpp", "%u", info->bpp);
	file_add_entry(file, "image compression", "%u", info->compression);
	file_add_entry(file, "image size", "%u", info->size);
	file_add_entry(file, "image x pixels/meter", "%i", info->x_pixels_meter);
	file_add_entry(file, "image y pixels/meter", "%i", info->y_pixels_meter);
	file_add_entry(file, "image colors used", "%u", info->colors_used);
	file_add_entry(file, "image colors important", "%u", info->colors_important);

	return 0;
}

/*
static uint32_t
ico_cur_dumper(file_t *file)
{
	ico_cur_header_t *hdr = (ico_cur_header_t *)file->sf_buf;

	return 0;
}
*/

static struct subfile_sign bmp_sign[] = {
	{.si_id = BMP_SIGN, .si_size = 2,},
	{NULL}
};

struct subfile bmp_subfile = {
	.sf_type = sf_type_gfx,
	.sf_desc = "Windows Bitmap (BMP)",
	.sf_sign = bmp_sign,
	.sf_size = BMP_HDRSZ+BMP_INFO_HDRSZ,
	.sf_dumper = bmp_dumper,
};

struct subfile ico_cur_subfile = {
	.sf_type = sf_type_gfx,
	.sf_desc = "Windows icon/cursor (ICO/CUR)",
	.sf_ext = "ico;cur",
	.sf_size = ICO_CUR_HDRSZ,
//	.sf_dumper = ico_cur_dumper,
};
