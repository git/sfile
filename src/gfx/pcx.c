/*
 * PC Paintbrush file (PCX)
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include "subfile.h"
#include "pcx.h"

static uint32_t
pcx_dumper(file_t *file)
{
	pcx_header_t *hdr = (pcx_header_t *)file->sf_buf;
	string_t tmp;

	switch (hdr->manufacturer) {
	case PCX_MAN_ZSOFT	: tmp = "Zsoft"; break;
	default			: tmp = "unknown"; break;
	}
	file_add_entry(file, "manufacturer", "%s", tmp);

	switch (hdr->ver) {
	case PCX_VER_2_5	: tmp = "2.5 PC Paintbrush"; break;
	case PCX_VER_2_8_PAL	: tmp = "2.8 w/ palette info"; break;
	case PCX_VER_2_8_NOPAL	: tmp = "2.8 w/o palette info"; break;
	case PCX_VER_WINDOWS	: tmp = "PC Paintbrush for Windows"; break;
	case PCX_VER_3_0	: tmp = "3.0+ PC Paintbush (Plus)"; break;
	default			: tmp = "unknown"; break;
	}
	file_add_entry(file, "version", "%s", tmp);

	switch (hdr->encoding) {
	case PCX_ENC_RLE	: tmp = "run length encoding (RLE)"; break;
	default			: tmp = "unknown"; break;
	}
	file_add_entry(file, "encoding", "%s", tmp);
	file_add_entry(file, "bits per pixel", "%hu", hdr->bpp);
	file_add_entry(file, "initial X", "%u", hdr->xini);
	file_add_entry(file, "initial Y", "%u", hdr->yini);
	file_add_entry(file, "final X", "%u", hdr->xend);
	file_add_entry(file, "final Y", "%u", hdr->yend);
	file_add_entry(file, "horizontal dpi", "%u", hdr->h_dpi);
	file_add_entry(file, "vertical dpi", "%u", hdr->v_dpi);
	file_add_entry(file, "color planes", "%hu", hdr->planes);
	file_add_entry(file, "bytes per line", "%u", hdr->line_bytes);

	switch (hdr->palette_info) {
	case PCX_PAL_INFO_COLOR_BW	: tmp = "color/BW"; break;
	case PCX_PAL_INFO_GRAYSCALE	: tmp = "grayscale"; break;
	default				: tmp = "unknown"; break;
	}
	file_add_entry(file, "palette info", "%s", tmp);
	file_add_entry(file, "horizontal screen size", "%u", hdr->h_screen_size);
	file_add_entry(file, "vertical screen size", "%u", hdr->v_screen_size);

	return 0;
}

/* XXX: Disabled: the signature is too short (just one byte) to avoid
 * collisions with tons of stuff. We use exclusively the extension. */
#if 0
static struct subfile_sign pcx_sign[] = {
	{.si_id = PCX_SIGN, .si_size = 1,},
	{NULL}
};
#endif

struct subfile pcx_subfile = {
	.sf_type = sf_type_gfx,
	.sf_desc = "PC Paintbrush bitmap format",
	.sf_ext = "pcx",
	.sf_size = PCX_HDRSZ,
	.sf_dumper = pcx_dumper,
};
