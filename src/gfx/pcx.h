/*
 * PC Paintbrush file (PCX)
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef PCX_H
#define PCX_H

#include "types.h"

#define PCX_HDRSZ		sizeof(pcx_header_t)

#define PCX_SIGN		"\x0a"

#define PCX_MAN_ZSOFT		10

#define PCX_VER_2_5		0
#define PCX_VER_2_8_PAL		2
#define PCX_VER_2_8_NOPAL	3
#define PCX_VER_WINDOWS		4
#define PCX_VER_3_0		5

#define PCX_ENC_RLE		1	/* Run length encoding */

#define PCX_PAL_INFO_COLOR_BW	1
#define PCX_PAL_INFO_GRAYSCALE	2

typedef struct {
	uint8_t		manufacturer;
	uint8_t		ver;
	uint8_t		encoding;
	uint8_t		bpp;
	uint16_t	xini;
	uint16_t	yini;
	uint16_t	xend;
	uint16_t	yend;
	uint16_t	h_dpi;
	uint16_t	v_dpi;
	uint8_t		palette[48];
	uint8_t		reserved1;
	uint8_t		planes;			/* Color planes */
	uint16_t	line_bytes;
	uint16_t	palette_info;
	uint16_t	h_screen_size;		/* In Pixels	*/
	uint16_t	v_screen_size;		/*  "    "	*/
	uint8_t		reserved2[54];
} PACKED pcx_header_t;

#endif
