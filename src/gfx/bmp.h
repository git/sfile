/*
 * Windows Bitmap (BMP)
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef BMP_H
#define BMP_H

#include "types.h"

#define BMP_HDRSZ	sizeof(bmp_header_t)
#define BMP_INFO_HDRSZ	sizeof(bmp_info_header_t)

#define BMP_SIGN	"BM"

typedef struct {
	uint16_t	sign;
	uint32_t	fsize;
	uint16_t	reserved1;
	uint16_t	reserved2;
	uint32_t	data_offs;
} PACKED bmp_header_t;

#define BMP_COMP_RGB	0
#define BMP_COMP_RLE8	1
#define BMP_COMP_RLE4	2

typedef struct {
	uint32_t	size;
	uint32_t	width;		/* Pixels */
	uint32_t	height;
	uint16_t	planes;
	uint16_t	bpp;
	uint32_t	compression;
	uint32_t	image_size;
	int32_t		x_pixels_meter;
	int32_t		y_pixels_meter;
	uint32_t	colors_used;
	uint32_t	colors_important;
} PACKED bmp_info_header_t;

#define ICO_CUR_HDRSZ	sizeof(ico_cur_header_t)

#define ICO_TYPE	1
#define CUR_TYPE	2

typedef struct {
	uint16_t	reserved;
	uint16_t	type;
	uint16_t	dir_entries;
} PACKED ico_cur_header_t;

typedef struct {
	uint8_t		width;
	uint8_t		height;
	uint8_t		colors;
	uint8_t		reserved;
	uint16_t	planes;
	uint16_t	bpp;
	uint32_t	image_size;
	uint32_t	image_offs;
} PACKED ico_dir_entry_t;

typedef struct {
	uint8_t		width;
	uint8_t		height;
	uint8_t		colors;
	uint8_t		reserved;
	uint16_t	x_hot_spot;
	uint16_t	y_hot_spot;
	uint32_t	image_size;
	uint32_t	image_offs;
} PACKED cur_dir_entry_t;

#endif
