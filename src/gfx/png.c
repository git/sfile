/*
 * Portable Network Graphics (PNG)
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include <string.h>
#include "endian.h"
#include "subfile.h"
#include "file.h"
#include "string.h"
#include "png.h"

static uint32_t
png_dumper(file_t *file)
{
	png_header_t *hdr = (png_header_t *)file->sf_buf;
	png_chunk_ihdr_t *ihdr = (png_chunk_ihdr_t *)(file->sf_buf + PNG_HDRSZ);
	string_t desc;
	char hex_sign[sizeof(hdr->sign) + 1];

	str_bin2hex(hex_sign, hdr->sign, sizeof(hdr->sign));
	file_add_entry(file, "png header signature", "0x%s (%.3s)",
	               hex_sign, &(hdr->sign[1]));

	if (!strncmp((char *)&ihdr->type, "IHDR", 4)) {
		file_add_entry(file, "png ihdr chunk size", "%u", be32toh(ihdr->size));
		file_add_entry(file, "png width", "%u", be32toh(ihdr->width));
		file_add_entry(file, "png height", "%u", be32toh(ihdr->height));
		file_add_entry(file, "png bit depth", "%hu", ihdr->bit_depth & 0xff);

		printf(" png color type		: ");
		if (ihdr->color_type & PNG_COLOR_TYPE_PALETTE)
			printf("palette ");
		if (ihdr->color_type & PNG_COLOR_TYPE_COLOR)
			printf("color ");
		if (ihdr->color_type & PNG_COLOR_TYPE_ALPHA)
			printf("alpha ");
		printf("\n");

		switch (ihdr->comp_method) {
		case PNG_COMPRESS_DEFLATE_INFLATE:
			desc = "deflate/inflate";
			break;
		default:
			desc = "unknown";
			break;
		}
		file_add_entry(file, "png compression method", "%s", desc);

		switch (ihdr->filter_method) {
		case PNG_FILTER_ADAPTATIVE	: desc = "adaptative"; break;
		default				: desc = "unknown"; break;
		}
		file_add_entry(file, "png filter method", "%s", desc);

		switch (ihdr->interlace_method) {
		case PNG_INTERLACE_NONE:
			desc = "none";
			break;
		case PNG_INTERLACE_ADAM7:
			desc = "adam7";
			break;
		default:
			desc = "unknown";
			break;
		}
		file_add_entry(file, "png interlace method", "%s", desc);

		file_add_entry(file, "png ihdr crc", "%#x", be32toh(ihdr->crc));
	}

	return 0;
}

static struct subfile_sign png_sign[] = {
	{.si_id = PNG_SIGN, .si_size = 8,},
	{NULL}
};

struct subfile png_subfile = {
	.sf_type = sf_type_gfx,
	.sf_desc = "Portable Network Graphics",
	.sf_ext = "png",
	.sf_sign = png_sign,
	.sf_size = PNG_HDRSZ + PNG_IHDR_SZ,
	.sf_dumper = png_dumper,
};
