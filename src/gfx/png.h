/*
 * Portable Network Graphics (PNG)
 *
 * Part of sfile (subfile structure)
 *
 * The file is encoded in big-endian, as stated in the standard.
 *
 * Copyright © 2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef PNG_H
#define PNG_H

#include "types.h"

#define PNG_HDRSZ	sizeof(png_header_t)
#define PNG_IHDR_SZ	sizeof(png_chunk_ihdr_t)

#define PNG_SIGN	"\x89PNG\x0d\x0a\x1a\x0a"

typedef struct {
	char	sign[8];
} PACKED png_header_t;

#define PNG_CHUNK_MAX_SIZE	(2<<31)-1

typedef struct {
	uint32_t	size;
	uint32_t	type;
/*	uint??_t	data;  current data with size = png_chunk.size */
	uint32_t	crc;
} PACKED png_chunk_t;

#define PNG_COLOR_TYPE_PALETTE	1
#define PNG_COLOR_TYPE_COLOR	2
#define PNG_COLOR_TYPE_ALPHA	4

#define PNG_COMPRESS_DEFLATE_INFLATE	0

#define PNG_FILTER_ADAPTATIVE	0

#define PNG_INTERLACE_NONE	0
#define PNG_INTERLACE_ADAM7	1

typedef struct {
	uint32_t	size;
	uint32_t	type;
	uint32_t	width;
	uint32_t	height;
	uint8_t		bit_depth;
	uint8_t		color_type;
	uint8_t		comp_method;
	uint8_t		filter_method;
	uint8_t		interlace_method;
	uint32_t	crc;
} PACKED png_chunk_ihdr_t;

typedef struct {
	uint8_t		red;
	uint8_t		green;
	uint8_t		blue;
} PACKED png_color_t;

typedef struct {
	uint32_t	size;
	uint32_t	type;
	png_color_t	color[0];
	uint32_t	crc;
} PACKED png_chunk_plte_t;

typedef struct {
	uint32_t	size;
	uint32_t	type;
	uint8_t		data[0];  /* current image data */
	uint32_t	crc;
} PACKED png_chunk_idat_t;

typedef struct {
	uint32_t	size;
	uint32_t	type;
/*	uint0_t		data;  // data field empty */
	uint32_t	crc;
} PACKED png_chunk_iend_t;

#endif
