/*
 * Autodesk Animation File (FLI, FLC)
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef FLI_H
#define FLI_H

#include "types.h"

#define FLI_HDRSZ       sizeof(fli_header_t)

#define FLI_SIGN        "\x11\xaf"
#define FLC_SIGN        "\x12\xaf"

#define FLI_BSIGN        0xaf11
#define FLC_BSIGN        0xaf12

#define MAX_FRAMES      4000

typedef struct {
	uint32_t	size;
	uint16_t	sign;
	uint16_t	frames;
	uint16_t	width;
	uint16_t	height;
	uint16_t	depth;
	uint16_t	flags;
	uint16_t	speed;
	uint32_t	next;
	uint32_t	frit;
	uint8_t		reserved[102];
} PACKED fli_header_t;

#endif
