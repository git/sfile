/*
 * Autodesk Animation File (FLI, FLC)
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include "subfile.h"
#include "fli.h"

static uint32_t
fli_dumper(file_t *file)
{
	fli_header_t *hdr = (fli_header_t *)file->sf_buf;
	string_t tmp;

	switch (hdr->sign) {
	case FLI_BSIGN	: tmp = "fli"; break;
	case FLC_BSIGN	: tmp = "flc"; break;
	default		: /*tmp = "invalid"*/ return -1;
	}

	file_add_entry(file, "%s file signature", "%#x", tmp, hdr->sign);
	file_add_entry(file, "%s file size", "%u", tmp, hdr->size);
	file_add_entry(file, "image width", "%u", hdr->width);
	file_add_entry(file, "image height", "%u", hdr->height);
	file_add_entry(file, "pixel depth", "%u", hdr->depth);
	file_add_entry(file, "flags", "%u", hdr->flags);
	file_add_entry(file, "playback speed", "%u", hdr->speed);
	file_add_entry(file, "next", "%u", hdr->next);
	file_add_entry(file, "frit", "%u", hdr->frit);

	return 0;
}

static struct subfile_sign fli_sign[] = {
	{.si_id = FLI_SIGN, .si_size = 2, .si_offs = 4,},
	{.si_id = FLC_SIGN, .si_size = 2, .si_offs = 4,},
	{NULL}
};

struct subfile fli_subfile = {
	.sf_type = sf_type_gfx,
	.sf_desc = "Autodesk animation file",
	.sf_ext = "fli;flc",
	.sf_sign = fli_sign,
	.sf_size = FLI_HDRSZ,
	.sf_dumper = fli_dumper,
};
