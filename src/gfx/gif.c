/*
 * Graphics Interchange Format (GIF)
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include "subfile.h"
#include "gif.h"

static uint32_t
gif_dumper(file_t *file)
{
	gif_header_t *hdr = (gif_header_t *)file->sf_buf;

	file_add_entry(file, "gif header signature", "%.3s", hdr->sign);
	file_add_entry(file, "gif version", "%.3s", hdr->ver);
	file_add_entry(file, "screen width", "%u", hdr->width);
	file_add_entry(file, "screen height", "%u", hdr->height);
	file_add_entry(file, "bits per pixel", "%u", (hdr->flags & GIF_FLAG_BITS) + 1);
	file_add_entry(file, "color resolution bits", "%u", ((hdr->flags & GIF_FLAG_RES_MASK) >> GIF_FLAG_RES_SHIFT) + 1);
	file_add_entry(file, "global color map", "%spresent", hdr->flags & GIF_FLAG_GLOBAL_MAP ? "" : "not ");
	file_add_entry(file, "map address", "%u", hdr->map);

	return 0;
}

static struct subfile_sign gif_sign[] = {
	{.si_id = GIF_SIGN, .si_size = 3,},
	{NULL}
};

struct subfile gif_subfile = {
	.sf_type = sf_type_gfx,
	.sf_desc = "Graphics Interchange Format",
	.sf_sign = gif_sign,
	.sf_size = GIF_HDRSZ,
	.sf_dumper = gif_dumper,
};
