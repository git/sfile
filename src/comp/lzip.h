/*
 * LZIP compressed file
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2014 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef SFILE_LZIP_H
#define SFILE_LZIP_H

#include "types.h"

#define LZIP_HDRSZ		sizeof(lzip_header_t)

#define LZIP_SIGN		"LZIP"

#define LZIP_DICT_BITS_MIN	12
#define LZIP_DICT_BITS_MAX	29
#define LZIP_DICT_SIZE_MIN	(1 << LZIP_DICT_BITS_MIN)
#define LZIP_DICT_SIZE_MAX	(1 << LZIP_DICT_BITS_MAX)

typedef struct {
	uint8_t		sign[4];
	uint8_t		version;
	uint32_t	dict_size;
} PACKED lzip_header_t;

#endif
