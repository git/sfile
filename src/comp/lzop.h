/*
 * LZOP compressed file
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2014 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef SFILE_LZOP_H
#define SFILE_LZOP_H

#include "types.h"

#define LZOP_HDRSZ		sizeof(lzop_header_t)

#define LZOP_SIGN		"\x89\x4c\x5a\x4f\x00\x0d\x0a\x1a\x0a"

#define LZOP_METHOD_LZO1X_1	1
#define LZOP_METHOD_LZO1X_1_15	2
#define LZOP_METHOD_LZO1X_999	3
#define LZOP_METHOD_NRV1A	0x1a
#define LZOP_METHOD_NRV1B	0x1b
#define LZOP_METHOD_NRV2A	0x2a
#define LZOP_METHOD_NRV2B	0x2b
#define LZOP_METHOD_NRV2D	0x2d
#define LZOP_METHOD_ZLIB	128

/* header flags */
#define LZOP_FLAG_ADLER32_D	0x00000001L
#define LZOP_FLAG_ADLER32_C	0x00000002L
#define LZOP_FLAG_STDIN		0x00000004L
#define LZOP_FLAG_STDOUT	0x00000008L
#define LZOP_FLAG_NAME_DEFAULT	0x00000010L
#define LZOP_FLAG_DOSISH	0x00000020L
#define LZOP_FLAG_H_EXTRA_FIELD	0x00000040L
#define LZOP_FLAG_H_GMTDIFF	0x00000080L
#define LZOP_FLAG_CRC32_D	0x00000100L
#define LZOP_FLAG_CRC32_C	0x00000200L
#define LZOP_FLAG_MULTIPART	0x00000400L
#define LZOP_FLAG_H_FILTER	0x00000800L
#define LZOP_FLAG_H_CRC32	0x00001000L
#define LZOP_FLAG_H_PATH	0x00002000L
#define LZOP_FLAG_MASK		0x00003fffL

/* operating system & file system that created the file [mostly unused] */
#define LZOP_FLAG_OS_FAT	0x00000000L /* DOS, OS2, Win95 */
#define LZOP_FLAG_OS_AMIGA	0x01000000L
#define LZOP_FLAG_OS_VMS	0x02000000L
#define LZOP_FLAG_OS_UNIX	0x03000000L
#define LZOP_FLAG_OS_VM_CMS	0x04000000L
#define LZOP_FLAG_OS_ATARI	0x05000000L
#define LZOP_FLAG_OS_OS2	0x06000000L /* OS2 */
#define LZOP_FLAG_OS_MAC9	0x07000000L
#define LZOP_FLAG_OS_Z_SYSTEM	0x08000000L
#define LZOP_FLAG_OS_CPM	0x09000000L
#define LZOP_FLAG_OS_TOPS20	0x0a000000L
#define LZOP_FLAG_OS_NTFS	0x0b000000L /* Win NT/2000/XP */
#define LZOP_FLAG_OS_QDOS	0x0c000000L
#define LZOP_FLAG_OS_ACORN	0x0d000000L
#define LZOP_FLAG_OS_VFAT	0x0e000000L /* Win32 */
#define LZOP_FLAG_OS_MFS	0x0f000000L
#define LZOP_FLAG_OS_BEOS	0x10000000L
#define LZOP_FLAG_OS_TANDEM	0x11000000L
#define LZOP_FLAG_OS_SHIFT	24
#define LZOP_FLAG_OS_MASK	0xff000000L

/* character set for file name encoding [mostly unused] */
#define LZOP_FLAG_CS_NATIVE	0x00000000L
#define LZOP_FLAG_CS_LATIN1	0x00100000L
#define LZOP_FLAG_CS_DOS	0x00200000L
#define LZOP_FLAG_CS_WIN32	0x00300000L
#define LZOP_FLAG_CS_WIN16	0x00400000L
#define LZOP_FLAG_CS_UTF8	0x00500000L  /* filename is UTF-8 encoded */
#define LZOP_FLAG_CS_SHIFT	20
#define LZOP_FLAG_CS_MASK	0x00f00000L

/* these bits must be zero */
#define LZOP_FLAG_RESERVED	((LZOP_FLAG_MASK | LZOP_FLAG_OS_MASK | LZOP_FLAG_CS_MASK) ^ 0xffffffffL)

typedef struct {
	uint8_t		sign[9];
	uint16_t	version;
	uint16_t	lib_version;
	uint16_t	min_version;
	uint8_t		method;
	uint8_t		level;
	uint32_t	flags;
} PACKED lzop_header_t;

#endif
