/*
 * RAR compressed file
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef RAR_H
#define RAR_H

#include "types.h"

#define RAR_HDRSZ	sizeof(rar_header_t)

#define RAR_SIGN        "Rar!\x1a\x07\x00"

#define RAR_ARCH_HDRSZ	sizeof(rar_arch_header_t)

typedef struct {
	uint16_t	crc;
	uint8_t		type;
	uint16_t	flags;
	uint16_t	size;
} PACKED rar_block_t;

#define RAR_CRC			0xEDB88320
#define RAR_METHOD_STORE	0x30
#define RAR_METHOD_FASTEST	0x31
#define RAR_METHOD_FAST		0x32
#define RAR_METHOD_NORMAL	0x33
#define RAR_METHOD_GOOD		0x34
#define RAR_METHOD_BEST		0x35

#define RAR_TYPE_MARK		0x72
#define RAR_TYPE_ARCH		0x73
#define RAR_TYPE_FILE		0x74
#define RAR_TYPE_COMM		0x75
#define RAR_TYPE_INF		0x76
#define RAR_TYPE_SUBBLK		0x77
#define RAR_TYPE_RECVRY		0x78

#define RAR_FLAG_NEW_BLOCK	0x4000
#define RAR_FLAG_OPT		0x8000

typedef struct {
	uint16_t	crc;
	uint8_t		type;
	uint16_t	flags;
	uint16_t	size;
} PACKED rar_header_t;

#define RAR_FLAG_ARCH_VOL	0x01
#define RAR_FLAG_ARCH_COMM	0x02
#define RAR_FLAG_ARCH_LOCK	0x04
#define RAR_FLAG_ARCH_SOLID	0x08
#define RAR_FLAG_ARCH_AV	0x20

typedef struct {
	uint16_t	crc;
	uint8_t		type;
	uint16_t	flags;
	uint16_t	size;
	uint16_t	reserved1;
	uint32_t	resreved2;
} PACKED rar_arch_header_t;

#define RAR_FLAG_FILE_PREV	0x01
#define RAR_FLAG_FILE_NEXT	0x02
#define RAR_FLAG_FILE_CRYPT	0x04
#define RAR_FLAG_FILE_COMM	0x08
#define RAR_FLAG_FILE_SOLID	0x10

#define RAR_FLAG_FILE_MASK	0xe0
#define RAR_FLAG_FILE_SHIFT	5
#define RAR_FLAG_FILE_D64	0x00
#define RAR_FLAG_FILE_D128	0x20
#define RAR_FLAG_FILE_D256	0x40
#define RAR_FLAG_FILE_D512	0x60
#define RAR_FLAG_FILE_D1024	0x80
#define RAR_FLAG_FILE_DIR	0xe0

#define RAR_OS_DOS		0
#define RAR_OS_OS2		1
#define RAR_OS_WINDOWS32	2
#define RAR_OS_UNIX		3

typedef struct {
	uint16_t	crc;
	uint8_t		type;
	uint16_t	flags;
	uint16_t	size;
	uint32_t	comp_size;
	uint32_t	uncomp_size;
	uint8_t		host_os;
	uint32_t	fcrc;
	uint32_t	tdstamp;
	uint8_t		ver;
	uint8_t		method;
	uint16_t	fname_size;
	uint32_t	fattr;
	char		fname[0];
} PACKED rar_file_header_t;

typedef struct {
	uint16_t	crc;
	uint8_t		type;
	uint16_t	flags;
	uint16_t	size;
	uint16_t	uncomp_size;
	uint8_t		ver;
	uint8_t		method;
	uint8_t		comm_crc;
	char		comment[0];
} PACKED rar_comm_header_t;

typedef struct {
	uint16_t	crc;
	uint8_t		type;
	uint16_t	flags;
	uint16_t	size;
	char		info[0];
} PACKED rar_inf_header_t;

typedef struct {
	uint16_t	crc;
	uint8_t		type;
	uint16_t	flags;
	uint16_t	size;
	uint32_t	data_size;
	uint16_t	sub_type;
	uint8_t		reserved;
} PACKED rar_subblk_header_t;

#endif
