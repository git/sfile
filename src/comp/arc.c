/*
 * ARC compressed file
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004, 2014 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include "subfile.h"
#include "endian.h"
#include "string.h"
#include "arc.h"

static uint32_t
arc_dumper(file_t *file)
{
	arc_header_t *hdr = (arc_header_t *)file->sf_buf;
	char ts[32];
	string_t desc;

	file_add_entry(file, "arc header signature", "%#x", hdr->sign);
	switch (hdr->method) {
	case ARC_METHOD_END_ARCHIVE	: desc = "end of archive"; break;
	case ARC_METHOD_UNPACKED_1_0	: desc = "unpacked (1.0)"; break;
	case ARC_METHOD_UNPACKED_3_1	: desc = "unpacked (3.1)"; break;
	case ARC_METHOD_PACKED		: desc = "packed"; break;
	case ARC_METHOD_SQUEEZED	: desc = "squeezed"; break;
	case ARC_METHOD_CRUNCHED_4_0	: desc = "crunched (4.0)"; break;
	case ARC_METHOD_CRUNCHED_4_1	: desc = "crunched (4.1)"; break;
	case ARC_METHOD_CRUNCHED_4_6	: desc = "crunched (4.6)"; break;
	case ARC_METHOD_CRUNCHED_5_0	: desc = "crunched (5.0)"; break;
	case ARC_METHOD_SQUASHED	: desc = "squashed"; break;
	case ARC_METHOD_CRUSHED		: desc = "crushed"; break;
	case ARC_METHOD_DISTILLED	: desc = "distilled"; break;
	default				:
		if (hdr->method < 20)
			desc = "arc (6.0)";
		else if (hdr->method < 30)
			desc = "information";
		else if (hdr->method < 40)
			desc = "control";
		else
			desc = "reserved";
	};
	file_add_entry(file, "arc compression method", "%u, %s",
	               hdr->method, desc);

	file_add_entry(file, "file name", "%.12s", hdr->fname);
	file_add_entry(file, "file size compressed", "%u", le32toh(hdr->comp_size));
	file_add_entry(file, "file size uncompressed", "%u", le32toh(hdr->uncomp_size));
	file_add_entry(file, "file timestamp", "%s", str_msdos_tdstamp(ts, le32toh(hdr->tdstamp)));
	file_add_entry(file, "file CRC-16", "%#hx", le16toh(hdr->crc));

	return 0;
}

static struct subfile_sign arc_sign[] = {
	{.si_id = ARC_SIGN, .si_size = 1,},
	{NULL}
};

struct subfile arc_subfile = {
	.sf_type = sf_type_comp,
	.sf_desc = "ARC compressed file",
	.sf_ext = "arc",
	.sf_sign = arc_sign,
	.sf_size = ARC_HDRSZ,
	.sf_dumper = arc_dumper,
};
