/*
 * ARJ compressed file
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <time.h>
#include "subfile.h"
#include "string.h"
#include "arj.h"

static uint32_t
arj_dumper(file_t *file)
{
	arj_header_t *hdr = (arj_header_t *)file->sf_buf;
	string_t desc;
	char ts[32];

	file_add_entry(file, "arj header signature", "%hu", hdr->sign);
	file_add_entry(file, "arj header size", "%hu", hdr->size);
	file_add_entry(file, "arj first header size", "%hu", hdr->first_hdr_size);

	file_add_entry(file, "program version", "%hu", hdr->prog_ver);
	file_add_entry(file, "min program version", "%hu", hdr->min_prog_ver);

	switch (hdr->os_type) {
	case ARJ_OS_MSDOS	: desc = "MSDOS"; break;
	case ARJ_OS_PRIMOS	: desc = "Primos"; break;
	case ARJ_OS_UNIX	: desc = "Unix"; break;
	case ARJ_OS_AMIGA	: desc = "Amiga"; break;
	case ARJ_OS_MACOS	: desc = "MacOS"; break;
	case ARJ_OS_OS2		: desc = "OS/2"; break;
	case ARJ_OS_APPLE_GS	: desc = "Apple GS"; break;
	case ARJ_OS_ATARI_ST	: desc = "Atari ST"; break;
	case ARJ_OS_NEXT	: desc = "NexT"; break;
	case ARJ_OS_VMS		: desc = "VAX/VMS"; break;
	default                 : desc = "unknown"; break;
	};
	file_add_entry(file, "OS type", "%s", desc);

	printf(" archive flags			: <|");
	if (hdr->flags & ARJ_FLAG_OLD_CRYPT)
		printf("old crypt|");
	if (hdr->flags & ARJ_FLAG_VOLUME)
		printf("volume|");
	if (hdr->flags & ARJ_FLAG_PATHSYM)
		printf("path sym|");
	if (hdr->flags & ARJ_FLAG_BACKUP)
		printf("backu|");
	if (hdr->flags & ARJ_FLAG_CRYPT)
		printf("crypt|");
	printf(">\n");

	file_add_entry(file, "security version", "%hu", hdr->security_ver);
	file_add_entry(file, "file type", "%hu", hdr->file_type);
	file_add_entry(file, "(reserved)", "%hu", hdr->reserved1);

	file_add_entry(file, "time & date create stamp", "%s", str_msdos_tdstamp(ts, hdr->tdstamp));
	file_add_entry(file, "time & date modify stamp", "%s", str_msdos_tdstamp(ts, hdr->tdmodify));

	file_add_entry(file, "file size", "%u", hdr->fsize);
	file_add_entry(file, "security offset", "%u", hdr->security_offs);
	file_add_entry(file, "fspec filename offset", "%u", hdr->fspec_fname_offs);
	file_add_entry(file, "security size", "%u", hdr->security_size);

	return 0;
}

static struct subfile_sign arj_sign[] = {
	{.si_id = ARJ_SIGN, .si_size = 2,},
	{NULL}
};

struct subfile arj_subfile = {
	.sf_type = sf_type_comp,
	.sf_desc = "ARJ compressed file",
	.sf_sign = arj_sign,
	.sf_size = ARJ_HDRSZ,
	.sf_dumper = arj_dumper,
};
