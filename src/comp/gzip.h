/*
 * GZIP compressed file
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef GZIP_H
#define GZIP_H

#include "types.h"

#define GZIP_HDRSZ		14

#define GZIP_SIGN		"\x1f\x8b"
#define GZIPOLD_SIGN		"\x1f\x9e"
#define LZW_SIGN		"\x1f\x9d"
#define LZH_SIGN		"\x1f\xa0"
#define PACK_SIGN		"\x1f\x1e"

#define GZIP_FLAG_ASCII		1
#define GZIP_FLAG_MULTI		2
#define GZIP_FLAG_EFIELD	4
#define GZIP_FLAG_FNAME		8
#define GZIP_FLAG_COMM		0x10
#define GZIP_FLAG_CRYPT		0x20

typedef struct {
	uint16_t	sign;
	uint8_t		method;
	uint8_t		flags;
	uint32_t	tdstamp;
	uint8_t		eflags;
	uint8_t		os_ver;
	uint16_t	part_id;
	uint16_t	efield_size;
	char		*efield;
	char		*fname;
	char		*fcomment;
} PACKED gzip_header_t;

#endif
