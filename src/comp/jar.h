/*
 * Java Archive compressed file (JAR)
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2000, 2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef JAR_H
#define JAR_H

#include "types.h"

#define JAR_HDRSZ	sizeof(jar_header_t)

#define JAR_SIGN        "\x1aJar\x1b\x00"

typedef struct {
	uint32_t	crc;
	uint8_t		reserved1[10];
	uint8_t		sign[6];
} PACKED jar_header_t;

#endif
