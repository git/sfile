/*
 * ARC compressed file
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004, 2014 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef ARC_H
#define ARC_H

#include "types.h"

#define ARC_HDRSZ		sizeof(arc_header_t)

#define ARC_SIGN		"x1A"
#define ARC_BSIGN		0x1a

#define ARC_METHOD_END_ARCHIVE	0
#define ARC_METHOD_UNPACKED_1_0	1
#define ARC_METHOD_UNPACKED_3_1	2
#define ARC_METHOD_PACKED	3
#define ARC_METHOD_SQUEEZED	4
#define ARC_METHOD_CRUNCHED_4_0	5
#define ARC_METHOD_CRUNCHED_4_1	6
#define ARC_METHOD_CRUNCHED_4_6	7
#define ARC_METHOD_CRUNCHED_5_0	8
#define ARC_METHOD_SQUASHED	9
#define ARC_METHOD_CRUSHED	10
#define ARC_METHOD_DISTILLED	11
#define ARC_METHOD_ARC_6_0	12 /* 12 <-> 19 */
#define ARC_METHOD_INFORMATION	20 /* 20 <-> 29 */
#define ARC_METHOD_CONTROL	30 /* 30 <-> 39 */
#define ARC_METHOD_RESERVED	40 /* 40 -> ... */

typedef struct {
	uint8_t		sign;
	uint8_t		method;
	uint8_t		fname[13];
	uint32_t	comp_size;
	uint32_t	tdstamp;	/* DOS format */
	uint16_t	crc;
	uint32_t	uncomp_size;
} PACKED arc_header_t;

#endif
