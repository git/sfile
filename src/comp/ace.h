/*
 * ACE
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef ACE_H
#define ACE_H

#include "types.h"

#define ACE_HDRSZ		sizeof(ace_header_t)

#define ACE_SIGN		"**ACE**"

#define ACE_HEADER_TYPE		0

#define ACE_FLAG_ADDSIZE	1
#define ACE_FLAG_COMMENT	2
#define ACE_FLAG_SFX		0x0200
#define ACE_FLAG_JR_SFX		0x0400	/* dictionary limited to 256K */
#define ACE_FLAG_VOLUMES	0x0800
#define ACE_FLAG_AV		0x1000
#define ACE_FLAG_RECOVERY	0x2000
#define ACE_FLAG_LOCKED		0x4000
#define ACE_FLAG_SOLID		0x8000

#define ACE_OS_MSDOS		0
#define ACE_OS_OS2		1
#define ACE_OS_WINDOWS32	2
#define ACE_OS_UNIX		3
#define ACE_OS_MACOS		4
#define ACE_OS_WINDOWSNT	5
#define ACE_OS_PRIMOS		6
#define ACE_OS_APPLE_GS		7
#define ACE_OS_ATARI		8
#define ACE_OS_VMS		9
#define ACE_OS_AMIGA		10
#define ACE_OS_NEXT		11

typedef struct {
	uint16_t	crc;
	uint16_t	size;
	uint8_t		type;
	uint16_t	flags;
	char		sign[7];
	uint8_t		extract_ver;
	uint8_t		created_ver;
	uint8_t		os;
	uint8_t		vol_num;
	uint32_t	tdstamp;	/* DOS format */
	uint8_t		reserved[8];
	uint8_t		av_size;	/* optional */
	uint8_t		av[0];		/* optional */
	uint16_t	comment_size;	/* optional */
	uint8_t		comment[0];	/* optional */
} PACKED ace_header_t;

typedef struct {
	uint16_t	crc;
	uint16_t	size;
	uint8_t		type;
	uint16_t	flags;
	uint32_t	addsize;	/* optional field */
} PACKED ace_block_t;

#define ACE_BLOCK_TYPE_FILE	1

typedef struct {
	uint16_t	crc;
	uint16_t	size;
	uint8_t		type;
	uint16_t	flags;
	uint32_t	addsize;
	uint32_t	orig_size;
	uint32_t	tdstamp;
	uint32_t	attr;
	uint32_t	crc32;
	uint32_t	comp_info;
	uint16_t	reserved;
	uint16_t	fname_size;
	uint8_t		fname;
	uint16_t	comment_size;	/* optional */
	uint8_t		comment[0];	/* optional */
} PACKED ace_file_block_t;

#define ACE_BLOCK_TYPE_RECOVERY	2

typedef struct {
	uint16_t	crc;
	uint16_t	size;
	uint8_t		type;
	uint16_t	flags;
	uint32_t	addsize;	/* optional field */
} PACKED ace_recovery_block_t;

#endif
