/*
 * HA compressed file
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2014 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef SFILE_HA_H
#define SFILE_HA_H

#include "types.h"

#define HA_HDRSZ	sizeof(ha_header_t)

#define HA_SIGN		"HA"

typedef struct {
	uint8_t		sign[2];
	uint16_t	file_count;
} PACKED ha_header_t;

typedef struct {
	uint8_t		info;
	uint32_t	compressed_size;
	uint32_t	uncompressed_size;
	uint32_t	crc;
	uint32_t	timestamp;
#if 0
	uint8_t		pathname[]; /* NUL-terminated. */
	uint8_t		filename[]; /* NUL-terminated. */
	uint8_t		machine_info_size;
	uint8_t		machine_info[];
#endif
} PACKED ha_file_header_t;

#endif
