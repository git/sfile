/*
 * HA compressed file
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2014 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include "subfile.h"
#include "endian.h"
#include "ha.h"

static uint32_t
ha_dumper(file_t *file)
{
	ha_header_t *hdr = (ha_header_t *)file->sf_buf;

	file_add_entry(file, "ha headear signature", "%.2s", hdr->sign);
	file_add_entry(file, "file count", "%hu", le16toh(hdr->file_count));

	return 0;
}

static struct subfile_sign ha_sign[] = {
	{ .si_id = HA_SIGN, .si_size = 2 },
	{ NULL }
};

struct subfile ha_subfile = {
	.sf_type = sf_type_comp,
	.sf_desc = "HA compressed archive",
	.sf_sign = ha_sign,
	.sf_size = HA_HDRSZ,
	.sf_dumper = ha_dumper,
};
