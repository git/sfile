/*
 * ARJ compressed file
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef ARJ_H
#define ARJ_H

#include "types.h"

#define ARJ_HDRSZ		sizeof(arj_header_t)
#define ARJ_MAX_HDRSZ		2600

#define ARJ_SIGN		"\x60\xEA"
#define ARJ_BSIGN		0xea60

#define ARJ_OS_MSDOS		0
#define ARJ_OS_PRIMOS		1
#define ARJ_OS_UNIX		2
#define ARJ_OS_AMIGA		3
#define ARJ_OS_MACOS		4
#define ARJ_OS_OS2		5
#define ARJ_OS_APPLE_GS		6
#define ARJ_OS_ATARI_ST		7
#define ARJ_OS_NEXT		8
#define ARJ_OS_VMS		9

#define ARJ_FLAG_OLD_CRYPT	0x02
#define ARJ_FLAG_VOLUME		0x04
#define ARJ_FLAG_PATHSYM	0x10
#define ARJ_FLAG_BACKUP		0x20
#define ARJ_FLAG_CRYPT		0x40

typedef struct {
	uint16_t	sign;
	uint16_t	size;
	uint8_t		first_hdr_size;
	uint8_t		prog_ver;
	uint8_t		min_prog_ver;
	uint8_t		os_type;
	uint8_t		flags;
	uint8_t		security_ver;
	uint8_t		file_type;
	uint8_t		reserved1;
	uint32_t	tdstamp;
	uint32_t	tdmodify;
	uint32_t	fsize;
	uint32_t	security_offs;
	uint16_t	fspec_fname_offs;
	uint16_t	security_size;
} PACKED arj_header_t;

typedef struct {
	uint16_t	sign;
	uint16_t	size;
	uint8_t		first_hdr_size;
	uint8_t		prog_ver;
	uint8_t		min_prog_ver;
	uint8_t		os_type;
	uint8_t		flags;
	uint8_t		method;
	uint8_t		ftype;
	uint8_t		reserved1;
	uint32_t	tdmodify;
	uint32_t	comp_size;
	uint32_t	uncomp_size;
	uint32_t	crc32;
	uint16_t	fspec_fname_offs;
	uint16_t	faccess;
	uint16_t	host_data;
} PACKED arj_local_header_t;

#endif
