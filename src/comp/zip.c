/*
 * ZIP compressed file
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include "subfile.h"
#include "zip.h"

static uint32_t
zip_dumper(file_t *file)
{
	zip_header_t *hdr = (zip_header_t *)file->sf_buf;
	string_t tmp;

	file_add_entry(file, "zip header signature", "%#0x", hdr->sign);
	switch (hdr->prog_ver >> 8) {
	case ZIP_OS_FAT		: tmp = "FAT file system"; break;
	case ZIP_OS_AMIGA	: tmp = "Amiga"; break;
	case ZIP_OS_VMS		: tmp = "VAX/VMS"; break;
	case ZIP_OS_UNIX	: tmp = "Unix"; break;
	case ZIP_OS_VM_CMS	: tmp = "VM/CMS"; break;
	case ZIP_OS_ATARI	: tmp = "Atari"; break;
	case ZIP_OS_HPFS	: tmp = "OS/2 HPFS"; break;
	case ZIP_OS_MAC		: tmp = "CP/M"; break;
	case ZIP_OS_Z_SYSTEM	: tmp = "Z-Systems"; break;
	case ZIP_OS_CPM		: tmp = "CP/M"; break;
	case ZIP_OS_TOPS20	: tmp = "TOPS20"; break;
	case ZIP_OS_NTFS	: tmp = "NTFS file system"; break;
	case ZIP_OS_QDOS	: tmp = "QDOS"; break;
	case ZIP_OS_ACORN	: tmp = "Acorn"; break;
	case ZIP_OS_VFAT	: tmp = "VFAT file syetem"; break;
	case ZIP_OS_MVS		: tmp = "MVS"; break;
	case ZIP_OS_BEOS	: tmp = "Beos"; break;
	case ZIP_OS_TANDEM	: tmp = "Tandem"; break;
	default                 : tmp = "unknown"; break;
	};
	file_add_entry(file, "OS used on compression ", "%s", tmp);
	file_add_entry(file, "program version used", "%u.%u",
	       (hdr->prog_ver & 0xFF) / 10,
	       (hdr->prog_ver & 0xFF) % 10);
	file_add_entry(file, "min version required", "%u.%u",
	       (hdr->prog_ver & 0xFF) / 10,
	       (hdr->prog_ver & 0xFF) % 10);
	file_add_entry(file, "encrypted", "%s", (hdr->flags & ZIP_FLAG_CRYPT) ? "yes" : "no");
	switch (hdr->method) {
	case ZIP_METHOD_STORED		: tmp = "Stored"; break;
	case ZIP_METHOD_SHRUNK		: tmp = "Shrunk"; break;
	case ZIP_METHOD_REDUCED1	: tmp = "Reduced Level 1"; break;
	case ZIP_METHOD_REDUCED2	: tmp = "Reduced Level 2"; break;
	case ZIP_METHOD_REDUCED3	: tmp = "Reduced Level 3"; break;
	case ZIP_METHOD_REDUCED4	: tmp = "Reduced Level 4"; break;
	case ZIP_METHOD_IMPLODED	: tmp = "Imploded"; break;
	case ZIP_METHOD_TOKENIZED	: tmp = "Tokenized"; break;
	case ZIP_METHOD_DEFLATED        : tmp = "Deflated"; break;
	default				: tmp = "unknown"; break;
	};
	file_add_entry(file, "compression method", "%s", tmp);
	if (hdr->method == ZIP_METHOD_IMPLODED) {
		file_add_entry(file, "sliding dictionary size", "%uKb", (hdr->flags & ZIP_FLAG_8KDIC) ? 8 : 4);
		file_add_entry(file, "Shannon-Fano trees type", "%u", (hdr->flags & ZIP_FLAG_SF3 ? 3 : 2));
	} else if (hdr->method == ZIP_METHOD_DEFLATED) {
		switch (hdr->flags & ZIP_FLAG_MASK) {
		case ZIP_FLAG_NORMAL    : tmp = "Normal"; break;
		case ZIP_FLAG_MAX       : tmp = "Maximum"; break;
		case ZIP_FLAG_FAST      : tmp = "Fast"; break;
		case ZIP_FLAG_SFAST     : tmp = "Fastest"; break;
		};
		file_add_entry(file, "compression option", "%s", tmp);
	};
	file_add_entry(file, "time stamp", "%u", hdr->time);
	file_add_entry(file, "date stamp", "%u", hdr->date);
	if (!(hdr->flags & ZIP_FLAG_EDATA)) {
		file_add_entry(file, "crc 32", "%#0x", hdr->crc32);
		file_add_entry(file, "compressed size", "%u", hdr->comp_size);
		file_add_entry(file, "uncompressed size", "%u", hdr->uncomp_size);
	}

	return 0;
}

static struct subfile_sign zip_sign[] = {
	{.si_id = ZIP_SIGN, .si_size = 4,},
	{NULL}
};

struct subfile zip_subfile = {
	.sf_type = sf_type_comp,
	.sf_desc = "ZIP compressed file",
	.sf_sign = zip_sign,
	.sf_size = ZIP_HDRSZ,
	.sf_dumper = zip_dumper,
};
