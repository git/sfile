/*
 * Microsoft compressed file
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef MSCOMP_H
#define MSCOMP_H

#include "types.h"

#define MSCOMP5_HDRSZ	sizeof(mscomp5_header_t)
#define MSCOMP6_HDRSZ	sizeof(mscomp6_header_t)

#define MSCOMP5_SIGN	"SZDD"
#define MSCOMP6_SIGN	"KWAJ"

#define MSCOMP5_RES1	0x3327f088

typedef struct {
	uint8_t		sign[4];
	uint32_t	reserved1;
	uint8_t		reserved2;
	uint8_t		last_char_fname;
	uint32_t	uncomp;
	uint8_t		reserved3;
} PACKED mscomp5_header_t;

#define MSCOMP6_RES1	0xD1127f088
#define MSCOMP6_RES2	0x001200003
#define MSCOMP6_RES3	0x0001

typedef struct {
	uint8_t		sign[4];
	uint32_t	reserved1;
	uint32_t	reserved2;
	uint16_t	reserved3;
} PACKED mscomp6_header_t;

#endif
