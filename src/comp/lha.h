/*
 * LHA compressed file
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2000, 2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef LHA_H
#define LHA_H

#include "types.h"

#define LHA_HDRSZ		21

#define LHA_METHOD_HUFF0	"-lh0-"
#define LHA_METHOD_HUFF1	"-lh1-"
#define LHA_METHOD_HUFF2	"-lh2-"
#define LHA_METHOD_HUFF3	"-lh3-"
#define LHA_METHOD_HUFF4	"-lh4-"
#define LHA_METHOD_HUFF5	"-lh5-"
#define LHA_METHOD_HUFF		"-lh -"
#define LHA_METHOD_STORED	"-lhs-"
#define LHA_METHOD_DIRS		"-lhd-"
#define LHA_METHOD_ARC4		"-lz4-"
#define LHA_METHOD_ARC5		"-lz5-"

#define LHA_OS_GENERIC		0
#define LHA_OS_UNIX		'U'
#define LHA_OS_MSDOS		'M'
#define LHA_OS_MACOS		'm'
#define LHA_OS_OS9		'9'
#define LHA_OS_OS2		'2'
#define LHA_OS_OS68K		'K'
#define LHA_OS_OS386		'3'		/* OS-9000??? */
#define LHA_OS_HUMAN		'H'
#define LHA_OS_CPM		'C'
#define LHA_OS_FLEX		'F'
#define LHA_OS_RUNSER		'R'
#define LHA_OS_TOWNSOS		'T'
#define LHA_OS_XOSK		'X'

#define LHA_LEVEL0		0x00
#define LHA_LEVEL1		0x01
#define LHA_LEVEL2		0x02

typedef struct {
	uint8_t		size;
	uint8_t		chksum;
	uint8_t		method[5];
	uint32_t	comp_size;
	uint32_t	uncomp_size;
	uint16_t	time;
	uint16_t	date;
	uint8_t		attrib;
	uint8_t		level;
/* end of pseudo-std header */
	uint8_t		fname_size;
	char		*fname;
	uint16_t	crc;
	uint8_t		os_type;
/*
	uint8_t		minor_version;
	time_t		unix_last_modified_stamp;
	uint16_t	unix_mode;
	uint16_t	unix_uid;
	uint16_t	unix_gid;
*/
} PACKED lha_header_t;

#endif
