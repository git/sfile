/*
 * LZOP compressed file
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2014 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include "subfile.h"
#include "string.h"
#include "endian.h"
#include "lzop.h"

static uint32_t
lzop_dumper(file_t *file)
{
	lzop_header_t *hdr = (lzop_header_t *)file->sf_buf;
	const char *desc;
	char sign[32];
	uint32_t flags;

	str_bin2hex(sign, (char *)hdr->sign, 9);

	file_add_entry(file, "lzop headear signature", "%s", sign);
	file_add_entry(file, "lzop header version", "%#.4hx", be16toh(hdr->version));
	file_add_entry(file, "lzop header lib version", "%#.4hx", be16toh(hdr->lib_version));
	file_add_entry(file, "lzop header min version", "%#.4hx", be16toh(hdr->min_version));

	switch (hdr->method) {
	case LZOP_METHOD_LZO1X_1:	desc = "LZO1X-1"; break;
	case LZOP_METHOD_LZO1X_1_15:	desc = "LZO1X-1.15"; break;
	case LZOP_METHOD_LZO1X_999:	desc = "LZO1X-999"; break;
	case LZOP_METHOD_NRV1A:		desc = "NRV1A"; break;
	case LZOP_METHOD_NRV1B:		desc = "NRV1B"; break;
	case LZOP_METHOD_NRV2A:		desc = "NRV2A"; break;
	case LZOP_METHOD_NRV2B:		desc = "NRV2B"; break;
	case LZOP_METHOD_NRV2D:		desc = "NRV2D"; break;
	case LZOP_METHOD_ZLIB:		desc = "zlib"; break;
	default:			desc = "unknown"; break;
	}
	file_add_entry(file, "lzop compression method", "%s (%u)", desc, hdr->method);

	file_add_entry(file, "lzop compression level", "%u", hdr->level);

	flags = be32toh(hdr->flags);
	file_add_entry(file, "lzop compression flags", "%#.8x", flags);

	printf(" lzop compression flags	: <|");
	if (flags & LZOP_FLAG_H_EXTRA_FIELD)
		printf("h-extra-field|");
	if (flags & LZOP_FLAG_H_PATH)
		printf("h-path|");
	if (flags & LZOP_FLAG_H_GMTDIFF)
		printf("h-gmtdiff|");
	if (flags & LZOP_FLAG_H_FILTER)
		printf("h-filter|");
	if (flags & LZOP_FLAG_H_CRC32)
		printf("h-crc32|");
	if (flags & LZOP_FLAG_CRC32_D)
		printf("crc32-d|");
	if (flags & LZOP_FLAG_CRC32_C)
		printf("crc32-c|");
	if (flags & LZOP_FLAG_ADLER32_D)
		printf("adler32-d|");
	if (flags & LZOP_FLAG_ADLER32_C)
		printf("adler32-c|");
	if (flags & LZOP_FLAG_STDIN)
		printf("stdin|");
	if (flags & LZOP_FLAG_STDOUT)
		printf("stdout|");
	if (flags & LZOP_FLAG_NAME_DEFAULT)
		printf("name-default|");
	if (flags & LZOP_FLAG_DOSISH)
		printf("dosish|");
	if (flags & LZOP_FLAG_MULTIPART)
		printf("multipart|");
	printf(">\n");

	switch (flags & LZOP_FLAG_OS_MASK) {
	case LZOP_FLAG_OS_FAT:		desc = "FAT"; break;
	case LZOP_FLAG_OS_AMIGA:	desc = "Amiga"; break;
	case LZOP_FLAG_OS_VMS:		desc = "VMS"; break;
	case LZOP_FLAG_OS_UNIX:		desc = "Unix"; break;
	case LZOP_FLAG_OS_VM_CMS:	desc = "VM/CMS"; break;
	case LZOP_FLAG_OS_ATARI:	desc = "Atari"; break;
	case LZOP_FLAG_OS_OS2:		desc = "OS/2"; break;
	case LZOP_FLAG_OS_MAC9:		desc = "Mac 9"; break;
	case LZOP_FLAG_OS_Z_SYSTEM:	desc = "Z/System"; break;
	case LZOP_FLAG_OS_CPM:		desc = "CPM"; break;
	case LZOP_FLAG_OS_TOPS20:	desc = "TOPS20"; break;
	case LZOP_FLAG_OS_NTFS:		desc = "NTFS"; break;
	case LZOP_FLAG_OS_QDOS:		desc = "QDOS"; break;
	case LZOP_FLAG_OS_ACORN:	desc = "ACORN"; break;
	case LZOP_FLAG_OS_VFAT:		desc = "VFAT"; break;
	case LZOP_FLAG_OS_MFS:		desc = "MFS"; break;
	case LZOP_FLAG_OS_BEOS:		desc = "BeOS"; break;
	case LZOP_FLAG_OS_TANDEM:	desc = "Tandem"; break;
	default:			desc = "unknown"; break;
	}
	file_add_entry(file, "lzop compression OS", "%s", desc);

	switch (flags & LZOP_FLAG_CS_MASK) {
	case LZOP_FLAG_CS_NATIVE:	desc = "Native"; break;
	case LZOP_FLAG_CS_LATIN1:	desc = "Latin1"; break;
	case LZOP_FLAG_CS_DOS:		desc = "DOS"; break;
	case LZOP_FLAG_CS_WIN32:	desc = "Win32"; break;
	case LZOP_FLAG_CS_WIN16:	desc = "Win16"; break;
	case LZOP_FLAG_CS_UTF8:		desc = "UTF-8"; break;
	default:			desc = "unknown"; break;
	}
	file_add_entry(file, "lzop compression charset", "%s", desc);

	return 0;
}

static struct subfile_sign lzop_sign[] = {
	{ .si_id = LZOP_SIGN, .si_size = 9 },
	{ NULL }
};

struct subfile lzop_subfile = {
	.sf_type = sf_type_comp,
	.sf_desc = "LZOP compressed file",
	.sf_sign = lzop_sign,
	.sf_size = LZOP_HDRSZ,
	.sf_dumper = lzop_dumper,
};
