/*
 * LRZIP compressed file
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2014 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef SFILE_LRZIP_H
#define SFILE_LRZIP_H

#include "types.h"

#define LRZIP_HDRSZ		sizeof(lrzip_header_t)

#define LRZIP_SIGN		"LRZI"

typedef struct {
	uint8_t		sign[4];
	uint8_t		version_major;
	uint8_t		version_minor;
	uint64_t	size;
	uint16_t	reserved1;
	uint8_t		lzma_flags[5];
	uint8_t		md5sum_flag;
	uint8_t		encrypt_flag;
	uint8_t		reserved2;
} PACKED lrzip_header_t;

#endif
