/*
 * RAR compressed file
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include "subfile.h"
#include "rar.h"

static uint32_t
rar_dumper(file_t *file)
{
	rar_header_t *hdr = (rar_header_t *)file->sf_buf;
	rar_arch_header_t *ahdr = (rar_arch_header_t *)(file->sf_buf + 7);
	string_t desc;

	if (ahdr->type != RAR_TYPE_ARCH)
		puts("invalid rar header...");
	file_add_entry(file, "rar header signature", "%.4s", (int8_t *)hdr);
	file_add_entry(file, "rar header size", "%#x", hdr->size);
	file_add_entry(file, "block crc ", "%#x", ahdr->crc);

	switch (ahdr->type) {
	case RAR_TYPE_MARK	: desc = "mark"; break;
	case RAR_TYPE_ARCH	: desc = "archive"; break;
	case RAR_TYPE_FILE	: desc = "file"; break;
	case RAR_TYPE_COMM	: desc = "comment"; break;
	case RAR_TYPE_INF	: desc = "extra info"; break;
	case RAR_TYPE_SUBBLK	: desc = "subblock"; break;
	case RAR_TYPE_RECVRY	: desc = "recovery record"; break;
	default                 : desc = "unknown"; break;
	}
	file_add_entry(file, "block type", "%s", desc);

	printf(" block flags		: <|");
	if (ahdr->flags & RAR_FLAG_ARCH_VOL)
		printf("volume|");
	if (ahdr->flags & RAR_FLAG_ARCH_COMM)
		printf("comment|");
	if (ahdr->flags & RAR_FLAG_ARCH_LOCK)
		printf("lock|");
	if (ahdr->flags & RAR_FLAG_ARCH_SOLID)
		printf("solid|");
	if (ahdr->flags & RAR_FLAG_ARCH_AV)
		printf("authenticity info|");
	if (ahdr->flags & RAR_FLAG_NEW_BLOCK)
		printf("new block|");
	if (ahdr->flags & RAR_FLAG_OPT)
		printf("add_size field|");
	printf(">\n");

	file_add_entry(file, "block size", "%u", ahdr->size);

	return 0;
}

static struct subfile_sign rar_sign[] = {
	{.si_id = RAR_SIGN, .si_size = 7,},
	{NULL}
};

struct subfile rar_subfile = {
	.sf_type = sf_type_comp,
	.sf_desc = "RAR compressed file",
	.sf_sign = rar_sign,
	.sf_size = RAR_HDRSZ,
	.sf_dumper = rar_dumper,
};
