/*
 * LRZIP compressed file
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2014 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include "subfile.h"
#include "endian.h"
#include "lrzip.h"

static uint32_t
lrzip_dumper(file_t *file)
{
	lrzip_header_t *hdr = (lrzip_header_t *)file->sf_buf;

	file_add_entry(file, "lrzip headear signature", "%.4s", hdr->sign);
	file_add_entry(file, "lrzip header version", "%u.%u",
	               hdr->version_major, hdr->version_minor);
	file_add_entry(file, "file size", "%ju", le64toh(hdr->size));
	file_add_entry(file, "file md5sum", "%d", hdr->md5sum_flag);
	file_add_entry(file, "file encrypted", "%d", hdr->encrypt_flag);

	return 0;
}

static struct subfile_sign lrzip_sign[] = {
	{ .si_id = LRZIP_SIGN, .si_size = 4 },
	{ NULL }
};

struct subfile lrzip_subfile = {
	.sf_type = sf_type_comp,
	.sf_desc = "LRZIP compressed file",
	.sf_sign = lrzip_sign,
	.sf_size = LRZIP_HDRSZ,
	.sf_dumper = lrzip_dumper,
};
