/*
 * ZOO compressed file
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2000, 2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef ZOO_H
#define ZOO_H

#include "types.h"

#define ZOO_HDRSZ	sizeof(zoo_header_t)

#define ZOO_SIGN	"\xdc\xa7\xc4\xfd"
#define ZOO_BSIGN	0xfdc4a7dcL

#define ZOO_TEXT_TERM	26

#define ZOO_OS_UNIX	0
#define ZOO_OS_MSDOS	1
#define ZOO_OS_PORTABLE	2

typedef struct {
	uint8_t		text[20];
	uint32_t	sign;
	uint32_t	data_offs;
	uint32_t	data_offs_neg;
	uint8_t		ver_hi;
	uint8_t		ver_lo;
	uint8_t		type;
	uint32_t	comment_offs;
	uint16_t	comment_size;
	uint8_t		data_ver;
} PACKED zoo_header_t;

typedef struct {
	uint32_t	sign;
	uint8_t		type;
	uint8_t		method;		/* 0 = no packing, 1 = normal LZW */
	uint32_t	next_offs;	/* pos'n of next directory entry */
	uint32_t	file_offs;	/* position of this file */
	uint16_t	date;		/* DOS format date */
	uint16_t	time;		/* DOS format time */
	uint16_t	file_crc;	/* CRC of this file */
	uint32_t	uncomp_size;
	uint32_t	comp_size;
	uint8_t		ver_hi;
	uint8_t		ver_lo;		/* minimum version needed to extract */
	uint8_t		deleted;	/* will be 1 if deleted, 0 if not */
	uint8_t		file_struc;	/* file structure if any */
	uint32_t	comment_offs;	/* points to comment;  zero if none */
	uint16_t	comment_size;	/* length of comment, 0 if none */
	uint8_t		file_name[13];	/* filename */
	uint16_t	var_dir_size;	/* length of variable part of dir entry */
	uint8_t		tz;		/* timezone where file was archived */
	uint16_t	dir_crc;	/* CRC of directory entry */
	uint8_t		lfn_size;	/* length of long filename */
	uint8_t		dirname_size;	/* length of directory name */
	uint8_t		lfname[256];	/* long filename */
	uint8_t		dirname[256];	/* directory name */
	uint16_t	os_type;	/* Filesystem ID */
	uint8_t		fattr[3];	/* File attributes */
	uint8_t		ver_flag;	/* version flag bits */
	uint16_t	fver;		/* file version number if any */
} PACKED zoo_dir_entry_t;

#endif
