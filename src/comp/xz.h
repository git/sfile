/*
 * XZ compressed file
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2014 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef XZ_H
#define XZ_H

#include "types.h"

#define XZ_HDRSZ		12

#define XZ_SIGN			"\xfd\x37\x7a\x58\x5a\x00"

#define XZ_FLAG_NONE		0x00
#define XZ_FLAG_CRC32		0x01
#define XZ_FLAG_CRC64		0x04
#define XZ_FLAG_SHA256		0x0a

typedef struct {
	uint8_t		sign[6];
	uint8_t		flags[2];
	uint32_t	crc32;
} PACKED xz_header_t;

#endif
