/*
 * RZIP compressed file
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2014 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include "subfile.h"
#include "endian.h"
#include "rzip.h"

static uint32_t
rzip_dumper(file_t *file)
{
	rzip_header_t *hdr = (rzip_header_t *)file->sf_buf;
	uint64_t size;

	size = ((uint64_t)be32toh(hdr->size_hi) << 32) | be32toh(hdr->size_lo);

	file_add_entry(file, "rzip headear signature", "%.4s", hdr->sign);
	file_add_entry(file, "rzip header version", "%u.%u",
	               hdr->version_major, hdr->version_minor);
	file_add_entry(file, "file size", "%ju", size);

	return 0;
}

static struct subfile_sign rzip_sign[] = {
	{ .si_id = RZIP_SIGN, .si_size = 4 },
	{ NULL }
};

struct subfile rzip_subfile = {
	.sf_type = sf_type_comp,
	.sf_desc = "RZIP compressed file",
	.sf_sign = rzip_sign,
	.sf_size = RZIP_HDRSZ,
	.sf_dumper = rzip_dumper,
};
