/*
 * LZIP compressed file
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2014 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include "subfile.h"
#include "lzip.h"

static uint32_t
lzip_dumper(file_t *file)
{
	lzip_header_t *hdr = (lzip_header_t *)file->sf_buf;
	unsigned int dict_size;

	dict_size = 1 << (hdr->dict_size & 0x1f);
	if (dict_size > LZIP_DICT_SIZE_MIN)
		dict_size -= (dict_size / 16) * (hdr->dict_size >> 5) & 0x07;

	file_add_entry(file, "lzip headear signature", "%.4s", hdr->sign);
	file_add_entry(file, "lzip header version", "%u", hdr->version);
	file_add_entry(file, "lzip dictionary size", "%u", dict_size);

	return 0;
}

static struct subfile_sign lzip_sign[] = {
	{ .si_id = LZIP_SIGN, .si_size = 4 },
	{ NULL }
};

struct subfile lzip_subfile = {
	.sf_type = sf_type_comp,
	.sf_desc = "LZIP compressed file",
	.sf_sign = lzip_sign,
	.sf_size = LZIP_HDRSZ,
	.sf_dumper = lzip_dumper,
};
