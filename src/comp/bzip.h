/*
 * BZIP compressed file
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef BZIP_H
#define BZIP_H

#include "types.h"

#define BZIP_HDRSZ		sizeof(bzip_header_t)

#define BZIP_SIGN		"BZ"
#define BZIP_BSIGN		0x5a42

#define BZIP_VER_0		'0'	/* Bzip 0.21 */
#define BZIP_VER_2		'h'	/* Bzip 2 Huffmanised */

#define BZIP_BLOCK		'0'	/* 100K block size */

typedef struct {
	uint16_t	sign;
	uint8_t		ver;
	uint8_t		block_size;
} PACKED bzip_header_t;

#endif
