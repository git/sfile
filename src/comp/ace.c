/*
 * ACE
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include "subfile.h"
#include "endian.h"
#include "string.h"
#include "file.h"
#include "ace.h"

static uint32_t
ace_dumper(file_t *file)
{
	ace_header_t *hdr = (ace_header_t *)file->sf_buf;
	string_t desc;
	char ts[32];

	file_add_entry(file, "ace header signature", "%.7s", hdr->sign);
	file_add_entry(file, "ace header crc", "%hu", hdr->crc);
	file_add_entry(file, "ace header type", "%hu", hdr->type & 0xff);

	printf(" archive flags			: <|");
	if (hdr->flags & ACE_FLAG_ADDSIZE)
		printf("addsize|");
	if (hdr->flags & ACE_FLAG_COMMENT)
		printf("comment|");
	if (hdr->flags & ACE_FLAG_SFX)
		printf("self-extract|");
	if (hdr->flags & ACE_FLAG_JR_SFX)
		printf("jr. self-extract|");
	if (hdr->flags & ACE_FLAG_VOLUMES)
		printf("volume|");
	if (hdr->flags & ACE_FLAG_AV)
		printf("authenticity info|");
	if (hdr->flags & ACE_FLAG_RECOVERY)
		printf("recovery record info|");
	if (hdr->flags & ACE_FLAG_LOCKED)
		printf("lock|");
	if (hdr->flags & ACE_FLAG_SOLID)
		printf("solid|");
	printf(">\n");

	file_add_entry(file, "extract version", "%hu", hdr->extract_ver & 0xff);
	file_add_entry(file, "created version", "%hu", hdr->created_ver & 0xff);

	switch (hdr->os) {
	case ACE_OS_MSDOS	: desc = "MSDOS"; break;
	case ACE_OS_OS2		: desc = "OS/2"; break;
	case ACE_OS_WINDOWS32	: desc = "Windows 32"; break;
	case ACE_OS_UNIX	: desc = "Unix"; break;
	case ACE_OS_MACOS	: desc = "MacOS"; break;
	case ACE_OS_WINDOWSNT	: desc = "Windows NT"; break;
	case ACE_OS_PRIMOS	: desc = "Primos"; break;
	case ACE_OS_APPLE_GS	: desc = "Apple GS"; break;
	case ACE_OS_ATARI	: desc = "Atari"; break;
	case ACE_OS_VMS		: desc = "VAX/VMS"; break;
	case ACE_OS_AMIGA	: desc = "Amiga"; break;
	case ACE_OS_NEXT	: desc = "NexT"; break;
	default                 : desc = "unknown"; break;
	};
	file_add_entry(file, "OS used on compression", "%s", desc);

	if (hdr->flags & ACE_FLAG_VOLUMES)
		file_add_entry(file, "volume number", "%hu", hdr->vol_num & 0xff);

	file_add_entry(file, "time & date stamp", "%s", str_msdos_tdstamp(ts, le32toh(hdr->tdstamp)));

	return 0;
}

static struct subfile_sign ace_sign[] = {
	{.si_id = ACE_SIGN, .si_size = 7, .si_offs = 7,},
	{NULL}
};

struct subfile ace_subfile = {
	.sf_type = sf_type_comp,
	.sf_desc = "ACE compressed file",
	.sf_ext = "ace",
	.sf_sign = ace_sign,
	.sf_size = ACE_HDRSZ,
	.sf_dumper = ace_dumper,
};
