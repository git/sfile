/*
 * GZIP compressed file
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include <time.h>
#include "subfile.h"
#include "zip.h"
#include "gzip.h"

static uint32_t
gzip_dumper(file_t *file)
{
	gzip_header_t *hdr = (gzip_header_t *)file->sf_buf;
	string_t tmp;
	time_t t = (time_t)hdr->tdstamp;
	char ts[64];

	file_add_entry(file, "gzip headear signature", "%#0x", hdr->sign);
	switch (hdr->os_ver) {
	case ZIP_OS_FAT		: tmp = "FAT file system"; break;
	case ZIP_OS_AMIGA	: tmp = "Amiga"; break;
	case ZIP_OS_VMS		: tmp = "VAX/VMS"; break;
	case ZIP_OS_UNIX	: tmp = "Unix"; break;
	case ZIP_OS_VM_CMS	: tmp = "VM/CMS"; break;
	case ZIP_OS_ATARI	: tmp = "Atari"; break;
	case ZIP_OS_HPFS	: tmp = "OS/2 HPFS"; break;
	case ZIP_OS_MAC		: tmp = "CP/M"; break;
	case ZIP_OS_Z_SYSTEM	: tmp = "Z-Systems"; break;
	case ZIP_OS_CPM		: tmp = "CP/M"; break;
	case ZIP_OS_TOPS20	: tmp = "TOPS20"; break;
	case ZIP_OS_NTFS	: tmp = "NTFS file system"; break;
	case ZIP_OS_QDOS	: tmp = "QDOS"; break;
	case ZIP_OS_ACORN	: tmp = "Acorn"; break;
	case ZIP_OS_VFAT	: tmp = "VFAT file syetem"; break;
	case ZIP_OS_MVS		: tmp = "MVS"; break;
	case ZIP_OS_BEOS	: tmp = "Beos"; break;
	case ZIP_OS_TANDEM	: tmp = "Tandem"; break;
	default			: tmp = "unknown"; break;
	};
	file_add_entry(file, "OS used on compression", "%s", tmp);
	file_add_entry(file, "encrypted", "%s",
	               (hdr->flags & GZIP_FLAG_CRYPT) ? "yes" : "no");
	file_add_entry(file, "probably an ascii file", "%s",
	               (hdr->flags & GZIP_FLAG_ASCII) ? "yes" : "no");
	if (hdr->flags & GZIP_FLAG_MULTI)
		file_add_entry(file, "multi file id", "%u", hdr->part_id);
	switch (hdr->method) {
	case ZIP_METHOD_STORED		: tmp = "Stored"; break;
	case ZIP_METHOD_SHRUNK		: tmp = "Shrunked"; break;
	case ZIP_METHOD_REDUCED1	: tmp = "Reduced Level 1"; break;
	case ZIP_METHOD_REDUCED2	: tmp = "Reduced Level 2"; break;
	case ZIP_METHOD_REDUCED3	: tmp = "Reduced Level 3"; break;
	case ZIP_METHOD_REDUCED4	: tmp = "Reduced Level 4"; break;
	case ZIP_METHOD_IMPLODED	: tmp = "Imploded"; break;
	case ZIP_METHOD_TOKENIZED	: tmp = "Tokenized"; break;
	case ZIP_METHOD_DEFLATED	: tmp = "Deflated"; break;
	default				: tmp = "Unknown"; break;
	};
	file_add_entry(file, "compression method", "%s", tmp);

	if (strftime(ts, sizeof(ts), "%c", gmtime(&t)) > 0)
		file_add_entry(file, "time & date stamp", "%s", ts);

	return 0;
}

static struct subfile_sign gzip_sign[] = {
	{.si_id = GZIP_SIGN, .si_size = 2,},
	{.si_id = GZIPOLD_SIGN, .si_size = 2,},
	{.si_id = PACK_SIGN, .si_size = 2,},
	{.si_id = LZH_SIGN, .si_size = 2,},
	{.si_id = LZW_SIGN, .si_size = 2,},
	{NULL}
};

struct subfile gzip_subfile = {
	.sf_type = sf_type_comp,
	.sf_desc = "GZIP compressed file",
	.sf_sign = gzip_sign,
	.sf_size = GZIP_HDRSZ,
	.sf_dumper = gzip_dumper,
};
