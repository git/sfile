/*
 * XZ compressed file
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2014 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include <time.h>
#include "subfile.h"
#include "xz.h"

static uint32_t
xz_dumper(file_t *file)
{
	xz_header_t *hdr = (xz_header_t *)file->sf_buf;
	string_t tmp;

	file_add_entry(file, "xz headear signature", "%s", hdr->sign + 1);
	switch (hdr->flags[1]) {
	case XZ_FLAG_NONE	: tmp = "none"; break;
	case XZ_FLAG_CRC32	: tmp = "CRC32"; break;
	case XZ_FLAG_CRC64	: tmp = "CRC64"; break;
	case XZ_FLAG_SHA256	: tmp = "SHA-256"; break;
	default			: tmp = "unknown"; break;
	};
	file_add_entry(file, "xz stream flags", "%s", tmp);

	return 0;
}

static struct subfile_sign xz_sign[] = {
	{ .si_id = XZ_SIGN, .si_size = 6 },
	{ NULL }
};

struct subfile xz_subfile = {
	.sf_type = sf_type_comp,
	.sf_desc = "XZ compressed file",
	.sf_sign = xz_sign,
	.sf_size = XZ_HDRSZ,
	.sf_dumper = xz_dumper,
};
