/*
 * ZIP compressed file
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef ZIP_H
#define ZIP_H

#include "types.h"

#define ZIP_HDRSZ	ZIP_LOCAL_HDRSZ

#define ZIP_SIGN	ZIP_LOCAL_SIGN

#define zip_header_t	zip_local_header_t

#define ZIP_CRC		0xdebb20e3L
#define ZIP_KEY1	305419896L
#define ZIP_KEY2	591751049L
#define ZIP_KEY3	878082192L

#define ZIP_VER_MAJOR(ver)	((ver & 0xff) / 10)
#define ZIP_VER_MINOR(ver)	((ver & 0xff) % 10)
#define ZIP_OS(ver)		((ver & 0xff00) >> 16)

#define ZIP_FLAG_CRYPT		1
#define ZIP_FLAG_8KDIC		2	/* for method 6 		*/
#define ZIP_FLAG_SF3		4	/* "				*/
#define ZIP_FLAG_NORMAL		0	/* for method 8 		*/
#define ZIP_FLAG_MAX		2	/* "				*/
#define ZIP_FLAG_FAST		4	/* "				*/
#define ZIP_FLAG_SFAST		6	/* "				*/
#define ZIP_FLAG_MASK		6	/* " mask for compression option*/
#define ZIP_FLAG_EDATA		8	/* " marks if crc32, compsize	*/
					/* " & uncompsize are present	*/

#define ZIP_METHOD_STORED	0
#define ZIP_METHOD_SHRUNK	1
#define ZIP_METHOD_REDUCED1	2
#define ZIP_METHOD_REDUCED2	3
#define ZIP_METHOD_REDUCED3	4
#define ZIP_METHOD_REDUCED4	5
#define ZIP_METHOD_IMPLODED	6
#define ZIP_METHOD_TOKENIZED	7
#define ZIP_METHOD_DEFLATED	8
#define ZIP_METHOD_ENHDEFLATED	9
#define ZIP_METHOD_DCLIMLPODED	10

#define ZIP_OS_FAT		0
#define ZIP_OS_AMIGA		1
#define ZIP_OS_VMS		2
#define ZIP_OS_UNIX		3
#define ZIP_OS_VM_CMS		4
#define ZIP_OS_ATARI		5
#define ZIP_OS_HPFS		6
#define ZIP_OS_MAC		7
#define ZIP_OS_Z_SYSTEM		8
#define ZIP_OS_CPM		9
#define ZIP_OS_TOPS20		10
#define ZIP_OS_NTFS		11
#define ZIP_OS_QDOS		12
#define ZIP_OS_ACORN		13
#define ZIP_OS_VFAT		14
#define ZIP_OS_MVS		15
#define ZIP_OS_BEOS		16
#define ZIP_OS_TANDEM		17

#define ZIP_EFIELD_AV		0x0007	/* PKWARE's authenticity verification */
#define ZIP_EFIELD_OS2		0x0009	/* OS/2 extended attributes */
#define ZIP_EFIELD_VMS		0x000c	/* PKWARE's VMS */
#define ZIP_EFIELD_UNIX		0x000d	/* PKWARE's Unix */
#define ZIP_EFIELD_IZ_VMS	0x4d49	/* Info-ZIP's VMS ("IM") */
#define ZIP_EFIELD_IZ_OLD_UNIX	0x5855	/* Info-ZIP's old Unix[1] ("UX") */
#define ZIP_EFIELD_IZ_NEW_UNIX	0x7855	/* Info-ZIP's new Unix[2] ("Ux") */
#define ZIP_EFIELD_TIME		0x5455	/* universal timestamp ("UT") */
#define ZIP_EFIELD_JLMAC	0x07c8	/* Johnny Lee's old Macintosh (= 1992) */
#define ZIP_EFIELD_ZIPIT	0x2605	/* Thomas Brown's Macintosh (ZipIt) */
#define ZIP_EFIELD_VM_CMS	0x4704	/* Info-ZIP's VM/CMS ("\004G") */
#define ZIP_EFIELD_MVS		0x470f	/* Info-ZIP's MVS ("\017G") */
#define ZIP_EFIELD_ACL		0x4c41	/* (OS/2) access control list ("AL") */
#define ZIP_EFIELD_NTSD		0x4453	/* NT security descriptor ("SD") */
#define ZIP_EFIELD_BEOS		0x6542	/* BeOS ("Be") */
#define ZIP_EFIELD_QDOS		0xfb4a	/* SMS/QDOS ("J\373") */
#define ZIP_EFIELD_AOS_VS	0x5356	/* AOS/VS ("VS") */
#define ZIP_EFIELD_SPARK	0x4341	/* David Pilling's Acorn/SparkFS ("AC") */
#define ZIP_EFIELD_MD5		0x4b46	/* Fred Kantor's MD5 ("FK") */
#define ZIP_EFIELD_ASI_UNIX	0x756e	/* ASi's Unix ("nu") */

#define ZIP_LOCAL_SIGN	"PK\x03\x04"	/* 0x04034b50 */
#define ZIP_LOCAL_HDRSZ	30		/* fixed by PKWARE */
#define ZIP_CRYPT_HDRSZ	12

typedef struct {
	uint32_t	sign;
	uint16_t	prog_ver;
	uint16_t	flags;
	uint16_t	method;
	uint16_t	time;
	uint16_t	date;
	uint32_t	crc32;
	uint32_t	comp_size;
	uint32_t	uncomp_size;
	uint16_t	fname_size;
	uint16_t	efield_size;
	char		*fname;
	uint8_t		*efield;
	uint8_t		*crypt_hdr;
} PACKED zip_local_header_t;

/*
#define ZIP_EXT_LOCAL_SIGN	0x08075450
#define ZIP_EXT_LOCAL_HDRSZ	???

typedef struct {

} PACKED ZIP_EXT_LOCAL_HEADER;
*/

#define ZIP_FILE_SIGN	0x02014b50
#define ZIP_FILE_HDRSZ	46

typedef struct {
	uint32_t	sign;
	uint16_t	os_ver;
	uint16_t	prog_ver;
	uint16_t	flags;
	uint16_t	method;
	uint16_t	time;
	uint16_t	date;
	uint32_t	crc32;
	uint32_t	comp_size;
	uint32_t	uncomp_size;
	uint16_t	fname_size;
	uint16_t	efield_size;
	uint16_t	fcomment_size;
	uint16_t	disk_start;
	uint16_t	int_fattr;
	uint32_t	ext_fattr;
	uint32_t	local_hdr_offs;
	char		*fname;
	uint8_t		*efield;
	uint8_t		*fcomment;
} PACKED zip_file_header_t;

#define ZIP_END_CENTRAL_DIR_SIGN	0x06054b50
#define ZIP_END_CENTRAL_DIR_HDRSZ	22
#define ZIP_ECD_OFFS			66000

typedef struct {
	uint32_t	sign;
	uint16_t	this_disk;
	uint16_t	start_disk;
	uint16_t	this_disk_ent;
	uint16_t	start_disk_ent;
	uint32_t	central_dir_size;
	uint32_t	central_dir_offs;
	uint16_t	zipfile_comment_size;
	uint8_t		*zipfile_comment;
} PACKED zip_end_central_dir_header_t;

#endif
