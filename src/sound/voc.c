/*
 * Creative Voice file (VOC)
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include "subfile.h"
#include "voc.h"

static uint32_t
voc_dumper(file_t *file)
{
	voc_header_t *hdr = (voc_header_t *)file->sf_buf;

	file_add_entry(file, "voc header signature", "%.19s", hdr->sign);
	file_add_entry(file, "data offset", "%u", hdr->data_offs);
	file_add_entry(file, "version", "%u.%u %x", hdr->ver >> 8, hdr->ver & 0xFF, hdr->ver);
	file_add_entry(file, "verify version", "%x %x", hdr->verify_ver, hdr->verify_ver + 0x1234);

	return 0;
}

static struct subfile_sign voc_sign[] = {
	{.si_id = VOC_SIGN, .si_size = 19,},
	{NULL}
};

struct subfile voc_subfile = {
	.sf_type = sf_type_sound,
	.sf_desc = "Creative Voice File",
	.sf_sign = voc_sign,
	.sf_size = VOC_HDRSZ,
	.sf_dumper = voc_dumper,
};
