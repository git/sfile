/*
 * Ogg Vorbis music file
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef OGG_H
#define OGG_H

#include "types.h"

#define OGG_HDRSZ	sizeof(ogg_header_t)

#define OGG_SIGN	"OggS"

typedef struct {
	uint32_t	dummy;
} PACKED ogg_header_t;

#endif
