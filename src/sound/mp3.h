/*
 * MP3 music file
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2000, 2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef MP3_H
#define MP3_H

#include "types.h"

#define MP3_HDRSZ	sizeof(mp3_header_t)

#define MP3_SIGN	"\xFF"

#define MP3_VER_RES		1
#define MP3_VER_1		3
#define MP3_VER_2		2
#define MP3_VER_2_5		0

#define MP3_LAYER_RES		0
#define MP3_LAYER_I		3
#define MP3_LAYER_II		2
#define MP3_LAYER_III		1

#define MP3_PROT_SET		0
#define MP3_PROT_NOSET		1

#define MP3_CHANNEL_STEREO	0
#define MP3_CHANNEL_JOINT	1
#define MP3_CHANNEL_DUAL	2
#define MP3_CHANNEL_SINGLE	3

#define MP3_MODE_EXT_OFF_OFF	0
#define MP3_MODE_EXT_ON_OFF	1
#define MP3_MODE_EXT_OFF_ON	2
#define MP3_MODE_EXT_ON_ON	3

#define MP3_EMPHASIS_NONE	0
#define MP3_EMPHASIS_50_15_ms	1
#define MP3_EMPHASIS_RES	2
#define MP3_EMPHASIS_CCIT_J_17	3

typedef struct {
	uint32_t	sync:		11;
	uint32_t	ver:		2;
	uint32_t	layer:		2;
	uint32_t	protection:	1;
	uint32_t	bitrate:	4;
	uint32_t	frequency:	2;
	uint32_t	padding:	1;
	uint32_t	private_bit:	1;
	uint32_t	channel:	2;
	uint32_t	mode_ext:	2;
	uint32_t	copyright:	1;
	uint32_t	original:	1;
	uint32_t	emphasis:	2;
} PACKED mp3_header_t;

#endif
