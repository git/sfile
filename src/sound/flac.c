/*
 * FLAC audio file
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2014 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include "subfile.h"
#include "file.h"
#include "flac.h"

static struct subfile_sign flac_sign[] = {
	{.si_id = FLAC_SIGN, .si_size = 4,},
	{NULL}
};

struct subfile flac_subfile = {
	.sf_type = sf_type_sound,
	.sf_desc = "FLAC Audio file",
	.sf_ext = "flac",
	.sf_sign = flac_sign,
	.sf_size = FLAC_HDRSZ,
};
