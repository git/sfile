/*
 * Ogg Vorbis music file
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include "subfile.h"
#include "file.h"
#include "ogg.h"

static uint32_t
ogg_dumper(file_t *file)
{
	/* HEADER(ogg, file, hdr); */

	return 0;
}

static struct subfile_sign ogg_sign[] = {
	{.si_id = OGG_SIGN, .si_size = 4,},
	{NULL}
};

struct subfile ogg_subfile = {
	.sf_type = sf_type_sound,
	.sf_desc = "Ogg Vorbis Audio file",
	.sf_ext = "ogg",
	.sf_sign = ogg_sign,
	.sf_size = OGG_HDRSZ,
	.sf_dumper = ogg_dumper,
};
