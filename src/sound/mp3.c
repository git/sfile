/*
 * MP3 music file
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2000, 2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include "subfile.h"
#include "file.h"
#include "mp3.h"

/*
 * Rows:
 *	MPEG 1 Layer I, Layer II, Layer III,
 *	MPEG 2 & 2.5 Layer I, Layer II, Layer III
 * 0 => variable bitrate
 */
static unsigned bitrate_table[6][15] = {
	{0, 32, 64, 96, 128, 160, 192, 224, 256, 288, 320, 352, 384, 416, 448},
	{0, 32, 48, 56, 64,  80,  96,  112, 128, 160, 192, 224, 256, 320, 384},
	{0, 32, 40, 48, 56,  64,  80,  96,  112, 128, 160, 192, 224, 256, 320},
	{0, 32, 64, 96, 128, 160, 192, 224, 256, 288, 320, 352, 384, 416, 448},
	{0, 32, 48, 56, 64,  80,  96,  112, 128, 160, 192, 224, 256, 320, 380},
	{0, 8,  16, 24, 32,  64,  80,  56,  64,  128, 160, 112, 128, 256, 320},
};

/*
 * Rows:
 *	MPEG 1
 *	MPEG 2
 *	MPEG 2.5
 */
static unsigned samplerate_table[3][3] = {
	{44100, 48000, 32000},
	{22050, 24000, 16000},
	{11025, 12000, 8000},
};

static uint32_t
mp3_dumper(file_t *file)
{
	mp3_header_t *hdr = (mp3_header_t *)file->sf_buf;
	string_t desc;
	int ver_layer = 0, ver = 0;

	file_add_entry(file, "mp3 header signature", "%#x", hdr->sync);
	switch (hdr->ver) {
	case MP3_VER_1:
		desc = "1.0";
		break;
	case MP3_VER_2:
		desc = "2.0";
		ver = 1;
		ver_layer = 1;
		break;
	case MP3_VER_2_5:
		desc = "2.5";
		ver = 2;
		ver_layer = 1;
		break;
	case MP3_VER_RES:
		desc = "reserved";
		break;
	default:
		desc = "unknown";
		break;
	}
	file_add_entry(file, "version", "%s", desc);

	switch (hdr->layer) {
	case MP3_LAYER_I:
		desc = "I";
		break;
	case MP3_LAYER_II:
		desc = "II";
		ver_layer += 1;
		break;
	case MP3_LAYER_III:
		desc = "III";
		ver_layer += 2;
		break;
	case MP3_LAYER_RES:
		desc = "reserved";
		break;
	default:
		desc = "unknown";
		break;
	}
	file_add_entry(file, "layer", "%s", desc);

	file_add_entry(file, "protection", "%s", hdr->protection ? "no" : "yes");
	file_add_entry(file, "bit rate", "%u Kbs",
	       bitrate_table[ver_layer][hdr->bitrate]);
	file_add_entry(file, "frequency", "%u Hz",
	       samplerate_table[ver][hdr->frequency]);
	file_add_entry(file, "padding", "%s", hdr->padding ? "yes" : "no");

	switch (hdr->channel) {
	case MP3_CHANNEL_STEREO	: desc = "stereo"; break;
	case MP3_CHANNEL_JOINT	: desc = "joint stereo"; break;
	case MP3_CHANNEL_DUAL	: desc = "dual"; break;
	case MP3_CHANNEL_SINGLE	: desc = "mono"; break;
	default			: desc = "unknown"; break;
	}
	file_add_entry(file, "channel", "%s", desc);

	/* lazy too lazy, better to look at each bit individually */
	switch (hdr->mode_ext) {
	case MP3_MODE_EXT_OFF_OFF	: desc = "off off"; break;
	case MP3_MODE_EXT_ON_OFF	: desc = "on off"; break;
	case MP3_MODE_EXT_OFF_ON	: desc = "off on"; break;
	case MP3_MODE_EXT_ON_ON		: desc = "on on"; break;
	default				: desc = "unknown"; break;
	}
	file_add_entry(file, "mode extension", "%s", desc);

	file_add_entry(file, "copyright", "%s", hdr->copyright ? "yes" : "no");
	file_add_entry(file, "original", "%s", hdr->original ? "yes" : "no");

	switch (hdr->emphasis) {
	case MP3_EMPHASIS_NONE		: desc = "none"; break;
	case MP3_EMPHASIS_50_15_ms	: desc = "50/15 ms"; break;
	case MP3_EMPHASIS_CCIT_J_17	: desc = "CCIT j.17"; break;
	case MP3_EMPHASIS_RES		: desc = "reserved"; break;
	default				: desc = "unknown"; break;
	}
	file_add_entry(file, "emphasis", "%s", desc);

	return 0;
}

/*
 * FIXME: Needed a special detection routine due to the special 11 bit
 * signature, and because a 8 bit sign is too short, and can mislead to
 * confusion. Maybe use the helper function?
 */
static struct subfile_sign mp3_sign[] = {
	{.si_id = MP3_SIGN, .si_size = 1,},
	{NULL}
};

struct subfile mp3_subfile = {
	.sf_type = sf_type_sound,
	.sf_desc = "MP3 Audio file",
	.sf_ext = "mp3",
	.sf_sign = mp3_sign,
	.sf_size = MP3_HDRSZ,
	.sf_dumper = mp3_dumper,
};
