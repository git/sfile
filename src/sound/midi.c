/*
 * MIDI music file
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdio.h>
#include "subfile.h"
#include "endian.h"
#include "midi.h"

static uint32_t
midi_dumper(file_t *file)
{
	midi_header_t *hdr = (midi_header_t *)file->sf_buf;
	string_t tmp;

	file_add_entry(file, "midi header signature", "%.4s", hdr->sign);
	file_add_entry(file, "midi header size", "%u", be32toh(hdr->size));
	switch (be16toh(hdr->format)) {
	case MIDI_FMT_SINGLE_TRK	: tmp = "single track"; break;
	case MIDI_FMT_MULTI_TRK_SYNCH	: tmp = "multi track synchronous"; break;
	case MIDI_FMT_MULTI_TRK_ASYNCH	: tmp = "multi track asynchronous"; break;
	default				: tmp = "unknown";
	}
	file_add_entry(file, "format", "%s", tmp);
	file_add_entry(file, "number of tracks", "%u", be16toh(hdr->tracks));
	file_add_entry(file, "delta-time", "%u", be16toh(hdr->delta_time));

	return 0;
}

static struct subfile_sign midi_sign[] = {
	{.si_id = MIDI_SIGN, .si_size = 4,},
	{NULL}
};

struct subfile midi_subfile = {
	.sf_type = sf_type_sound,
	.sf_desc = "MIDI music file",
	.sf_sign = midi_sign,
	.sf_size = MIDI_HDRSZ,
	.sf_dumper = midi_dumper,
};
