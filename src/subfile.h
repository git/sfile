/*
 * Subfile structures
 *
 * Part of sfile
 *
 * Copyright © 1998-2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef	SUBFILE_H
#define	SUBFILE_H

#include "file.h"
#include "types.h"

/*
 * SubFile Declarations
 */

enum subfile_type {
	sf_type_unknown,
	sf_type_unclassified,
	sf_type_arch,
	sf_type_comp,
	sf_type_exec,
	sf_type_exec_lib,
	sf_type_3d,
	sf_type_gfx,
	sf_type_video,
	sf_type_sound,
	sf_type_text,
	sf_type_database,
	sf_type_spreadsheet
};

extern string_t subfile_type_desc[];

struct subfile_sign {
	string_t	si_id;		/* Signature String */
	size_t		si_size;	/* Signature String size */
	uintmax_t	si_offs;	/* Signature offset within file */
	string_t	si_desc;	/* Alternate SubFile description */
	uint32_t	(*si_helper)(file_t *file, uint8_t *buf);
};

typedef struct subfile_sign subfile_sign_t;

struct subfile {
	enum subfile_type	sf_type;	/* SubFile type		*/
	string_t		sf_desc;	/* SubFile description	*/
	string_t		sf_ext;		/* SubFile extensions	*/
	subfile_sign_t		*sf_sign;	/* SubFile signature	*/
	size_t			sf_size;	/* SubFile header size	*/
	uint32_t (*sf_dumper)(file_t *);	/* SubFile Dumper	*/
#if 0
	int (*sf_loader)(file_t *);		/* SubFile Loader	*/
	int (*sf_detect)(file_t *);		/* SubFile Detect	*/
#endif
};

typedef struct subfile subfile_t;

#define HEADER(t, f, h) t##_header_t *h = (t##_header_t *)(f->sf_buf);

extern int print_field(subfile_t *subfile, string_t desc, string_t content, ...);

#endif
