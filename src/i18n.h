#ifndef I18N_H
#define I18N_H

#include <libintl.h>
#include <locale.h>
#define _(Str) gettext(Str)

#endif
