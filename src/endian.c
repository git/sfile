/*
 * Endianness conversion functions
 *
 * Lib Version 1.0.3
 *
 * Copyright © 2000, 2001, 2002 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include "endian.h"

/* Swap bytes on 16 bit data */
uint16_t
swapb16(uint16_t v)
{
	return ((v & 0x00FF) << 8) |
	       ((v & 0xFF00) >> 8);
}

/* Swap bytes on 32 bit data */
uint32_t
swapb32(uint32_t v)
{
	return ((v & 0x000000FF) << 24) |
	       ((v & 0x0000FF00) << 8) |
	       ((v & 0x00FF0000) >> 8) |
	       ((v & 0xFF000000) >> 24);
}

/* Swap bytes on 64 bit data */
uint64_t
swapb64(uint64_t v)
{
	return ((v & 0x00000000000000FFULL) << 56) |
	       ((v & 0x000000000000FF00ULL) << 40) |
	       ((v & 0x0000000000FF0000ULL) << 24) |
	       ((v & 0x00000000FF000000ULL) << 8) |
	       ((v & 0x000000FF00000000ULL) >> 8) |
	       ((v & 0x0000FF0000000000ULL) >> 24) |
	       ((v & 0x00FF000000000000ULL) >> 40) |
	       ((v & 0xFF00000000000000ULL) >> 56);
}

/* Swap shorts(16 bit) on 32 bit data */
uint32_t
swaps32(uint32_t l)
{
	return ((((l) & 0x0000FFFF) << 16) |
		(((l) & 0xFFFF0000) >> 16));
}

/* Swap bytes in shorts(16 bit) on 32 bit data */
uint32_t
swapbs32(uint32_t l)
{
	return (((l & 0x00FF00FF) << 8) |
	        ((l & 0xFF00FF00) >> 8));
}

/* Swap longs(32 bit) on 64 bit data */
uint64_t
swapl64(uint64_t ll)
{
	return ((((ll) & 0x00000000FFFFFFFFLL) << 32) |
	        (((ll) & 0xFFFFFFFF00000000LL) >> 32));
}
