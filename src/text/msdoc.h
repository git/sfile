/*
 * Microsoft Document
 *
 * Part of sfile (subfile structure)
 *
 * Copyright © 2000, 2001, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef MSDOC_H
#define MSDOC_H

#include "types.h"

/*
 * Microsoft Documents and others
 */

#define MSDOC_HDRSZ	sizeof(msdoc_header_t)

#define MSDOC_SIGN	"\xd0\xcf"

#define MSDOC_FLAG_DOT
#define MSDOC_FLAG_GLSY
#define MSDOC_FLAG_COMPLEX
#define MSDOC_FLAG_HASPIC

typedef struct {
	uint16_t	sign;
	uint16_t	version;
	uint16_t	product;
	uint16_t	language;
	uint16_t	next_page_offs;
	uint8_t		flags;
} PACKED msdoc_header_t;

#endif
