/*
 * Stack structures
 *
 * Part of sfile
 *
 * Copyright © 1998-2002, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#include <stdlib.h>
#include <errno.h>

#include "diags.h"
#include "i18n.h"
#include "file.h"

int
stack_alloc(stack_t *stack, size_t size)
{
	stack->st_count = 0;
	stack->st_size = size;
	stack->st = malloc(size * sizeof(size));

	if (stack->st == NULL)
		die(ENOMEM, _("not enough memory"));

	return 0;
}

int
stack_free(stack_t *stack)
{
	stack->st_count = stack->st_size = 0;
	free(stack->st);

	return 0;
}

int
stack_push(stack_t *stack, uintmax_t val)
{
	if (stack->st_count == stack->st_size) {
		stack->st_size += 20;
		stack->st = realloc(stack->st, stack->st_size * sizeof(val));
		if (stack->st == NULL)
			die(ENOMEM, _("not enough memory"));
	}
	stack->st[stack->st_count] = val;
	stack->st_count++;

	return 0;
}

int
stack_pop(stack_t *stack, uintmax_t *val)
{
	if (stack->st_count) {
		--stack->st_count;
		*val = stack->st[stack->st_count];

		return 0;
	}

	return 1;
}
