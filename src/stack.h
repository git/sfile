/*
 * Stack structures
 *
 * Part of sfile
 *
 * Copyright © 1998-2002, 2004 Guillem Jover <guillem@hadrons.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 */

#ifndef	STACK_H
#define	STACK_H

#include "types.h"

struct stack {
	uintmax_t	*st;		/* stack */
	size_t		st_size;	/* stack current allocated size */
	size_t		st_count;	/* stack current elements */
};

typedef struct stack stack_t;

extern int stack_alloc(stack_t *stack, size_t size);
extern int stack_free(stack_t *stack);
extern int stack_push(stack_t *stack, uintmax_t val);
extern int stack_pop(stack_t *stack, uintmax_t *val);

#endif
